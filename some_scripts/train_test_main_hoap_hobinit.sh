#!/bin/bash
#$-cwd

#if [ ! -n "$1" ]
#    then
#    echo "Usage: " $0 " OBJ_CLASS"
#    exit
#else
#    ACTION=$1
#fi

#C_init=$2
#P_init=$3

FILEDIR=/home/galen/pukumar/data/action_data_svmltype_imagebased_Allbboxes/train/
TESTFILEDIR=/home/galen/pukumar/data/action_data_svmltype_imagebased_Allbboxes/test/


#Cbest=( 100 100 1000 100 100 1000 100 10000 100 100 )
#Bbest=( 10 100 1000 1000 0.1 1 10 1000 100 0.1 )

#Cbest=( 100 100 1000 100 100 )
#Bbest=( 10 100 1000 1000 0.1 )


Cbest=( 100 10000 )
Bbest=( 10 1000 )

C=( 0.1 1 10 100 )
#C=( 0.01 )
G=( 0 )
#B=( 0.0001 0.001 0.01 0.1 1 )
B=( 10 )
folds=5

#declare -a action=("jumping" "phoning" "playinginstrument" "reading" "ridingbike" "running" "takingphoto" "usingcomputer" "walking" "ridinghorse")
#declare -a action=("jumping" "phoning" "playinginstrument" "reading" "ridingbike")
#declare -a action=("jumping")
#declare -a action=("running")
#declare -a action=("phoning" "ridinghorse" "walking")
declare -a action=("takingphoto" "usingcomputer")

cno=0
for a in ${action[@]};
do

        ACTION=$a
        PBS_LOG=./PBS_M4AP_IMAGEBASED_ALLBB_BINWEIGHT_CSSVMINIT_${ACTION}/
        LOGDIR=${PBS_LOG}TRAIN/
        MODELDIR=./models_m4ap_imagebased_allbb_binweight_cssvminit_${ACTION}/
	
	Cinit=${Cbest[$cno]}
	Binit=${Bbest[$cno]}
	cno=`expr $cno + 1`

        if [ ! -d ${PBS_LOG} ]; then
        echo "Log Directory ${PBS_LOG} not present, creating it"
        mkdir $PBS_LOG
        fi

        if [ ! -d ${LOGDIR} ]; then
        echo "Log Directory ${LOGDIR} not present, creating it"
        mkdir $LOGDIR
        fi

        if [ ! -d ${MODELDIR} ]; then
        echo "Model Directory ${MODELDIR} not present, creating it"
        mkdir $MODELDIR
        fi


        for c in ${C[@]};
        do
          for g in ${G[@]};
           do
	    for b in ${B[@]};
	     do
                for k in `seq 1 ${folds}`;
                do

                        tf_ex=${FILEDIR}${ACTION}_${k}_myown_ic_data_train
                        testfile_ex=${TESTFILEDIR}${ACTION}_${k}_myown_ic_data_test
			
			model_init=/home/galen/pukumar/codes/M4/src_bb_imageBased/best_models_cssvm_binweights_correct/model_cssvm_imagebased_${ACTION}_C${Cinit}_Pt-1_G0_B${Binit}_fold${k}
			model=${MODELDIR}model_m4ap_imagebased_${ACTION}_C${c}_Pt-1_G${g}_B${b}_fold${k}

                        JOB_NAME=M4CS_${ACTION}C${c}B${b}f${k}
                        LOGSTRINP="${LOGDIR}model_m4ap_${ACTION}_C${c}_Pt-1_G${g}_B${b}_f${k}.o"
                        LOGSTRINP_ERR="${LOGDIR}model_m4ap_${ACTION}_C${c}_Pt-1_G${g}_B${b}_f${k}.e"

                        #echo $C_export
                        #echo $tf_ex
                        #echo $testfile_ex
                        #echo $model_init
                        #echo $LOGSTRINP

                        qsub -o ${LOGSTRINP} -e ${LOGSTRINP_ERR} -N ${JOB_NAME} train_test_helper_hoap_hobinit.sh ${c} ${g} ${b} $tf_ex $model $model_init $testfile_ex

                        done

                done
         done
    done

done

