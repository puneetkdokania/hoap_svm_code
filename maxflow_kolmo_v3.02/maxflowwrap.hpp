/************************************************************************/
/*   maxflowwrap.hpp                                                    */
/*                                                                      */
/*   function declarations for dynamic graph cut used by HOB-SVM and HOAP-SVM
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

#ifndef __MAXFLOWWRAP_H__
#define __MAXFLOWWRAP_H__

extern int *maxflow_labels(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg);
extern int **dgc_maxmarginal_labels(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg, double *up_new, double *un_new);

extern int **dgc_maxmarginal_labels_with_flow(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg, double *up_new, double *un_new, double **mm_score);
extern int **dgc_maxmarginal_labels_subgraph_flow(double *unary_pos, double *unary_neg, double **binary, double *up_new, double *un_new, int num_nodes_subgraph, int num_ex, int *node_index,double *mmi_w, int **mm_labels);

extern int *maxflow_labels_withflow(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg, double *flow_val);

extern void check_gynamic_gc();

#endif
