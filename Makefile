# Makefile for HOAP-SVM
# Author: Puneet K. Dokania

CPP=g++
CC=gcc
CFLAGS= -fopenmp -O3
LD=gcc -O3
LDFLAGS= 
#LIBS= -lm

GRAPH_H= ./maxflow_kolmo_v3.02/  
OS=$(shell uname)
ifeq ($(OS),Darwin) # for mac

	#NOTE: Change the following variable to point to your Mosek headers directory
	MOSEK_H= /Users/puneet/mosek/7/tools/platform/osx64x86/h/
	MSKLINKFLAGS= -lmosek64 -lpthread -lm                                            
	MSKLIBPATH= /Users/puneet/mosek/7/tools/platform/osx64x86/bin/                                

LIBS= -lm
else ifeq ($(OS),Linux)

	#NOTE: Change the following variable to point to your Mosek headers directory
	MOSEK_H= /home/puneet/Projects/Software/mosek/6/tools/platform/linux64x86/h/
	MSKLINKFLAGS= -lmosek64 -lpthread -lm
	MSKLIBPATH= /home/puneet/Projects/Software/mosek/6/tools/platform/linux64x86/bin/

LIBS= -lm -lrt
endif

all: hoap_svm_learn  hoap_svm_classify
	rm -f *.o

clean: tidy
	rm -f hoap_svm_learn hoap_svm_classify

tidy:
	rm -f *.o

hoap_svm_learn: hoap_svm_main.o svm_common.o mosek_qp_optimize.o maxflow.o maxflowwrap.o hoap_svm_api.o
	$(LD) $(LDFLAGS) hoap_svm_main.o svm_common.o mosek_qp_optimize.o hoap_svm_api.o maxflow.o  maxflowwrap.o  -lstdc++ -o hoap_svm_learn \
			 $(LIBS) -L $(MSKLIBPATH) $(MSKLINKFLAGS) 

hoap_svm_classify: hoap_svm_classify.o svm_common.o maxflow.o  maxflowwrap.o hoap_svm_api.o
	$(LD) $(LDFLAGS) hoap_svm_classify.o svm_common.o hoap_svm_api.o maxflow.o maxflowwrap.o -lstdc++ -o hoap_svm_classify $(LIBS)

hoap_svm_main.o: ./hoap_svm/hoap_svm_main.c
	$(CC) -c $(CFLAGS) ./hoap_svm/hoap_svm_main.c -o hoap_svm_main.o

svm_common.o: ./svm_light/svm_common.c ./svm_light/svm_common.h ./svm_light/kernel.h
	$(CC) -c $(CFLAGS) ./svm_light/svm_common.c -o svm_common.o

mosek_qp_optimize.o: ./mosek/mosek_qp_optimize.c
	$(CC) -c $(CFLAGS) ./mosek/mosek_qp_optimize.c -o mosek_qp_optimize.o -I $(MOSEK_H)

hoap_svm_api.o: ./hoap_svm/hoap_svm_api.c ./hoap_svm/hoap_svm_api_types.h
	$(CC) -c $(CFLAGS) ./hoap_svm/hoap_svm_api.c -o hoap_svm_api.o -I $(GRAPH_H)

graph.o: ./maxflow_kolmo_v3.02/graph.cpp ./maxflow_kolmo_v3.02/graph.h
	$(CPP) -c $(CFLAGS) ./maxflow_kolmo_v3.02/graph.cpp -o graph.o

maxflow.o: ./maxflow_kolmo_v3.02/maxflow.cpp ./maxflow_kolmo_v3.02/graph.h
	$(CPP) -c $(CFLAGS) ./maxflow_kolmo_v3.02/maxflow.cpp -o maxflow.o

maxflowwrap.o: ./maxflow_kolmo_v3.02/maxflowwrap.cpp ./maxflow_kolmo_v3.02/maxflowwrap.hpp 
	$(CPP) -c $(CFLAGS) ./maxflow_kolmo_v3.02/maxflowwrap.cpp -o maxflowwrap.o

hoap_svm_classify.o: ./hoap_svm/hoap_svm_classify.c
	$(CC) -c $(CFLAGS) ./hoap_svm/hoap_svm_classify.c -o hoap_svm_classify.o


