/************************************************************************/
/*   hoap_svm_api.c                                                    */
/*                                                                      */
/*   API for the following learning algorithms:
    (1) Multi-Class SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP SVM (HOAP-SVM)
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

#include <time.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include "hoap_svm_api.h"
#include "../maxflow_kolmo_v3.02/maxflowwrap.hpp"
#include "../fastapprox-0.3.2/fastonebigheader.h"

#define BILLION  1E9
#define MAX_INPUT_LINE_LENGTH 10000

#define MAX(x,y) ((x) < (y) ? (y) : (x))
#define MIN(x,y) ((x) > (y) ? (y) : (x))

double const MARGINAL_INF = 1.0E5;

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

void current_utc_time(struct timespec *ts) {
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts->tv_sec = mts.tv_sec;
    ts->tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, ts);
#endif

}

void die(const char *message)
{
    if(errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }
    exit(1);
}



int img_score_comp(const void *a, const void *b) {
    SAMPLE_SCORE *aa = (SAMPLE_SCORE*)a;
    SAMPLE_SCORE *bb = (SAMPLE_SCORE*)b;

    if(aa->score > bb->score)
        return -1;
    if(bb->score > aa->score)
        return 1;
    return (aa->idx > bb->idx) ? 1 : -1;
}

void  find_adjacency_matrix( SAMPLE *sample, STRUCT_LEARN_PARM *sparm)
/* find the underlying strucutre based on distance in the poselet space and the gist space */
{

    int i,j;
    int n = sample->examples[0].n_imgs;
    SVECTOR *temp_sub = NULL;
    double poseletDistance = 0.0;
    double gistDistance = 0.0;

    //FILE *fid1;
    //fid1 = fopen("poselet_gist_distances_ordered","w");

    sparm->num_edges_pp = 0;
    sparm->num_edges_pn = 0;
    sparm->num_edges_nn = 0;

    printf("\n\t\tCreating neighborhood: Connected if neightbors in POSELET or GIST feature space.");

    for (i = 0; i < n; i++){

        sample->examples[0].x.neighbors[i] = (int *) malloc(n*sizeof(int));

        for (j=(i+1); j < n; j++){

            temp_sub = sub_ss(sample->examples[0].x.x_is[i].phi1, sample->examples[0].x.x_is[j].phi1); //poselet
            poseletDistance = sprod_ss(temp_sub, temp_sub); //poselet distance
            free_svector(temp_sub);

            temp_sub = sub_ss(sample->examples[0].x.x_is[i].phi2, sample->examples[0].x.x_is[j].phi2); //gist
            gistDistance = sprod_ss(temp_sub, temp_sub); //gist distance
            free_svector(temp_sub);

            //fprintf(fid1,"%f %f\n",vecDistance1,vecDistance2);
            //printf("%f\t",vecDistance2);

            if( poseletDistance <= sparm->pose_thres || gistDistance <= sparm->gist_thres) // if neighbors in POSELET or GIST                
            {
                sample->examples[0].x.neighbors[i][j]=1;
                sample->examples[0].x.n_neighbors++;

                if(sample->examples[0].y.labels[i] == sample->examples[0].y.labels[j]){
                    if(sample->examples[0].y.labels[i] == 1){
                        sparm->num_edges_pp++;
                    }else{
                        sparm->num_edges_nn++;
                    }
                }else{
                    sparm->num_edges_pn++;
                }

            }
            else{
                sample->examples[0].x.neighbors[i][j]=0;
                //printf("\n%f\t%f\n",vecDistance1,vecDistance2);
            }

        }

    }
}

void union_set(int i, int j, int num_elements, int *ID_num, int *ID_counter, int* graphID, int* visited){

    // if already visited then merge the sets
    if(visited[i] && visited[j]){
        int ID_i = graphID[i];
        int ID_j = graphID[j];

        int k;
        for(k=0;k<num_elements;k++){
            if((graphID[k] == ID_i)||(graphID[k] == ID_j))   graphID[k] = MIN(ID_i,ID_j);
        }
        if(ID_i!=ID_j) (*ID_counter)--;
        return;
    }

    if(visited[i]){
        int ID = graphID[i];
        graphID[j] = ID;
        visited[j] = 1;
        return;
    }
    if(visited[j]){
        int ID = graphID[j];
        graphID[i] = ID;
        visited[i] = 1;
        return;
    }

    graphID[i] = *ID_num;
    graphID[j] = *ID_num;
    visited[i] = 1;
    visited[j] = 1;
    (*ID_counter)++;
    (*ID_num)++;

    return;
}

void find_disconnected_subgraphs(int n, int** adjmatrix, int* graphID, int *edges){

    /* finding different disconnected subgraphs (not an optimized code) -- Not using it*/
    int *visited,i,j;

    visited = (int*) calloc(n, sizeof(int));

    int too_big = n+10;

    for(i=0;i<n;i++){
        graphID[i] = too_big; // it's good to initialize it with a number which can't be a graphID
    }

    int ID_num = 0;
    int ID_counter = 0;
    for(i=0;i<n;i++){
        for(j=i+1;j<n;j++){
            if(adjmatrix[i][j]){
                union_set(i,j,n, &ID_num, &ID_counter, graphID, visited); // merge edges
                edges[i]++;
            }
        }
    }

    for(i=0;i<n;i++){
        if(!visited[i]){
            graphID[i] = ID_num;
            ID_num++;
            ID_counter++;
        }
    }
    free(visited);

    //return ID_counter;

}

int uf_find(int v1, int* graphID)
{
    if(graphID[v1] == v1)
        return v1;
    int ret = uf_find(graphID[v1], graphID);
    graphID[v1] = ret;
    return ret;
}

void uf_union(int v1, int v2, int* graphID)
{
    //comp[v1]=uf_find(v2,comp);
    graphID[uf_find(v1,graphID)]=uf_find(v2,graphID);
}

void solve_unionfind(int n, int** adjmatrix, int* graphID, int *edges){
    int i,j;
    for(i=0;i<n;i++)
        graphID[i]=i;
    for(i=0;i<n;i++)
    {
        for(j=i+1;j<n;j++)
            if(adjmatrix[i][j]){
                edges[i]++;
                uf_union(i,j,graphID);
            }
    }
    for(i =0;i<n;i++)
        uf_find(i,graphID);
}

void find_disconnected_subgraphs_main(SAMPLE *sample){

    /* find disconnected components of the graphs. Uses union-find datastructure to solve this problem */

    printf("\n\t\tFinding independent components of the graph ...\n");
    int i,j;
    int num_imgs = sample->examples[0].n_imgs;
    int *graphID = (int*) malloc(num_imgs*sizeof(int));
    int *num_edges = (int*) calloc(num_imgs,sizeof(int));

    /* finding different disconnected subgraphs using union-find datastructure */
    solve_unionfind(num_imgs, sample->examples[0].x.neighbors, graphID, num_edges);

    /* My method: finding different disconnected subgraphs */
    /*int *graphID_my = (int*) malloc(num_imgs*sizeof(int));
    int *num_edges_my = (int*) calloc(num_imgs,sizeof(int));
    find_disconnected_subgraphs(num_imgs, sample->examples[0].x.neighbors, graphID_my, num_edges_my);
    for (i=0;i<num_imgs;i++){
        if(num_edges_my[i] != num_edges[i]) printf("\n\t\tCAUTION : num_edges_my[i] != num_edges[i]");
    }*/

    /* rename the components in increasing order (starts with 0) */
    int ID_ordered = 0, ID_temp;
    int *visited = (int*)calloc(num_imgs, sizeof(int));
    int *graphID_ordered = (int*)calloc(num_imgs, sizeof(int));
    for (i=0;i<num_imgs;i++){
        if(!visited[i]){
            ID_temp = graphID[i];
            for(j=i;j<num_imgs;j++){
                if(!visited[j] && graphID[j] == ID_temp){
                    graphID_ordered[j] = ID_ordered;
                    visited[j] = 1;

                }
            }
            ID_ordered++;
        }
    }

    /* storing information about the graph structure e.g. nodes/edges for each subgraph, mask etc. (node_index starts with 0, subgraphID starts with 0) */
    int num_subgraphs = ID_ordered;
    sample->examples[0].x.graph.num_subgraphs = num_subgraphs;
    sample->examples[0].x.graph.subgraph = (SUB_GRAPH*) malloc (num_subgraphs*sizeof(SUB_GRAPH));
    sample->examples[0].x.graph.graphID = (int*) malloc(num_imgs*sizeof(int));

    for(i=0;i<num_subgraphs;i++){
        sample->examples[0].x.graph.subgraph[i].node_index = (int*)malloc(sizeof(int));
        sample->examples[0].x.graph.subgraph[i].mask = (int*) calloc(num_imgs,sizeof(int));
        sample->examples[0].x.graph.subgraph[i].num_edges = 0;
    }

    int ID;
    int *node_counter = (int*) calloc(num_subgraphs,sizeof(int));

    for (i=0;i<num_imgs;i++){
        ID = graphID_ordered[i];
        node_counter[ID]++;
        sample->examples[0].x.graph.graphID[i] = ID;
        sample->examples[0].x.graph.subgraph[ID].node_index = (int*)realloc(sample->examples[0].x.graph.subgraph[ID].node_index,node_counter[ID]*sizeof(int));
        sample->examples[0].x.graph.subgraph[ID].node_index[node_counter[ID]-1] = i;
        sample->examples[0].x.graph.subgraph[ID].num_nodes = node_counter[ID];
        sample->examples[0].x.graph.subgraph[ID].num_edges += num_edges[i];
        sample->examples[0].x.graph.subgraph[ID].mask[i] = 1;
    }

    /* sanity check */
    /*int num_nodes, mask_flag, num_nodes_temp;
    for (i=0;i<num_subgraphs;i++){
        num_nodes = sample->examples[0].x.graph.subgraph[i].num_nodes;
        mask_flag = 0;
        num_nodes_temp = 0;
        for(j=0;j<num_imgs;j++){
            if(sample->examples[0].x.graph.subgraph[i].mask[j] == 1){
                mask_flag = 1;
                num_nodes_temp++;
            }
        }
        if(mask_flag == 0){
            printf("\n--- SANITY CHECK FAIL: No node with mask =1 in the subgraph:%d ---\n",i);

        }
        if(num_nodes_temp != num_nodes){
            printf("\n--- SANITY CHECK FAIL: num_nodes_temp != num_nodes in the subgraph:%d ---\n",i);

        }

    }*/

    free(graphID);
    free(graphID_ordered);
    free(node_counter);
    free(visited);
    free(num_edges);

    printf("\t\tDone !!!");
}



SAMPLE read_struct_examples_direct(char *traindatafile, STRUCT_LEARN_PARM *sparm) {
    /* Data format: class index:value
      Note that for each example there are two consecutive sparse vectors, one for poselet and second for the gist */

    printf("\n\tData Loading (svml type): %s ",traindatafile);fflush(stdout);

    DOC **docs;
    long totwords,totdoc;
    double *labels;
    //char traindatafile[1024];
    //sprintf(traindatafile,"jumping_1_svml");

    read_documents(traindatafile,&docs,&labels,&totwords,&totdoc); // function from svm_common.c

    SAMPLE sample;

    int i,num_imgs,j;

    sample.n = 1;
    sample.examples = (EXAMPLE *) malloc(sample.n*sizeof(EXAMPLE));
    if(!sample.examples) die("Memory error.");
    sample.examples[0].x.n_pos = 0;
    sample.examples[0].x.n_neg = 0;


    num_imgs = (int) totdoc/2;
    sample.examples[0].n_imgs = num_imgs;

    // Initialise pattern
    sample.examples[0].x.example_cost = 1.0;

    sample.examples[0].x.x_is = (SUB_PATTERN *) malloc(num_imgs*sizeof(SUB_PATTERN));
    if(!sample.examples[0].x.x_is) die("Memory error.");
    sample.examples[0].y.labels = (int *) malloc(num_imgs*sizeof(int));
    if(!sample.examples[0].y.labels) die("Memory error.");

    SVECTOR *temp=NULL;

    for(i = 0; i < num_imgs; i++){

        sample.examples[0].y.labels[i]= labels[2*i];

        sample.examples[0].x.x_is[i].phi1 = docs[2*i][0].fvec;
        sample.examples[0].x.x_is[i].phi2 = docs[2*i+1][0].fvec;
        temp = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1, sparm->phi1_size);
        sample.examples[0].x.x_is[i].phi1phi2_pos = add_ss(sample.examples[0].x.x_is[i].phi1, temp);
        free_svector(temp);
        sample.examples[0].x.x_is[i].phi1phi2_neg = create_svector_with_index(sample.examples[0].x.x_is[i].phi1phi2_pos->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size));
        sample.examples[0].x.x_is[i].phi1phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi1phi2_pos->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
        ////sample.examples[0].x.x_is[i].phi1phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1, (sparm->phi1_size+sparm->phi2_size)*2);

        sample.examples[0].x.x_is[i].phi1_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi1->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);

        if(sparm->psi2_type == 3){ // [poselet;gist]
            sample.examples[0].x.x_is[i].phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1.0,  (sparm->phi1_size+sparm->phi2_size)*2 + sparm->phi1_size);
        }else{
            sample.examples[0].x.x_is[i].phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
        }       

        if(sample.examples[0].y.labels[i] == 1) {
            sample.examples[0].x.n_pos++;
            sample.examples[0].x.x_is[i].label = 1;
        }
        else{
            sample.examples[0].x.n_neg++;
            sample.examples[0].x.x_is[i].label = -1;

        }
    }
    sample.examples[0].y.n_pos = sample.examples[0].x.n_pos;
    sample.examples[0].y.n_neg = sample.examples[0].x.n_neg;

    sample.examples[0].x.neighbors = (int **) malloc(num_imgs*sizeof(int*)); // mem loss
    sample.examples[0].x.n_neighbors=0;

    sparm->J = (double) sample.examples[0].x.n_neg / (double) sample.examples[0].x.n_pos;    
    sparm->num_ex = (double) (sample.examples[0].x.n_pos + sample.examples[0].x.n_neg);

    /* find the adjacency matrix based on feature vector (GIST) distance */
    find_adjacency_matrix(&sample, sparm);

    /* finding different disconnected subgraphs, subgraph vertices, mask, num_edges and nodes for each sungraph etc. */
    find_disconnected_subgraphs_main(&sample);

    /* find true ranking */
    sample.examples[0].y.ranking = (int *) calloc(num_imgs, sizeof(int)); // memloss
    for(i = 0; i < num_imgs; i++){
        for(j = i+1; j < num_imgs; j++){
            if(sample.examples[0].y.labels[i] == 1){
                if(sample.examples[0].y.labels[j] == -1){
                    sample.examples[0].y.ranking[i]++;
                    sample.examples[0].y.ranking[j]--;
                }
            }
            else{
                if(sample.examples[0].y.labels[j] == 1){
                    sample.examples[0].y.ranking[i]--;
                    sample.examples[0].y.ranking[j]++;
                }
            }
        }
    }

    sparm->num_neighbors = sample.examples[0].x.n_neighbors;
    fflush(stdout);

    printf("\n\tData loading done !!!\n");

    free(labels);
    // free doc also

    return(sample);
}

SVECTOR *read_sparse_vector(char *file_name, int object_id, STRUCT_LEARN_PARM *sparm){

    int scanned;
    WORD *words = NULL;
    char feature_file[1000];
    sprintf(feature_file, "%s_%d.feature", file_name, object_id); // e.g. 2011_005586_1.feature
    FILE *fp = fopen(feature_file, "r");

    if(fp!=NULL){

        int length = 0;
        while(!feof(fp)){
            length++;
            words = (WORD *) realloc(words, length*sizeof(WORD));
            if(!words) die("Memory error.");
            scanned = fscanf(fp, " %d:%f", &words[length-1].wnum, &words[length-1].weight);
            if(scanned < 2) {
                words[length-1].wnum = 0;
                words[length-1].weight = 0.0;
            }
        }
        fclose(fp);

        if(words[length-1].wnum) {
            length++;
            words = (WORD *) realloc(words,length*sizeof(WORD));
            words[length-1].wnum = 0;
            words[length-1].weight = 0.0;
        }

        SVECTOR *fvec = create_svector(words,"",1);
        free(words);

        return fvec;
    }
    else{
        printf("Feature file not found: %s\n\n",feature_file);
        exit(1);
    }
}

SVECTOR *read_sparse_vector_phi1(char *file_name, int object_id, STRUCT_LEARN_PARM *sparm){
    //The poselet features given are 24004 dimensional but we use 2404 dimensional poselet features.
    //Reads poselets between [2401 - 4800] and [24001 - 24004], therefore, new poselet will have the dimension of 2404

    int scanned;
    WORD *words = NULL;
    char feature_file[1000];
    sprintf(feature_file, "%s_%d.feature", file_name, object_id); // e.g. 2011_005586_1.feature
    FILE *fp = fopen(feature_file, "r");

    int wnum;
    float weight;

    if(fp!=NULL){

        int length = 0;
        while(!feof(fp)){

            scanned = fscanf(fp, " %d:%f", &wnum, &weight);
            if(wnum>2400 && wnum<4801 && scanned == 2){
                length++;
                words = (WORD *) realloc(words, length*sizeof(WORD));
                if(!words) die("Memory error.");
                words[length-1].wnum = wnum - 2400;
                words[length-1].weight = weight;
            }
            if(wnum>24000 && scanned == 2){
                length++;
                words = (WORD *) realloc(words, length*sizeof(WORD));
                if(!words) die("Memory error.");
                words[length-1].wnum = wnum - 24000 + 2400;
                words[length-1].weight = weight;
            }
            //scanned = fscanf(fp, " %d:%f", &words[length-1].wnum, &words[length-1].weight);
            /*if(scanned < 2) {
                length++;
                words = (WORD *) realloc(words, length*sizeof(WORD));
                if(!words) die("Memory error.");
                words[length-1].wnum = 0;
                words[length-1].weight = 0.0;
            }*/
        }
        fclose(fp);

        if(words[length-1].wnum) {
            length++;
            words = (WORD *) realloc(words,length*sizeof(WORD));
            words[length-1].wnum = 0;
            words[length-1].weight = 0.0;
        }

        SVECTOR *fvec = create_svector(words,"",1);
        free(words);

        return fvec;
    }
    else{
        printf("Feature file not found: %s\n\n",feature_file);
        exit(1);
    }
}

SVECTOR *read_sparse_vector_phi2(char *file_name, STRUCT_LEARN_PARM *sparm){

    int scanned;
    WORD *words = NULL;
    char feature_file[1000];
    sprintf(feature_file, "%s", file_name);
    FILE *fp = fopen(feature_file, "r");

    if(fp!=NULL){

        int length = 0;
        while(!feof(fp)){
            length++;
            words = (WORD *) realloc(words, length*sizeof(WORD));
            if(!words) die("Memory error.");
            scanned = fscanf(fp, " %d:%f", &words[length-1].wnum, &words[length-1].weight);
            if(scanned < 2) {
                words[length-1].wnum = 0;
                words[length-1].weight = 0.0;
            }
        }
        fclose(fp);

        if(words[length-1].wnum) {
            length++;
            words = (WORD *) realloc(words,length*sizeof(WORD));
            words[length-1].wnum = 0;
            words[length-1].weight = 0.0;
        }

        SVECTOR *fvec = create_svector(words,"",1);
        free(words);

        return fvec;
    }
    else{
        printf("Feature file not found: %s\n\n",feature_file);
        exit(1);

    }
}

SAMPLE read_struct_examples(char *file, STRUCT_LEARN_PARM *sparm) {

    printf("\n\tData Loading (location type): %s", file);fflush(stdout);
    SAMPLE sample;

    int i,j, num_imgs;


    // open the file containing candidate bounding box dimensions/labels/featurePath and image label
    FILE *fp = fopen(file, "r");
    if(fp==NULL){
        printf("Error: Cannot open input file %s\n",file);
        exit(1);
    }

    sample.n = 1;
    sample.examples = (EXAMPLE *) malloc(sample.n*sizeof(EXAMPLE));
    if(!sample.examples) die("Memory error.");
    sample.examples[0].x.n_pos = 0;
    sample.examples[0].x.n_neg = 0;

    fscanf(fp,"%d", &sample.examples[0].n_imgs);
    num_imgs = sample.examples[0].n_imgs;

    // Initialise pattern
    sample.examples[0].x.example_cost = 1.0;

    sample.examples[0].x.x_is = (SUB_PATTERN *) malloc(num_imgs*sizeof(SUB_PATTERN));
    if(!sample.examples[0].x.x_is) die("Memory error.");
    sample.examples[0].y.labels = (int *) malloc(num_imgs*sizeof(int));
    if(!sample.examples[0].y.labels) die("Memory error.");

    SVECTOR *temp=NULL;

    for(i = 0; i < num_imgs; i++){
        fscanf(fp,"%s",sample.examples[0].x.x_is[i].phi1_file_name); // poselet file
        fscanf(fp,"%s",sample.examples[0].x.x_is[i].phi2_file_name); // gist file
        fscanf(fp, "%d", &sample.examples[0].x.x_is[i].id); // class
        fscanf(fp, "%d", &sample.examples[0].y.labels[i]);

        sample.examples[0].x.x_is[i].phi1 = read_sparse_vector_phi1(sample.examples[0].x.x_is[i].phi1_file_name, sample.examples[0].x.x_is[i].id, sparm);
        sample.examples[0].x.x_is[i].phi2 = read_sparse_vector_phi2(sample.examples[0].x.x_is[i].phi2_file_name, sparm);
        temp = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1, sparm->phi1_size);
        sample.examples[0].x.x_is[i].phi1phi2_pos = add_ss(sample.examples[0].x.x_is[i].phi1, temp);
        free_svector(temp);
        sample.examples[0].x.x_is[i].phi1phi2_neg = create_svector_with_index(sample.examples[0].x.x_is[i].phi1phi2_pos->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size));
        sample.examples[0].x.x_is[i].phi1phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi1phi2_pos->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
        ////sample.examples[0].x.x_is[i].phi1phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1, (sparm->phi1_size+sparm->phi2_size)*2);


        sample.examples[0].x.x_is[i].phi1_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi1->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
        if(sparm->psi2_type == 3){ // [poselet;gist]
            sample.examples[0].x.x_is[i].phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2 + sparm->phi1_size);
        }else{
            sample.examples[0].x.x_is[i].phi2_shift = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
        }

        /*temp = sample.examples[0].x.x_is[i].phi1_shift;
        sample.examples[0].x.x_is[i].phi1_shift = smult_s(temp, sparm->binary_weight);
        free_svector(temp);

        temp = sample.examples[0].x.x_is[i].phi2_shift;
        sample.examples[0].x.x_is[i].phi2_shift = smult_s(temp, sparm->binary_weight);
        free_svector(temp);*/


        /*temp = create_svector_with_index(sample.examples[0].x.x_is[i].phi2->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2+(sparm->phi1_size));
        sample.examples[0].x.x_is[i].phi2_shift = smult_s(temp,sparm->pairwise_weight2);
        free_svector(temp);*/

        if(sample.examples[0].y.labels[i] == 1) {
            sample.examples[0].x.n_pos++;
            sample.examples[0].x.x_is[i].label = 1;
        }
        else{
            sample.examples[0].x.n_neg++;
            sample.examples[0].x.x_is[i].label = -1;

        }
    }
    sample.examples[0].y.n_pos = sample.examples[0].x.n_pos;
    sample.examples[0].y.n_neg = sample.examples[0].x.n_neg;

    sample.examples[0].x.neighbors = (int **) malloc(num_imgs*sizeof(int*)); // mem loss
    sample.examples[0].x.n_neighbors=0;

    sparm->J = (double) sample.examples[0].x.n_neg / (double) sample.examples[0].x.n_pos;
    sparm->num_ex = (double) (sample.examples[0].x.n_pos + sample.examples[0].x.n_neg);

    /* find the adjacency matrix based on feature vector (GIST) distance */
    find_adjacency_matrix(&sample, sparm);

    /* finding different disconnected subgraphs, subgraph vertices, mask, num_edges and nodes for each sungraph etc. */
    find_disconnected_subgraphs_main(&sample);

    /* find true ranking */
    sample.examples[0].y.ranking = (int *) calloc(num_imgs, sizeof(int)); // memloss
    for(i = 0; i < num_imgs; i++){
        for(j = i+1; j < num_imgs; j++){
            if(sample.examples[0].y.labels[i] == 1){
                if(sample.examples[0].y.labels[j] == -1){
                    sample.examples[0].y.ranking[i]++;
                    sample.examples[0].y.ranking[j]--;
                }
            }
            else{
                if(sample.examples[0].y.labels[j] == 1){
                    sample.examples[0].y.ranking[i]--;
                    sample.examples[0].y.ranking[j]++;
                }
            }
        }
    }

    sparm->num_neighbors = sample.examples[0].x.n_neighbors;
    fflush(stdout);

    printf("\n\tData loading done !!!\n");
    fclose(fp);


    return(sample);
}

void init_struct_model(SAMPLE sample, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {
    /*
       Initialize parameters in STRUCTMODEL sm. Set the diminension
       of the feature space sm->sizePsi. Can also initialize your own
       variables in sm here.
       */

    sm->n = sample.n;
    sm->len_w1 = (sparm->phi1_size+sparm->phi2_size)*2;

    if(sparm->psi2_type == 1){ // pairwise using only poselet
        sm->len_w2 = (sparm->phi1_size);
    }else if(sparm->psi2_type == 2){  // pairwise using only gist
        sm->len_w2 = (sparm->phi2_size);
    }else{  // pairwise using [poselet;gist]
        sm->len_w2 = (sparm->phi1_size + sparm->phi2_size);
    }

    sm->sizePsi = sm->len_w1 + sm->len_w2;
    sparm->sizePsi = sm->sizePsi;

    sm->inter_time_taken = 0.0;

    sm->pose_sigma = sparm->pose_sigma;
    sm->gist_sigma = sparm->gist_sigma;
    sm->psi2_type = sparm->psi2_type;
    sm->psi_normalizer = 1.0;

    sm->binary_weight = sparm->binary_weight;


    /* Scaling of the problem, good for mosek */
    if(sparm->A == 0){ //CSSVM
        sparm->slack_normalizer = (double) sparm->num_ex;
        //sparm.slack_normalizer /= (double) 10.0;
        //C *= (double) 10.0;
        //sparm.slack_normalizer = (double) MAX(sparm.num_neighbors, num_ex);
        //sparm.slack_normalizer = (double) MAX(sparm.binary_weight, num_ex);
        //C *= (double) sparm.slack_normalizer;
        //epsilon /= (double) sparm.slack_normalizer;
    }else if(sparm->A == 1){
        //sparm.slack_normalizer = (double) 1.0;
        //C *= (double) sparm.slack_normalizer;

    }else if(sparm->A == 2){ //M4AP
        //sparm.slack_normalizer = 1.0;
        //sparm.slack_normalizer = (double) MAX(sparm.num_neighbors, num_ex);
        //C *= (double) sparm.slack_normalizer;

        //sm.psi_normalizer = (double) MAX(sparm.num_neighbors, num_ex);
        //sm.psi_normalizer = (double) MAX(sparm.num_edges_pn, num_ex);
        //sm.psi_normalizer = 10000.0;
        //sm.psi_normalizer = (double) num_ex;;
        //C *= (double) sm.psi_normalizer;
    }

}

int** dgc_maxmarginal_labels_pos(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg){

    int num_ex = n_pos + n_neg;
    int i, **mm_labels_pos;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));

    for (i = 0; i < num_ex; i++){

        up_new[i] = unary_pos[i]; // y = 1
        un_new[i] = MARGINAL_INF;        

    }
    mm_labels_pos = dgc_maxmarginal_labels(unary_pos, unary_neg, binary, n_pos, n_neg, up_new, un_new);

    free(up_new);
    free(un_new);

    return mm_labels_pos;
}

int** dgc_maxmarginal_labels_neg(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg){

    int num_ex = n_pos + n_neg;
    int i, **mm_labels_neg;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));

    for (i = 0; i < num_ex; i++){

        up_new[i] = MARGINAL_INF; // y = -1        
        un_new[i] = unary_neg[i];

    }
    mm_labels_neg = dgc_maxmarginal_labels(unary_pos, unary_neg, binary, n_pos, n_neg, up_new, un_new);

    free(up_new);
    free(un_new);

    return mm_labels_neg;
}

SVECTOR *psi(PATTERN x, LABEL y, int *label_mask, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {
    /*
       Creates the feature vector \Psi(x,y) and return a pointer to
       sparse vector SVECTOR. The dimension of the
       feature vector returned has to agree with the dimension in sm->sizePsi.
       */
    SVECTOR *fvec=NULL;
    SVECTOR *psi1=NULL;
    SVECTOR *psi2=NULL;
    SVECTOR *temp_psi=NULL;
    //SVECTOR *temp_sub=NULL;

    SVECTOR *phi1_shift = NULL;
    SVECTOR *phi2_shift = NULL;
    SVECTOR *phi1_shift_sub = NULL;
    SVECTOR *phi2_shift_sub = NULL;
    SVECTOR *phi1_shift_temp = NULL;
    SVECTOR *phi2_shift_temp = NULL;


    WORD *words = NULL;
    words = (WORD *) malloc(sizeof(WORD));
    if(!words) die("Memory error.");
    words[0].wnum = 0;
    words[0].weight = 0.0;
    //fvec = create_svector(words,"",1); // mem loss (major)
    psi1 = create_svector(words,"",1);
    assert(psi1->words != NULL); // CARE
    //psi2 = create_svector(words,"",1); // mem loss (major)

    phi1_shift = create_svector(words,"",1);
    phi2_shift = create_svector(words,"",1);

    int i,j = 0, k = 0;
    int num_nodes_subgraph = 0;
    int num_edges_subgraph = 0;
    int graphID;
    //int num_nodes = 0;
    //int num_edges = 0;
    int num_ex;


    double factor_edge = 0.0;
    double factor_node = 0.0;

    num_ex = x.n_pos+x.n_neg;
    //double factor = MAX(num_ex, sparm->num_neighbors);

    for (i = 0; i <num_ex ; i++){
        if(label_mask[i]){

            graphID = x.graph.graphID[i];
            num_edges_subgraph =  x.graph.subgraph[graphID].num_edges;
            num_nodes_subgraph =  x.graph.subgraph[graphID].num_nodes;

            //num_nodes_subgraph = (double) (x.n_pos + x.n_neg);
            //num_edges_subgraph = (double) (x.n_pos + x.n_neg);


            //factor_node = 1.0/(double)(num_nodes_subgraph);
            //factor_edge = 1.0/(double)(num_edges_subgraph);

            factor_node = 1.0;
            factor_edge = 1.0;

            //factor_node = 1.0/sm->psi_normalizer;
            //factor_edge = 1.0/sm->psi_normalizer;

            if(y.labels[i] == 1){
                temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_pos, factor_node);
            }
            else{

                temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_neg, factor_node);

            }
            free_svector(psi1);
            psi1 = copy_svector(temp_psi);
            free_svector(temp_psi);
            //num_nodes++;


            for (j=(i+1); j < (x.n_pos+x.n_neg); j++){
                if(x.neighbors[i][j]){
                    //num_edges++;
                    if(y.labels[i] != y.labels[j]){

                        //SVECTOR* exp_add_ss_pow(SVECTOR *a, SVECTOR *b, double factor_add, double factor_pow, double factor_exp )
                        phi1_shift_sub = exp_add_ss_pow(x.x_is[i].phi1_shift, x.x_is[j].phi1_shift, -1.0, 2.0, sparm->pose_sigma);
                        phi1_shift_temp = multadd_ss(phi1_shift, phi1_shift_sub, factor_edge);
                        free_svector(phi1_shift);
                        free_svector(phi1_shift_sub);
                        phi1_shift = phi1_shift_temp;

                        phi2_shift_sub = exp_add_ss_pow(x.x_is[i].phi2_shift, x.x_is[j].phi2_shift, -1.0, 2.0, sparm->gist_sigma);
                        phi2_shift_temp = multadd_ss(phi2_shift, phi2_shift_sub, factor_edge);
                        free_svector(phi2_shift);
                        free_svector(phi2_shift_sub);
                        phi2_shift = phi2_shift_temp;
                    }
                }
            }
        }
    }

    assert(psi1->words != NULL); // CARE

    if(sparm->psi2_type == 1){ //only poselet
        psi2 = copy_svector(phi1_shift);

    }else if(sparm->psi2_type == 2){ //only gist
        psi2 = copy_svector(phi2_shift);

    }else{ // [poselet;gist]
        //// add phi1, phi2 to get psi2 (note that they are already indexed)
        psi2 = add_ss(phi1_shift, phi2_shift);
    }

    /* multiply pairwise feature vector with the given binary weight parameter */
    temp_psi = smult_s(psi2, sparm->binary_weight);
    free_svector(psi2);
    psi2 = copy_svector(temp_psi);
    free_svector(temp_psi);



    // scale psi1 by 1/n
    /*temp_psi = smult_s(psi1, (double)1.0/(double)(num_nodes));
    free_svector(psi1);
    psi1 = temp_psi;*/

    // scale psi2 by 1/|E|
    /*if (num_edges){
        temp_psi = smult_s(psi2, (double)1.0/(double)(num_edges));
        free_svector(psi2);
        psi2 = temp_psi;
    }*/

    //// concatenate psi1, psi2
    fvec = add_ss(psi1, psi2);
    free_svector(psi1);
    free_svector(psi2);

    free_svector(phi1_shift);
    free_svector(phi2_shift);

    // concatenate psi1, psi2
    /*temp_psi = create_svector_with_index(psi2->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
      free_svector(psi2);
      fvec = add_ss(psi1, temp_psi);
      free_svector(temp_psi);
      free_svector(psi1);*/

    free(words);
    return(fvec);
}


SVECTOR *dynamic_psi(PATTERN x, SVECTOR *f0, LABEL y, LABEL ybar, int* label_mask, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {
    /* this method is based on subgraphs, i.e., dynamic psi for each subgraph independently, the input psi is the psi of a particular subgraph */

    // find the indices where the labels are different
    int i,j = 0,j_changed = 0, i_changed = 0;
    int num_ex = x.n_pos+x.n_neg;

    int *diff_indices = (int*) malloc(sizeof(int));
    int *changed_flag = (int*) malloc(num_ex*sizeof(int));
    int diff_count = 0;

    for(i=0;i<num_ex; i++){
        if(y.labels[i] != ybar.labels[i]){
            diff_count++;
            diff_indices = (int*)realloc(diff_indices,diff_count*sizeof(int));
            diff_indices[diff_count-1] = i;
            changed_flag[i] = 1;
        }else{
            changed_flag[i] = 0;
        }
    }
    //printf("\ndiff_count:%02d",diff_count);


    SVECTOR *fvec_extra=NULL, *fvec=NULL;
    SVECTOR *psi1=NULL;
    SVECTOR *psi2=NULL;
    SVECTOR *temp_psi=NULL;
    SVECTOR *temp_sub=NULL;

    SVECTOR *phi1_shift = NULL;
    SVECTOR *phi2_shift = NULL;
    SVECTOR *phi1_shift_sub = NULL;
    SVECTOR *phi2_shift_sub = NULL;
    SVECTOR *phi1_shift_temp = NULL;
    SVECTOR *phi2_shift_temp = NULL;


    WORD *words = NULL;
    words = (WORD *) malloc(sizeof(WORD));
    if(!words) die("Memory error.");
    words[0].wnum = 0;
    words[0].weight = 0.0;
    //fvec = create_svector(words,"",1); // mem loss (major)
    psi1 = create_svector(words,"",1);
    //psi2 = create_svector(words,"",1); // mem loss (major)

    phi1_shift = create_svector(words,"",1);
    phi2_shift = create_svector(words,"",1);

    free(words);

    int num_nodes_subgraph, num_edges_subgraph;
    int graphID;
    double factor_node, factor_edge;


    for (i = 0; i < num_ex; i++){
        if(label_mask[i]){

            graphID = x.graph.graphID[i];
            num_edges_subgraph =  x.graph.subgraph[graphID].num_edges;
            num_nodes_subgraph =  x.graph.subgraph[graphID].num_nodes;

            //factor_node = 1.0/(double)(num_nodes_subgraph);
            //factor_edge = 1.0/(double)(num_edges_subgraph);

            factor_node = 1.0;
            factor_edge = 1.0;

            i_changed = 0;

            if(changed_flag[i]){

                if(ybar.labels[i] == 1){
                    temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_pos, factor_node);
                }
                else{
                    temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_neg, factor_node);
                }
                free_svector(psi1);
                psi1 = copy_svector(temp_psi);
                free_svector(temp_psi);

                // substract the previous effects
                if(y.labels[i] == 1){
                    temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_pos, -1.0*factor_node);
                }
                else{
                    temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_neg, -1.0*factor_node);
                }
                free_svector(psi1);
                psi1 = copy_svector(temp_psi);
                free_svector(temp_psi);

                i_changed = 1;
            }


            for (j=(i+1); j < num_ex; j++){

                if(x.neighbors[i][j]){

                    if(changed_flag[j]) j_changed = 1;
                    else j_changed = 0;

                    if( (i_changed && j_changed && ybar.labels[i] == ybar.labels[j]) || // means, they were having same labels previously also, do nothing because /psi(xi,1,xj,1) = /psi(xi,-1,xj,-1) = 0
                            (i_changed && j_changed && ybar.labels[i] != ybar.labels[j])  || // means, they were having different labels previously also, do nothing
                            (!i_changed && !j_changed) ) // means, nothing changed
                    {
                        // do nothing

                    }


                    else if( (i_changed && !j_changed && ybar.labels[i] == ybar.labels[j] ) || // means, they were having different labels previously (substract the previous effects)
                             (!i_changed && j_changed && ybar.labels[i] == ybar.labels[j] ) )  // means, they were having different labels previously (substract the previous effects)
                    {

                        phi1_shift_sub = exp_add_ss_pow(x.x_is[i].phi1_shift, x.x_is[j].phi1_shift, -1.0, 2.0, sparm->pose_sigma); // phi_shift and phi2_shift are weighted by the user input weights (default weights is 1.0)
                        phi1_shift_temp = multadd_ss(phi1_shift, phi1_shift_sub, -1.0*factor_edge);
                        free_svector(phi1_shift);
                        free_svector(phi1_shift_sub);
                        phi1_shift = copy_svector(phi1_shift_temp);
                        free_svector(phi1_shift_temp);

                        phi2_shift_sub = exp_add_ss_pow(x.x_is[i].phi2_shift, x.x_is[j].phi2_shift, -1.0, 2.0, sparm->gist_sigma); // phi_shift and phi2_shift are weighted by the user input weights (default weights is 1.0)
                        phi2_shift_temp = multadd_ss(phi2_shift, phi2_shift_sub, -1.0*factor_edge);
                        free_svector(phi2_shift);
                        free_svector(phi2_shift_sub);
                        phi2_shift = copy_svector(phi2_shift_temp);
                        free_svector(phi2_shift_temp);

                    }

                    else if( (i_changed && !j_changed && ybar.labels[i] != ybar.labels[j]) || // means, they were having same labels previously (add the new effects)
                             (!i_changed && j_changed && ybar.labels[i] != ybar.labels[j]) )  // means, they were having same labels previously (add the new effects)
                    {

                        phi1_shift_sub = exp_add_ss_pow(x.x_is[i].phi1_shift, x.x_is[j].phi1_shift, -1.0, 2.0, sparm->pose_sigma); // phi_shift and phi2_shift are weighted by the user input weights (default weights is 1.0)
                        phi1_shift_temp = multadd_ss(phi1_shift, phi1_shift_sub, 1.0*factor_edge);
                        free_svector(phi1_shift);
                        free_svector(phi1_shift_sub);
                        phi1_shift = copy_svector(phi1_shift_temp);
                        free_svector(phi1_shift_temp);

                        phi2_shift_sub = exp_add_ss_pow(x.x_is[i].phi2_shift, x.x_is[j].phi2_shift, -1.0, 2.0, sparm->gist_sigma); // phi_shift and phi2_shift are weighted by the user input weights (default weights is 1.0)
                        phi2_shift_temp = multadd_ss(phi2_shift, phi2_shift_sub, 1.0*factor_edge);
                        free_svector(phi2_shift);
                        free_svector(phi2_shift_sub);
                        phi2_shift = copy_svector(phi2_shift_temp);
                        free_svector(phi2_shift_temp);

                    }

                }
            }
        }

    }


    if(sparm->psi2_type == 1){ //only poselet
        psi2 = copy_svector(phi1_shift);

    }else if(sparm->psi2_type == 2){ //only gist
        psi2 = copy_svector(phi2_shift);

    }else{ // [poselet;gist]
        //// add phi1, phi2 to get psi2 (note that they are already indexed)
        psi2 = add_ss(phi1_shift, phi2_shift);
    }

    /* multiply pairwise feature vector with the given binary weight parameter */
    temp_psi = smult_s(psi2, sparm->binary_weight);
    free_svector(psi2);
    psi2 = copy_svector(temp_psi);
    free_svector(temp_psi);

    //// concatenate psi1, psi2
    fvec_extra = add_ss(psi1, psi2); // mem loss (major)
    fvec = add_ss(fvec_extra,f0);

    free_svector(fvec_extra);
    free_svector(psi1);
    free_svector(psi2);
    free_svector(phi1_shift);
    free_svector(phi2_shift);

    free(diff_indices);
    free(changed_flag);


    return(fvec);
}



SVECTOR *phi_combined(PATTERN x, SVECTOR **fycache_lin, SVECTOR **fycache_mvc, int *ranking,  STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {
    /*
  slower version: Creates the combined feature vector \Phi(X,R) used for minimizing MAP loss, where R is the ranking matrix
*/

    long i,j;
    int num_ex;
    int R_i_count;
    double R_ij;
    double norm_factor;

    SVECTOR *fvec=NULL;
    SVECTOR *temp1=NULL;
    SVECTOR *temp2=NULL;
    SVECTOR *temp3=NULL;

    WORD *words = NULL;
    words = (WORD *) malloc(sizeof(WORD));
    words[0].wnum = 0;
    words[0].weight = 0.0;
    fvec = create_svector(words,"",1);
    free(words);
    num_ex = (x.n_pos + x.n_neg);
    int *neg_coeff = (int*) calloc(num_ex, sizeof(int));

    int *R = (int*) calloc(num_ex, sizeof(int));

    //int *Ri = (int*) calloc(num_ex, sizeof(int));

    //printf("\nslower\n");
    for(i = 0; i < num_ex ; i++){
        //printf("%d\t",ranking[i]);

        if(x.x_is[i].label == 1){

            R[i] = 0;

            for(j = 0; j < num_ex; j++){

                if(x.x_is[j].label == -1){

                    if(ranking[i] > ranking[j]){

                        R[j]--;
                        R[i]++;

                    }
                    else{

                        R[j]++;
                        R[i]--;


                    }
                }

            }
        }
        //printf("%d\t",R[i]);
    }


    /*printf("\nslower\n");
    for(i = 0; i < num_ex ; i++){
        //printf("%d\t",ranking[i]);

        if(x.x_is[i].label == 1){

            R_i_count = 0;

            for(j = 0; j < num_ex; j++){

                if(x.x_is[j].label == -1){

                    if(ranking[i] > ranking[j]){
                        //    R_ij = 1;
                        neg_coeff[j]--;
                        R_i_count++;
                        //Ri[i]++;

                        //neg_coeff[j] = -5;
                        //R_i_count = 2;
                    }
                    else{
                        // R_ij = -1;
                        neg_coeff[j]++;
                        R_i_count--;
                        //Ri[i]--;

                        //neg_coeff[j] = -5;
                        //R_i_count = 2;
                    }
                }

            }

            temp1 = sub_ss(fycache_lin[i], fycache_mvc[i]); // ok
            temp2 = smult_s(temp1,(double)  R_i_count);
            free_svector(temp1);
            temp3 = add_ss(fvec, temp2);
            free_svector(temp2);
            free_svector(fvec);
            fvec = temp3;
        }


        //printf("%d\t",Ri[i]);
        //printf("%d\t",neg_coeff[i]);
    }*/

    for(i = 0; i < num_ex; i++){

        if(x.x_is[i].label == -1){

            temp1 = sub_ss(fycache_mvc[i], fycache_lin[i]);
            temp2 = smult_s(temp1,(double) R[i]);
            free_svector(temp1);
            temp3 = add_ss(fvec, temp2);
            free_svector(temp2);
            free_svector(fvec);
            fvec = temp3;
        }else{

            temp1 = sub_ss(fycache_lin[i], fycache_mvc[i]); // ok
            temp2 = smult_s(temp1,(double)  R[i]);
            free_svector(temp1);
            temp3 = add_ss(fvec, temp2);
            free_svector(temp2);
            free_svector(fvec);
            fvec = temp3;

        }
    }

    /*for(j = 0; j < num_ex; j++){

        if(x.x_is[j].label == -1){

            temp1 = sub_ss(fycache_mvc[j], fycache_lin[j]);
            temp2 = smult_s(temp1,(double) neg_coeff[j]);
            free_svector(temp1);
            temp3 = add_ss(fvec, temp2);
            free_svector(temp2);
            free_svector(fvec);
            fvec = temp3;
        }
    }*/

    norm_factor = 1.0/(double)(x.n_pos*x.n_neg);
    temp1 = smult_s(fvec, norm_factor);
    free_svector(fvec);
    fvec = copy_svector(temp1);
    free_svector(temp1);

    free(neg_coeff);
    free(R);

    return(fvec);
}

SVECTOR *psi_with_weights(PATTERN x, int *up_factor, int *un_factor, int **bin_factor, int *up_factor_gt, int *un_factor_gt, int **bin_factor_gt, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **philin) {

    SVECTOR *fvec=NULL;
    SVECTOR *psi1=NULL;
    SVECTOR *psi2=NULL;
    SVECTOR *temp_psi=NULL;


    SVECTOR *phi1_shift = NULL;
    SVECTOR *phi2_shift = NULL;
    SVECTOR *phi1_shift_sub = NULL;
    SVECTOR *phi2_shift_sub = NULL;
    SVECTOR *phi1_shift_temp = NULL;
    SVECTOR *phi2_shift_temp = NULL;

    SVECTOR *fvec_gt=NULL;
    SVECTOR *psi1_gt=NULL;
    SVECTOR *psi2_gt=NULL;
    SVECTOR *temp_psi_gt=NULL;

    SVECTOR *phi1_shift_gt = NULL;
    SVECTOR *phi2_shift_gt = NULL;
    SVECTOR *phi1_shift_sub_gt = NULL;
    SVECTOR *phi2_shift_sub_gt = NULL;
    SVECTOR *phi1_shift_temp_gt = NULL;
    SVECTOR *phi2_shift_temp_gt = NULL;


    WORD *words = NULL;
    words = (WORD *) malloc(sizeof(WORD));
    if(!words) die("Memory error.");
    words[0].wnum = 0;
    words[0].weight = 0.0;

    psi1 = create_svector(words,"",1);
    phi1_shift = create_svector(words,"",1);
    phi2_shift = create_svector(words,"",1);

    psi1_gt = create_svector(words,"",1);
    phi1_shift_gt = create_svector(words,"",1);
    phi2_shift_gt = create_svector(words,"",1);

    int i,j = 0;
    int num_ex;

    float factor_up = 0.0;
    float factor_un = 0.0;
    double factor_bin = 0.0;

    float factor_up_gt = 0.0;
    float factor_un_gt = 0.0;
    double factor_bin_gt = 0.0;

    //int graphID;

    num_ex = x.n_pos+x.n_neg;

    for (i = 0; i <num_ex ; i++){

        //graphID = x.graph.graphID[i];

        // phi(X,R)

        factor_up = (float) up_factor[i];
        factor_un = (float) un_factor[i];

        temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_pos, factor_up);
        free_svector(psi1);
        psi1 = copy_svector(temp_psi);
        free_svector(temp_psi);

        temp_psi = multadd_ss(psi1, x.x_is[i].phi1phi2_neg, factor_un);
        free_svector(psi1);
        psi1 = copy_svector(temp_psi);
        free_svector(temp_psi);

        // phi(X,R*)

        factor_up_gt = (float) up_factor_gt[i];
        factor_un_gt = (float) un_factor_gt[i];

        temp_psi_gt = multadd_ss(psi1_gt, x.x_is[i].phi1phi2_pos, factor_up_gt);
        free_svector(psi1_gt);
        psi1_gt = copy_svector(temp_psi_gt);
        free_svector(temp_psi_gt);

        temp_psi_gt = multadd_ss(psi1_gt, x.x_is[i].phi1phi2_neg, factor_un_gt);
        free_svector(psi1_gt);
        psi1_gt = copy_svector(temp_psi_gt);
        free_svector(temp_psi_gt);

        for (j=(i+1); j < num_ex; j++){

            //if(x.neighbors[i][j] && x.graph.subgraph[graphID].mask[i] && x.graph.subgraph[graphID].mask[j]){
            if(x.neighbors[i][j]){

                factor_bin = (double) bin_factor[i][j];
                factor_bin_gt = (double) bin_factor_gt[i][j];

                //SVECTOR* exp_add_ss_pow(SVECTOR *a, SVECTOR *b, double factor_add, double factor_pow, double factor_exp )
                phi1_shift_sub = exp_add_ss_pow(x.x_is[i].phi1_shift, x.x_is[j].phi1_shift, -1.0, 2.0, sparm->pose_sigma);
                phi1_shift_temp = multadd_ss(phi1_shift, phi1_shift_sub, factor_bin);
                phi1_shift_temp_gt = multadd_ss(phi1_shift_gt, phi1_shift_sub, factor_bin_gt);

                free_svector(phi1_shift);
                free_svector(phi1_shift_gt);
                free_svector(phi1_shift_sub);

                phi1_shift = phi1_shift_temp;
                phi1_shift_gt = phi1_shift_temp_gt;



                phi2_shift_sub = exp_add_ss_pow(x.x_is[i].phi2_shift, x.x_is[j].phi2_shift, -1.0, 2.0, sparm->gist_sigma);
                phi2_shift_temp = multadd_ss(phi2_shift, phi2_shift_sub, factor_bin);
                phi2_shift_temp_gt = multadd_ss(phi2_shift_gt, phi2_shift_sub, factor_bin_gt);
                free_svector(phi2_shift);
                free_svector(phi2_shift_gt);
                free_svector(phi2_shift_sub);

                phi2_shift = phi2_shift_temp;
                phi2_shift_gt = phi2_shift_temp_gt;

            }

        }
    }


    assert(psi1->words != NULL); // CARE

    if(sparm->psi2_type == 1){ //only poselet
        psi2 = copy_svector(phi1_shift);
        psi2_gt = copy_svector(phi1_shift_gt);

    }else if(sparm->psi2_type == 2){ //only gist
        psi2 = copy_svector(phi2_shift);
        psi2_gt = copy_svector(phi2_shift_gt);

    }else{ // [poselet;gist]
        //// add phi1, phi2 to get psi2 (note that they are already indexed)
        psi2 = add_ss(phi1_shift, phi2_shift);
        psi2_gt = add_ss(phi1_shift_gt, phi2_shift_gt);
    }

    /* multiply pairwise feature vector with the given binary weight parameter */
    temp_psi = smult_s(psi2, sparm->binary_weight);
    free_svector(psi2);
    psi2 = copy_svector(temp_psi);
    free_svector(temp_psi);

    /* multiply pairwise feature vector with the given binary weight parameter */
    temp_psi = smult_s(psi2_gt, sparm->binary_weight);
    free_svector(psi2_gt);
    psi2_gt = copy_svector(temp_psi);
    free_svector(temp_psi);

    //// concatenate psi1, psi2
    fvec = add_ss(psi1, psi2);
    free_svector(psi1);
    free_svector(psi2);

    free_svector(phi1_shift);
    free_svector(phi2_shift);


    fvec_gt = add_ss(psi1_gt, psi2_gt);
    free_svector(psi1_gt);
    free_svector(psi2_gt);

    free_svector(phi1_shift_gt);
    free_svector(phi2_shift_gt);

    *philin = fvec_gt;

    // concatenate psi1, psi2
    /*temp_psi = create_svector_with_index(psi2->words, "", 1.0, (sparm->phi1_size+sparm->phi2_size)*2);
      free_svector(psi2);
      fvec = add_ss(psi1, temp_psi);
      free_svector(temp_psi);
      free_svector(psi1);*/

    free(words);
    return(fvec);
}
SVECTOR *phi_combined_faster(PATTERN x, int **labels_lin, int **labels_mvc, int *ranking, int *ranking_gt, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **philin) {
    /*
  Creates the combined feature vector \Phi(X,R) used for minimizing MAP loss, where R is the ranking matrix
*/

    long i,j,k;
    int num_ex;
    double norm_factor;

    SVECTOR *phibar=NULL;
    SVECTOR *philin_temp;
    SVECTOR *temp1=NULL;

    num_ex = (x.n_pos + x.n_neg);

    int *R = (int*) calloc(num_ex, sizeof(int));
    int *Rgt = (int*) calloc(num_ex, sizeof(int));

    int *up_factor = (int*) calloc(num_ex, sizeof(int));
    int *un_factor = (int*) calloc(num_ex, sizeof(int));
    int **bin_factor = (int**) malloc(num_ex*sizeof(int*));

    int *up_factor_gt = (int*) calloc(num_ex, sizeof(int));
    int *un_factor_gt = (int*) calloc(num_ex, sizeof(int));
    int **bin_factor_gt = (int**) malloc(num_ex*sizeof(int*));
    int graphID;

    struct timespec start_t,finish_t;
    double time_taken, total_time = 0.0;
    current_utc_time(&start_t);

    /* get rank coefficients */
    for(i = 0; i < num_ex ; i++){
        if(x.x_is[i].label == 1){

            for(j = 0; j < num_ex; j++){

                if(x.x_is[j].label == -1){

                    if(ranking[i] > ranking[j]){

                        R[j]--;
                        R[i]++;

                    }
                    else{

                        R[j]++;
                        R[i]--;
                    }

                    if(ranking_gt[i] > ranking_gt[j]){

                        Rgt[j]--;
                        Rgt[i]++;

                    }
                    else{

                        Rgt[j]++;
                        Rgt[i]--;
                    }
                }
            }
        }

        bin_factor[i] = (int*) calloc(num_ex, sizeof(int));
        bin_factor_gt[i] = (int*) calloc(num_ex, sizeof(int));
    }


    for(i = 0; i < num_ex ; i++){

        graphID = x.graph.graphID[i];

        for (j=0;j<num_ex;j++){

            if(x.x_is[i].label == 1 && x.graph.subgraph[graphID].mask[j]){

                if(labels_lin[i][j] == 1) { up_factor[j]+=R[i]; up_factor_gt[j]+=Rgt[i]; }
                else if(labels_lin[i][j] == -1) { un_factor[j]+=R[i]; un_factor_gt[j]+=Rgt[i]; }

                if(labels_mvc[i][j] == 1) { up_factor[j]-=R[i]; up_factor_gt[j]-=Rgt[i]; }
                else if(labels_mvc[i][j] == -1) { un_factor[j]-=R[i]; un_factor_gt[j]-=Rgt[i]; }

            }else if(x.x_is[i].label == -1 && x.graph.subgraph[graphID].mask[j]){

                if(labels_mvc[i][j] == 1) { up_factor[j]+=R[i]; up_factor_gt[j]+=Rgt[i]; }
                else if(labels_mvc[i][j] == -1) { un_factor[j]+=R[i]; un_factor_gt[j]+=Rgt[i]; }

                if(labels_lin[i][j] == 1) { up_factor[j]-=R[i]; up_factor_gt[j]-=Rgt[i]; }
                else if(labels_lin[i][j] == -1) { un_factor[j]-=R[i]; un_factor_gt[j]-=Rgt[i]; }

            }

            // get binary potential coefficients

            for (k=j+1;k<num_ex;k++){

                if(x.graph.subgraph[graphID].mask[j] && x.graph.subgraph[graphID].mask[k] && x.neighbors[j][k]){

                    if(x.x_is[i].label == 1){

                        if(labels_lin[i][j] != labels_lin[i][k]) { bin_factor[j][k]+=R[i]; bin_factor_gt[j][k]+=Rgt[i]; }
                        if(labels_mvc[i][j] != labels_mvc[i][k]) { bin_factor[j][k]-=R[i]; bin_factor_gt[j][k]-=Rgt[i]; }
                    }

                    else if(x.x_is[i].label == -1){

                        if(labels_lin[i][j] != labels_lin[i][k]) { bin_factor[j][k]-=R[i]; bin_factor_gt[j][k]-=Rgt[i]; }
                        if(labels_mvc[i][j] != labels_mvc[i][k]) { bin_factor[j][k]+=R[i]; bin_factor_gt[j][k]+=Rgt[i]; }
                    }

                }

            }

        }

    }


    //current_utc_time(&finish_t);
    //time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\n\tTime taken to get factors to compute phi: %f",time_taken);

    /*for (i=0;i<num_ex;i++){
        printf("\n");

        for(j=0;j<num_ex;j++){
            printf("%d ",labels_mvc[i][j]);
        }
        printf("\n");
        for(j=0;j<num_ex;j++){
            printf("%d ",labels_lin[i][j]);
        }

        printf("\n");
    }
    for (i=0;i<num_ex;i++){
        printf("%d:%d ",up_factor[i],un_factor[i]);
    }
    printf("\n");*/

    //current_utc_time(&start_t);


    phibar = psi_with_weights(x,up_factor, un_factor, bin_factor, up_factor_gt, un_factor_gt, bin_factor_gt, sm, sparm, &philin_temp);

    //current_utc_time(&finish_t);
    //time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\n\tTime taken to compute phibar and philin both (using psi_with_weights) : %f",time_taken);

    norm_factor = 1.0/(double)(x.n_pos*x.n_neg);

    temp1 = smult_s(phibar, norm_factor);
    free_svector(phibar);
    phibar = copy_svector(temp1);
    free_svector(temp1);


    temp1 = copy_svector(philin_temp);
    free_svector(philin_temp);
    philin_temp = smult_s(temp1, norm_factor);
    free_svector(temp1);

    *philin = philin_temp;

    /*double norm_phi = sprod_ss(fvec, fvec);
    double norm_phibar = sprod_ss(fvec_bar, fvec_bar);

    printf("\n\t%f %f",norm_phi, norm_phibar);*/

    for(i=0;i<num_ex;i++){
        free(bin_factor[i]);
        free(bin_factor_gt[i]);
    }
    free(bin_factor);
    free(bin_factor_gt);
    free(R);
    free(up_factor);
    free(un_factor);
    free(Rgt);
    free(up_factor_gt);
    free(un_factor_gt);


    return(phibar);
}


void get_unary(PATTERN x, STRUCTMODEL *sm, double **up, double **un) {

    int i;

    int num_ex = x.n_pos+x.n_neg;
    int graphID, num_nodes;
    double *unary_pos = (double*)malloc(num_ex*sizeof(double));
    double *unary_neg =  (double*)malloc(num_ex*sizeof(double));
    double up_max = 0.0, up_min = 0.0, un_max = 0.0, un_min = 0.0;

    double factor = MAX(num_ex, x.n_neighbors);

    for (i = 0; i < num_ex; i++){

        graphID = x.graph.graphID[i];
        //num_nodes = x.graph.subgraph[graphID].num_nodes;
        //num_nodes = num_ex; //CHANGE
        num_nodes = 1;        

        // compute unary potential for ybar.labels[i] == 1
        unary_pos[i] = sprod_ns(sm->w, x.x_is[i].phi1phi2_neg)/(double)(num_nodes);
        unary_neg[i] = sprod_ns(sm->w, x.x_is[i].phi1phi2_pos)/(double)(num_nodes);


        if(unary_pos[i] <= up_min){
            up_min = unary_pos[i];
        }
        if(unary_pos[i] > up_max){
            up_max = unary_pos[i];
        }
        if(unary_neg[i] <= un_min){
            un_min = unary_neg[i];
        }
        if(unary_neg[i] > un_max){
            un_max = unary_neg[i];
        }
    }

    //printf("\nUnary Pos\tMin = %f\tMax:%f", up_min, up_max);
    //printf("\nUnary Neg\tMin = %f\tMax:%f", un_min, un_max);
    //printf("\nnum_nodes: %d", num_nodes);

    *up = unary_pos;
    *un = unary_neg;

}

double **get_binary(PATTERN x, STRUCTMODEL *sm) {

    int i, j;
    SVECTOR *temp_sub=NULL;
    int num_ex = x.n_pos+x.n_neg;
    int graphID, num_edges;
    double **binary =  (double**)malloc((num_ex)*sizeof(double*));
    double binary_pose = 0.0, binary_gist = 0.0;
    double bin_max = 0.0, bin_min = 0.0;

    //SVECTOR *temp_sub_shift ;

    for (i = 0; i < num_ex; i++){
        binary[i] =  (double*)calloc(num_ex, sizeof(double));
    }

    double factor = MAX(num_ex, x.n_neighbors);

    for (i = 0; i < num_ex; i++){

        graphID = x.graph.graphID[i];
        //num_edges = x.graph.subgraph[graphID].num_edges;
        //num_edges = x.n_neighbors; //CHANGE
        num_edges = 1;
        //num_edges = num_ex;


        for (j = (i+1); j < num_ex; j++){
            if(x.neighbors[i][j]){

                if(sm->psi2_type == 1){ //only poselet

                    temp_sub = exp_add_ss_pow(x.x_is[i].phi1_shift, x.x_is[j].phi1_shift, -1.0, 2.0, sm->pose_sigma );
                    binary_pose = sprod_ns(sm->w, temp_sub);
                    binary_gist = 0.0;
                    free_svector(temp_sub);

                }else if(sm->psi2_type == 2){ //only gist

                    temp_sub = exp_add_ss_pow(x.x_is[i].phi2_shift, x.x_is[j].phi2_shift, -1.0, 2.0, sm->gist_sigma );
                    binary_gist = sprod_ns(sm->w, temp_sub);
                    binary_pose = 0.0;
                    free_svector(temp_sub);

                }else{ // [poselet;gist]

                    //// add phi1, phi2 to get psi2 (note that they are already indexed)
                    temp_sub = exp_add_ss_pow(x.x_is[i].phi1_shift, x.x_is[j].phi1_shift, -1.0, 2.0, sm->pose_sigma );
                    binary_pose = sprod_ns(sm->w, temp_sub);
                    free_svector(temp_sub);

                    temp_sub = exp_add_ss_pow(x.x_is[i].phi2_shift, x.x_is[j].phi2_shift, -1.0, 2.0, sm->gist_sigma );
                    binary_gist = sprod_ns(sm->w, temp_sub);
                    free_svector(temp_sub);
                }

                binary[i][j] =   sm->binary_weight*(binary_pose + binary_gist)/(double)(num_edges);                

                bin_max = MAX(bin_max,binary[i][j]);
                bin_min = MIN(bin_max,binary[i][j]);

                if (binary[i][j]>0.0) {binary[i][j] = 0.0;}

                binary[i][j] *= -1.0;

            }
            else{
                binary[i][j] = 0.0;
            }
        }
    }
    if(bin_max > 1E-10){
        printf("\nCAUTION Binary>1E-10, binary (max) = %.10g\n", bin_max);
    }

    //printf("\nnum_edges: %d", num_edges);
    //printf("\nBinary score\tMin = %f\tMax:%f\n", bin_min,bin_max);

    return binary;
}


SVECTOR** create_feature_cache_dynamic(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, int **labels){
    /* this method is based on subgraphs, i.e., dynamic psi for each subgraph independently */

    LABEL y, ystar;
    SVECTOR **fycache;
    struct timespec start_t,finish_t;

    int i, j, num_ex, index, last_index, num_nodes;
    double time_taken = 0.0, total_time_taken = 0.0;

    num_ex = x.n_neg + x.n_pos;
    fycache = (SVECTOR**)malloc(num_ex*sizeof(SVECTOR*));

    for (i=0;i<x.graph.num_subgraphs;i++){

        current_utc_time(&start_t);
        num_nodes = x.graph.subgraph[i].num_nodes;

        for(j=0;j<num_nodes;j++){
            index = x.graph.subgraph[i].node_index[j];

            if(j==0){
                ystar.labels = labels[index];
                fycache[index] = psi(x, ystar, x.graph.subgraph[i].mask, sm, sparm); // highly expensive step
                last_index = index;
            }else{
                y.labels = labels[last_index];
                ystar.labels = labels[index];
                //fycache[index] = psi(x, ystar, x.graph.subgraph[i].mask, sm, sparm); // highly expensive step
                fycache[index] = dynamic_psi(x, fycache[last_index], y, ystar, x.graph.subgraph[i].mask, sm, sparm); // highly expensive step
                last_index = index;
            }
        }

        current_utc_time(&finish_t);
        time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
        total_time_taken+=time_taken;
        //printf("\t\tFeature vector cache created (subgraph/total subgraphs) :%05d/%05d.\tTime taken:%.04f/%.04f\r",i+1,x.graph.num_subgraphs,time_taken,total_time_taken);fflush(stdout);
    }

    if(sparm->verbosity){
        //printf("\t\tFeature vector cache created:%05d/%05d.\tTime taken:%.04f/%.04f\n",i+1,num_ex,time_taken,total_time_taken);fflush(stdout);
    }

    //printf("\n");
    return fycache;
}

SVECTOR** create_feature_cache_nodynamic(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, int **labels){

    LABEL ystar;
    SVECTOR **fycache;
    struct timespec start_t,finish_t;

    int i, j, num_ex, index,num_nodes;
    double time_taken = 0.0, total_time_taken = 0.0;

    num_ex = x.n_neg + x.n_pos;
    fycache = (SVECTOR**)malloc(num_ex*sizeof(SVECTOR*));

    for (i=0;i<x.graph.num_subgraphs;i++){

        current_utc_time(&start_t);
        num_nodes = x.graph.subgraph[i].num_nodes;

        for(j=0;j<num_nodes;j++){
            index = x.graph.subgraph[i].node_index[j];
            ystar.labels = labels[index];
            fycache[index] = psi(x, ystar, x.graph.subgraph[i].mask, sm, sparm); // highly expensive step
        }

        current_utc_time(&finish_t);
        time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
        total_time_taken+=time_taken;
        //printf("\t\tFeature vector cache created (subgraph/total subgraphs) :%05d/%05d.\tTime taken:%.04f/%.04f\r",i+1,x.graph.num_subgraphs,time_taken,total_time_taken);fflush(stdout);
    }

    if(sparm->verbosity){
        //printf("\t\tFeature vector cache created:%05d/%05d.\tTime taken:%.04f/%.04f\n",i+1,num_ex,time_taken,total_time_taken);fflush(stdout);
    }

    //printf("\n");
    return fycache;
}


void encode_ranking(PATTERN x, LABEL y, int *ranking_encoded, SAMPLE_SCORE *posSampleScores, SAMPLE_SCORE *negSampleScores, int *sampleIndexMap, int *optimumLocNegImg){

    int i, j, i_prime, j_prime, oj_prime, oi_prime;

    for(i = 0; i < (x.n_pos+x.n_neg); i++){
        for(j = i+1; j < (x.n_pos+x.n_neg); j++){
            if(i == j){
                //ybar->rank_matrix[i][j] = 0;
            }
            else if(y.labels[i] == y.labels[j]){
                if(y.labels[i] == 1){
                    if(posSampleScores[sampleIndexMap[i]].score > posSampleScores[sampleIndexMap[j]].score){
                        //ybar->rank_matrix[i][j] = 1;
                        ranking_encoded[i]++;
                        ranking_encoded[j]--;
                    }
                    else if(posSampleScores[sampleIndexMap[j]].score > posSampleScores[sampleIndexMap[i]].score){
                        //ybar->rank_matrix[i][j] = -1;
                        ranking_encoded[i]--;
                        ranking_encoded[j]++;
                    }
                    else{
                        if(i < j){
                            //ybar->rank_matrix[i][j] = 1;
                            ranking_encoded[i]++;
                            ranking_encoded[j]--;
                        }
                        else{
                            //ybar->rank_matrix[i][j] = -1;
                            ranking_encoded[i]--;
                            ranking_encoded[j]++;
                        }
                    }
                }
                else{
                    if(negSampleScores[sampleIndexMap[i]].score > negSampleScores[sampleIndexMap[j]].score){
                        //ybar->rank_matrix[i][j] = 1;
                        ranking_encoded[i]++;
                        ranking_encoded[j]--;
                    }
                    else if(negSampleScores[sampleIndexMap[j]].score > negSampleScores[sampleIndexMap[i]].score){
                        //ybar->rank_matrix[i][j] = -1;
                        ranking_encoded[i]--;
                        ranking_encoded[j]++;
                    }
                    else{
                        if(i < j){
                            //ybar->rank_matrix[i][j] = 1;
                            ranking_encoded[i]++;
                            ranking_encoded[j]--;
                        }
                        else{
                            //ybar->rank_matrix[i][j] = -1;
                            ranking_encoded[i]--;
                            ranking_encoded[j]++;
                        }
                    }
                }
            }
            else if((y.labels[i] == 1) && (y.labels[j] == -1)){
                i_prime = sampleIndexMap[i]+1;
                j_prime = sampleIndexMap[j]+1;
                oj_prime = optimumLocNegImg[j_prime-1];

                //printf("%d:%d:%d\t",i_prime, j_prime, oj_prime);

                if((oj_prime - i_prime - 0.5) > 0.0){
                    //ybar->rank_matrix[i][j] = 1;
                    ranking_encoded[i]++;
                    ranking_encoded[j]--;
                }
                else{
                    //ybar->rank_matrix[i][j] = -1;
                    ranking_encoded[i]--;
                    ranking_encoded[j]++;
                }
            }
            else if((y.labels[i] == -1) && (y.labels[j]== 1)){
                i_prime = sampleIndexMap[i]+1;
                j_prime = sampleIndexMap[j]+1;
                oi_prime = optimumLocNegImg[i_prime-1];

                if((j_prime - oi_prime + 0.5) > 0.0){
                    //ybar->rank_matrix[i][j] = 1;
                    ranking_encoded[i]++;
                    ranking_encoded[j]--;
                }
                else{
                    //ybar->rank_matrix[i][j] = -1;
                    ranking_encoded[i]--;
                    ranking_encoded[j]++;
                }
            }
        }
    }
}


void find_optimum_neg_locations(PATTERN x, LABEL y, int *ranking_encoded, SAMPLE_SCORE *posSampleScores, SAMPLE_SCORE *negSampleScores, int *sampleIndexMap, SAMPLE_SCORE *scores){

    int j, k;
    int *optimumLocNegImg = (int*)malloc(x.n_neg*sizeof(int));
    if(!optimumLocNegImg) die("Memory error");

    double maxValue = 0;
    double currentValue = 0;
    int maxIndex = -1;
    // for every jth negative image
    for(j = 1; j <= x.n_neg; j++){
        maxValue = 0;
        maxIndex = x.n_pos+1;
        // k is what we are maximising over. There would be one k_max for each negative sample j
        currentValue = 0.0;
        for(k = x.n_pos; k >= 1; k--){
            currentValue += (1.0/(double)x.n_pos)*(((double) j/(double)(j+k))-((double) (j-1)/(double)(j+k-1))) - (2.0/(double)(x.n_pos*x.n_neg))*(posSampleScores[k-1].score - negSampleScores[j-1].score);
            if(currentValue > maxValue){
                maxValue = currentValue;
                maxIndex = k;
            }
        }
        optimumLocNegImg[j-1] = maxIndex;
    }

    encode_ranking(x, y, ranking_encoded, posSampleScores, negSampleScores, sampleIndexMap, optimumLocNegImg);
    free(optimumLocNegImg);
}

int* find_mvc_hoap_svm_dgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **fycache_lin, double *pred_score, int **labels_lin, SVECTOR **phibar, SVECTOR **philin)
/* finding the most violated constraint for the HOAP-SVM. Dynamic graph cut has been used to calculate the max-marginal scores. We
modified the Kolmogorov's max flow code in order to convert it into dynamic graph cut code */
{
    int i,j,num_ex;
    double mmi_bar = 0.0, mmi = 0.0, score = 0.0;
    double *unary_pos, *unary_neg, **binary = NULL;
    //double *flow;
    num_ex = x.n_pos+x.n_neg;
    double *mm_scores = (double*) calloc (num_ex, sizeof(double));    

    /* find the unary and the pairwise potentials */
    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);    

    SAMPLE_SCORE *posSampleScores = (SAMPLE_SCORE*) malloc(x.n_pos*sizeof(SAMPLE_SCORE));
    if(!posSampleScores) die("Memory error");
    SAMPLE_SCORE *negSampleScores = (SAMPLE_SCORE*) malloc(x.n_neg*sizeof(SAMPLE_SCORE));
    if(!negSampleScores) die("Memory error");
    SAMPLE_SCORE *scores = (SAMPLE_SCORE*) malloc(num_ex*sizeof(SAMPLE_SCORE));
    if(!scores) die("Memory error");
    int *sampleIndexMap = (int*) malloc(num_ex*sizeof(*sampleIndexMap));
    if(!sampleIndexMap) die("Memory error");

    int negativeId = 0;
    int positiveId = 0;


    int  **mm_labels_w;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));

    for (i = 0; i < num_ex; i++){

        if(y.labels[i] == 1){
            up_new[i] = MARGINAL_INF; // y1 = -1
            un_new[i] = unary_neg[i];
        }else{
            up_new[i] = unary_pos[i]; // y2 = 1
            un_new[i] = MARGINAL_INF;
        }
    }

    /* find the labeling corresponding to current w, mmi = -1, mmj = 1 */
    mm_labels_w = dgc_maxmarginal_labels(unary_pos, unary_neg, binary, x.n_pos, x.n_neg, up_new, un_new);
    //mm_labels_w = dgc_maxmarginal_labels_with_flow(unary_pos, unary_neg, binary, x.n_pos, x.n_neg, up_new, un_new, &flow);

    //based on subgraphs, dgc independently on each subgraph
    //mm_labels_w = dgc_subgraph_based_mvc(x, sm, sparm, unary_pos, unary_neg, up_new, un_new, binary, &flow);

    /* max-marginal scores using labeling */    
    for (i=0; i<num_ex;i++){
        mm_scores[i] = get_score_from_labeling_mvc(x, i, unary_pos, unary_neg, binary, mm_labels_w[i]);
    }

    int i_prime = 0, j_prime = 0;
    for(i = 0; i < num_ex; i++){

        mmi_bar = sprod_ns(sm->w,fycache_lin[i]);
        mmi = mm_scores[i];

        if(y.labels[i] == 1){

            score = mmi_bar - mmi;

            posSampleScores[positiveId].idx = i;
            posSampleScores[positiveId].score = score;
            positiveId++;

            scores[i].idx = i_prime; // not using right now
            scores[i].score = score;
            i_prime++;

        }else{

            score = mmi - mmi_bar;

            negSampleScores[negativeId].idx = i;
            negSampleScores[negativeId].score = score;
            negativeId++;

            scores[i].idx = j_prime;  // not using right now
            scores[i].score = score;
            j_prime++;
        }

    }

    qsort(negSampleScores, x.n_neg, sizeof(SAMPLE_SCORE), img_score_comp);
    qsort(posSampleScores, x.n_pos, sizeof(SAMPLE_SCORE), img_score_comp);

    negativeId = 0;
    positiveId = 0;
    for(i = 0; i < num_ex; i++){
        if(y.labels[i] == 1){
            sampleIndexMap[posSampleScores[positiveId].idx] = positiveId;
            positiveId++;
        }
        else{
            sampleIndexMap[negSampleScores[negativeId].idx] = negativeId;
            negativeId++;
        }
    }

    int *ranking_encoded = (int*) calloc(num_ex,sizeof(int));

    find_optimum_neg_locations(x, y, ranking_encoded, posSampleScores, negSampleScores, sampleIndexMap, scores);


    //*phibar = phi_combined_faster_bkup(x, labels_lin, mm_labels_w, ranking_encoded, sm, sparm);
    //*phi = phi_combined_faster_bkup(x, labels_lin, mm_labels_w, y.ranking, sm, sparm);

    SVECTOR *philin_temp;
    *phibar = phi_combined_faster(x, labels_lin, mm_labels_w, ranking_encoded, y.ranking, sm, sparm, &philin_temp);
    *philin = philin_temp;  


    free(posSampleScores);
    free(negSampleScores);
    //free(flow);
    free(sampleIndexMap);
    free(scores);
    free(unary_pos);
    free(unary_neg);
    free(up_new);
    free(un_new);

    for (i = 0; i < num_ex; i++){
        free(binary[i]);
        free(mm_labels_w[i]);
    }
    free(binary);
    free(mm_labels_w);
    free(mm_scores);

    return(ranking_encoded);
}



int **find_mvc_m4svm_dgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR ***fybarcache, int *pred_labels) {


    int i, j,num_ex;

    num_ex = x.n_pos+x.n_neg;


    SVECTOR **fymmcache, **fymmcache_pos, **fymmcache_neg;
    LABEL ybar;
    double loss_pos, loss_neg, loss_avg;

    double mm_pos_score = 0.0, mm_neg_score = 0.0;
    double *unary_pos, *unary_neg, **binary = NULL;

    int **mm_labels_neg, **mm_labels_pos, **mvc_labels;
    mvc_labels = (int **) malloc(num_ex*sizeof(int*));

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    mm_labels_pos = dgc_maxmarginal_labels_pos(unary_pos, unary_neg, binary, x.n_pos, x.n_neg);
    mm_labels_neg = dgc_maxmarginal_labels_neg(unary_pos, unary_neg, binary, x.n_pos, x.n_neg);

    //printf("\nFinding max-marginal labeling (pos)\n");

    /* final mvc labels and create fymmcache correspoding to that */
    fymmcache_pos = create_feature_cache_dynamic(x, sm, sparm, mm_labels_pos);
    fymmcache_neg = create_feature_cache_dynamic(x, sm, sparm, mm_labels_neg);
    fymmcache = (SVECTOR**)malloc(num_ex*sizeof(SVECTOR*));


    for(i = 0; i < num_ex; i++){

        mm_neg_score = sprod_ns(sm->w,fymmcache_neg[i]);
        mm_pos_score = sprod_ns(sm->w,fymmcache_pos[i]);

        mvc_labels[i] = (int*) malloc (num_ex*sizeof(int));

        //sanity check
        /*if(mm_labels_neg[i][i] != -1 || mm_labels_pos[i][i] != 1 ){
            printf("\nInside MVC: max-marginal labeling incorrect");
        }*/

        if(mm_pos_score>mm_neg_score){
            pred_labels[i] = 1;
        }else{
            pred_labels[i] = -1;
        }


        if(y.labels[i] == 1){
            mm_neg_score += (double) 1.0;
            //mm_neg_score += (double) 100.0;

        }else{
            mm_pos_score += (double) 1.0;
            //mm_pos_score += (double) 100.0;
        }
        //printf("pos score:%.6g\tneg_score:.%6g\n",mm_pos_score,mm_neg_score);

        if(mm_pos_score > mm_neg_score){
            for (j = 0; j < num_ex; j++){
                mvc_labels[i][j] = mm_labels_pos[i][j];
            }
            fymmcache[i] = copy_svector(fymmcache_pos[i]);

        }else{
            for (j = 0; j < num_ex; j++){
                mvc_labels[i][j] = mm_labels_neg[i][j];
            }
            fymmcache[i] = copy_svector(fymmcache_neg[i]);

        }

        /* only yi_bar = yi */
        /*for (j = 0; j < (x.n_pos+x.n_neg); j++){
            mvc_labels[i][j] = mm_labels_neg[i][j];
        }
        fymmcache[i] = copy_svector(fymmcache_neg[i]);*/


        free_svector(fymmcache_neg[i]);
        free_svector(fymmcache_pos[i]);

    }

    free(fymmcache_neg);
    free(fymmcache_pos);

    *fybarcache = fymmcache;

    free(unary_pos);
    free(unary_neg);
    for (i = 0; i < num_ex; i++){
        free(binary[i]);
        free(mm_labels_neg[i]);
        free(mm_labels_pos[i]);
    }
    free(binary);
    free(mm_labels_neg);
    free(mm_labels_pos);

    /*free(up_new_neg);
    free(un_new_neg);
    free(up_new_pos);
    free(un_new_pos);*/

    //clock_gettime(CLOCK_REALTIME, &finish_t);
    //time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\ntime taken by fmvc: %f",time_taken);

    return mvc_labels;
}

int **find_mvc_m4svm_nodgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR ***fybarcache, int *pred_labels) {


    int i,j,num_ex, ii;

    num_ex = x.n_pos+x.n_neg;


    SVECTOR **fymmcache, **fymmcache_pos, **fymmcache_neg;
    double mm_pos_score = 0.0, mm_neg_score = 0.0;


    double *unary_pos, *unary_neg, **binary = NULL;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));
    int **mm_labels_neg, **mm_labels_pos, **mvc_labels;

    mvc_labels = (int **) malloc(num_ex*sizeof(int*));
    mm_labels_pos = (int **) malloc(num_ex*sizeof(int*));
    mm_labels_neg = (int **) malloc(num_ex*sizeof(int*));

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    for (i = 0; i < num_ex; i++){
        up_new[i] = unary_pos[i];
        un_new[i] = unary_neg[i];
    }

    /* non-dynamic max-marginal labeling */

    for (ii = 0; ii < num_ex; ii++){

        // max marginal condition for the sample being postive y = 1
        up_new[ii] = -MARGINAL_INF;
        un_new[ii] = MARGINAL_INF;
        mm_labels_pos[ii] = maxflow_labels(up_new, un_new, binary, x.n_pos, x.n_neg);

        // max marginal condition for the sample being negative y = -1
        up_new[ii] = MARGINAL_INF;
        un_new[ii] = -MARGINAL_INF;

        mm_labels_neg[ii] = maxflow_labels(up_new, un_new, binary, x.n_pos, x.n_neg);

        // reverting the changes
        up_new[ii] = unary_pos[ii];
        un_new[ii] = unary_neg[ii];

        // sanity check
        if(mm_labels_pos[ii][ii] != 1 || mm_labels_neg[ii][ii] != -1){
            printf("\n\t\t---- Sanity check FAILED (In FMVC_NODGC: max-marginal labelings incorrect) ---- \n\n");
        }

    }

    /* final mvc labels and create fymmcache correspoding to that */
    fymmcache_pos = create_feature_cache_dynamic(x, sm, sparm, mm_labels_pos);
    fymmcache_neg = create_feature_cache_dynamic(x, sm, sparm, mm_labels_neg);
    fymmcache = (SVECTOR**)malloc(num_ex*sizeof(SVECTOR*));

    for(i = 0; i < num_ex; i++){

        mm_neg_score = sprod_ns(sm->w,fymmcache_neg[i]);
        mm_pos_score = sprod_ns(sm->w,fymmcache_pos[i]);

        mvc_labels[i] = (int*) malloc (num_ex*sizeof(int));

        //sanity check
        /*if(mm_labels_neg[i][i] != -1 || mm_labels_pos[i][i] != 1 ){
            printf("\nInside MVC: max-marginal labeling incorrect");
        }*/
        if(mm_pos_score>=mm_neg_score){
            pred_labels[i] = 1;
        }else{
            pred_labels[i] = -1;
        }

        if(y.labels[i] == 1){
            mm_neg_score += (double) 1.0;
        }else{
            mm_pos_score += (double) 1.0;
        }
        //printf("pos score:%.6g\tneg_score:.%6g\n",mm_pos_score,mm_neg_score);

        if(mm_pos_score > mm_neg_score){
            for (j = 0; j < num_ex; j++){
                mvc_labels[i][j] = mm_labels_pos[i][j];
            }
            fymmcache[i] = copy_svector(fymmcache_pos[i]);

        }else{
            for (j = 0; j < num_ex; j++){
                mvc_labels[i][j] = mm_labels_neg[i][j];
            }
            fymmcache[i] = copy_svector(fymmcache_neg[i]);

        }

        free_svector(fymmcache_neg[i]);
        free_svector(fymmcache_pos[i]);

    }

    free(fymmcache_neg);
    free(fymmcache_pos);

    *fybarcache = fymmcache;

    free(up_new);
    free(un_new);
    free(unary_pos);
    free(unary_neg);


    for (i = 0; i < num_ex; i++){
        free(binary[i]);
        free(mm_labels_neg[i]);
        free(mm_labels_pos[i]);
    }
    free(binary);
    free(mm_labels_neg);
    free(mm_labels_pos);


    //clock_gettime(CLOCK_REALTIME, &finish_t);
    //time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\ntime taken by fmvc: %f",time_taken);

    return mvc_labels;

}

void find_mvc_hob_svm(PATTERN x, LABEL y, LABEL *ybar, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, int **pred_labels)
/* finding the most violated constraint for the HOB-SVM */
{
    /*struct timespec start_t,finish_t;
    double time_taken;
    clock_gettime(CLOCK_REALTIME, &start_t);*/

    int i, num_ex;
    double J = sparm->J;
    double *unary_pos, *unary_neg, **binary;

    num_ex = (x.n_pos+x.n_neg);
    get_unary(x,  sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    *pred_labels = maxflow_labels(unary_pos, unary_neg, binary, x.n_pos, x.n_neg); // for sanity check

    for (i = 0; i < num_ex ; i++){

        if(y.labels[i] == 1){
            // add 1/n to 'ybar == -1' unary term
            //unary_neg[i] -= (double)J/(double)(J*x.n_pos+x.n_neg);
             //unary_neg[i] -= 100.0*(double)J;
            unary_neg[i] -= (double)J;
        }
        else{
            // add 1/n to 'ybar == 1' unary term
            //unary_pos[i] -= (double)1.0/(double)(J*x.n_pos+x.n_neg);            
            //unary_pos[i] -= 100.0*(double)1.0;
            unary_pos[i] -= (double)1.0;
        }
    }

    ybar->labels = maxflow_labels(unary_pos, unary_neg, binary, x.n_pos, x.n_neg);

    free(unary_pos);
    free(unary_neg);
    for (i = 0; i < (x.n_pos+x.n_neg); i++){
        free(binary[i]);
    }
    free(binary);

    /*clock_gettime(CLOCK_REALTIME, &finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    printf("\ntime taken by fmvc: %f",time_taken);*/

    return;

}

int **linearize_concave_part(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {


    printf("\n\tLinearization using dynamic graph cut - subgraph based");
    int i,j;
    int num_ex = x.n_pos+x.n_neg;

    double *unary_pos, *unary_neg, **binary;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));



    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    for (i = 0; i < num_ex; i++){

        if(y.labels[i] == 1){
            up_new[i] = unary_pos[i]; // y1 = 1;
            un_new[i] = MARGINAL_INF;
        }else{

            up_new[i] = MARGINAL_INF; // y1 = -1;
            un_new[i] = unary_neg[i];

        }
    }

    double *flow;
    int **mm_labels_lin = dgc_maxmarginal_labels_with_flow(unary_pos, unary_neg, binary, x.n_pos, x.n_neg, up_new, un_new, &flow);


    free(unary_pos);
    free(unary_neg);
    free(up_new);
    free(un_new);
    for (i = 0; i < (x.n_pos+x.n_neg); i++){
        free(binary[i]);
    }
    free(binary);
    free(flow);

    return mm_labels_lin;
}

int **linearize_concave_part_nodgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {

    printf("\n\tLinearization without dynamic graph cut");

    int i, index;
    int num_ex = x.n_pos+x.n_neg;


    int **labels_star = (int**)malloc(num_ex*sizeof(int*));
    double *unary_pos, *unary_neg, **binary;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    for (i = 0; i < num_ex; i++){

        up_new[i] = unary_pos[i];
        un_new[i] = unary_neg[i];
    }

    for (i = 0; i < num_ex; i++){

        if(y.labels[i] == 1){
            up_new[i] = unary_pos[i];
            un_new[i] = MARGINAL_INF;

        }else{
            up_new[i] = MARGINAL_INF;
            un_new[i] = unary_neg[i];
        }

        index = i-1;
        if(i>0){
            up_new[index] = unary_pos[index];
            un_new[index] = unary_neg[index];

        }

        labels_star[i] = maxflow_labels(up_new, un_new, binary, x.n_pos, x.n_neg);
    }


    free(unary_pos);
    free(unary_neg);
    free(up_new);
    free(un_new);
    for (i = 0; i < (x.n_pos+x.n_neg); i++){
        free(binary[i]);
    }
    free(binary);

    return labels_star;
}


/* loss functions */

double loss_map(int *true_labels, double *scores, int num_ex, STRUCT_LEARN_PARM *sparm) {

    double l = 0.0;
    int i;
    int posCount = 0;
    int totalCount = 0;
    double precisionAti = 0;
    int label;

    SAMPLE_SCORE *sorted_scores = (SAMPLE_SCORE*) malloc(num_ex*sizeof(SAMPLE_SCORE));

    // sort the scores and keep track of the indices    
    for(i = 0; i < num_ex; i++){
        sorted_scores[i].score = scores[i];
        sorted_scores[i].idx = i;
    }
    qsort(sorted_scores, num_ex, sizeof(SAMPLE_SCORE), img_score_comp);

    // calculate map
    for(i = 0; i < num_ex; i++){
        label = true_labels[sorted_scores[i].idx];
        if(label == 1){
            posCount++;
            totalCount++;
        }
        else{
            totalCount++;
        }
        if(label == 1){
            precisionAti += (double)posCount/(double)totalCount;
        }
    }
    precisionAti = precisionAti/(double)posCount;
    l = 1.0 - precisionAti;
    free(sorted_scores);    

    return(l);

}

double loss_zero_one(LABEL y, LABEL ybar, STRUCT_LEARN_PARM *sparm, int ex_num) {

    double l  = 0.0;
    int ic_pos = 0;
    int ic_neg = 0;
    int i;

    if(sparm->A){
        if(y.labels[ex_num] == ybar.labels[ex_num]) l = 0.0;
        else l = 1.0;

    }else{
        for (i = 0; i < (y.n_pos+y.n_neg); i++){
            if (y.labels[i] != ybar.labels[i]){
                if(y.labels[i] == 1){
                    ic_pos++;
                }
                else{
                    ic_neg++;
                }
            }
        }

        //l = (double) (sparm->J*(double) ic_pos + (double) ic_neg)/(double)(sparm->J*(double) y.n_pos + (double) y.n_neg);
        l = (double) (sparm->J*(double) ic_pos + (double) ic_neg);
        //l = (double) (sparm->J*(double) ic_pos + (double) ic_neg)/(double)(y.n_pos + y.n_neg);
    }

    return(l);

}

double* classify_hob(PATTERN x, LABEL *y, double **max_marginals, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {

    int i, num_ex;
    double *unary_pos, *unary_neg, **binary;

    num_ex = x.n_pos+x.n_neg;

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    y->labels = maxflow_labels(unary_pos, unary_neg, binary, x.n_pos, x.n_neg);

    double *scores = (double *) malloc(sizeof(double)*num_ex);

    for (i = 0; i < num_ex; i++){
        scores[i] = max_marginals[i][0]-max_marginals[i][1];
        free(binary[i]);
    }

    /*for (i = 0; i < num_ex; i++){
        if(y->labels[i] == 1) scores[i] = unary_pos[i];
        else scores[i] = unary_neg[i];

        free(binary[i]);
    }*/

    free(unary_pos);
    free(unary_neg);
    free(binary);

    return scores;


}

double* classify_hoap(PATTERN x, LABEL *y, double **max_marginals, STRUCT_LEARN_PARM *sparm) {

    int i, num_ex;

    num_ex = x.n_pos+x.n_neg;

    int *labels = (int *) malloc(sizeof(int)*num_ex);
    double *scores = (double *) malloc(sizeof(double)*num_ex);

    for(i=0;i<num_ex;i++){

        if(max_marginals[i][0]>max_marginals[i][1]){
            labels[i] = 1;
        }else{
            labels[i] = -1;
        }
        scores[i] = max_marginals[i][0]-max_marginals[i][1];

        /*if(sparm->A == 1){
            scores[i] = MAX(max_marginals[i][0], max_marginals[i][1]);
        }else if(sparm->A == 2){
            scores[i] = max_marginals[i][0]-max_marginals[i][1];
        }*/
    }

    y->labels = labels;

    return scores;
}

void loss_classify(LABEL y, LABEL ybar, double *scores, STRUCT_LEARN_PARM *sparm, double *loss_avg, double *loss_weighted, double *ap) {

    double l_avg  = 0.0;
    double l_weighted  = 0.0;
    double avg_prec = 0.0;
    double J = sparm->J;

    int ic_pos = 0;
    int ic_neg = 0;
    int num_ex = y.n_pos+y.n_neg;

    int i;
    for (i = 0; i <num_ex; i++){
        if (y.labels[i] != ybar.labels[i]){
            if(y.labels[i] == 1){
                ic_pos++;
            }
            else{
                ic_neg++;
            }
        }
    }

    l_weighted = (double) (J*(double) ic_pos + (double) ic_neg)/(double)(J*(double) y.n_pos + (double) y.n_neg);
    l_avg = (double) (ic_pos + ic_neg)/(double)(y.n_pos + y.n_neg);
    avg_prec = 1.0 - loss_map(y.labels, scores,  num_ex, sparm);

    *loss_avg = l_avg;
    *loss_weighted = l_weighted;
    *ap = avg_prec;

    //printf("\n\t\tIncorrect pos labels: %d",ic_pos);
    //printf("\n\t\tIncorrect neg labels: %d\n",ic_neg);
}


void write_struct_model(char *file, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {
    /*
       Writes the learned weight vector sm->w to file after training.
       */
    FILE *modelfl;
    int i;

    modelfl = fopen(file,"w");
    if (modelfl==NULL) {
        printf("Cannot open model file %s for output!", file);
        exit(1);
    }

    fprintf(modelfl,"%lf\n",sm->primal_obj);
    fprintf(modelfl,"%d\n",sparm->num_edges_pp);
    fprintf(modelfl,"%d\n",sparm->num_edges_nn);
    fprintf(modelfl,"%d\n",sparm->num_edges_pn);
    fprintf(modelfl,"%ld\n",sm->sizePsi);
    fprintf(modelfl,"%ld\n",sm->len_w1);
    fprintf(modelfl,"%ld\n",sm->len_w2);
    fprintf(modelfl,"%f\n",sparm->svm_c);
    fprintf(modelfl,"%f\n",sparm->pose_thres);
    fprintf(modelfl,"%f\n",sparm->gist_thres);
    fprintf(modelfl,"%ld\n",sparm->num_neighbors);
    fprintf(modelfl,"%d\n",sm->cp_iter);
    fprintf(modelfl,"%d\n",sm->active_constraints);
    fprintf(modelfl,"%f\n",sm->total_time_taken);

    for (i=1;i<sm->sizePsi+1;i++) {
        //fprintf(modelfl, "%d:%.16g\n", i, sm->w[i]);
        fprintf(modelfl,"%.16g\n",sm->w[i]);
    }
    fclose(modelfl);

}

STRUCTMODEL read_struct_model(char *file, STRUCT_LEARN_PARM *sparm) {
    /*
       Reads in the learned model parameters from file into STRUCTMODEL sm.
       The input file format has to agree with the format in write_struct_model().
       */
    STRUCTMODEL sm;

    FILE *modelfl;
    long i, fnum;

    //double fweight;
    long sizePsi, len_w1, len_w2, num_neighbors;
    int cp_iter, active_constraints, num_edges_pp, num_edges_nn, num_edges_pn;
    double C, total_time_taken, pose_thres, gist_thres, primal_obj;

    modelfl = fopen(file,"r");
    if (modelfl==NULL) {
        printf("Cannot open model file %s for input!", file);
        exit(1);
    }

    fscanf(modelfl,"%lf",&primal_obj);
    fscanf(modelfl,"%d",&num_edges_pp);
    fscanf(modelfl,"%d",&num_edges_nn);
    fscanf(modelfl,"%d",&num_edges_pn);
    fscanf(modelfl,"%ld",&sizePsi);
    fscanf(modelfl,"%ld",&len_w1);
    fscanf(modelfl,"%ld",&len_w2);
    fscanf(modelfl,"%lf", &C);
    fscanf(modelfl,"%lf", &pose_thres);
    fscanf(modelfl,"%lf", &gist_thres);
    fscanf(modelfl,"%ld",&num_neighbors);
    fscanf(modelfl,"%d", &cp_iter);
    fscanf(modelfl,"%d", &active_constraints);
    fscanf(modelfl,"%lf", &total_time_taken);

    //sizePsi = 1;
    sm.w = (double*)calloc((sizePsi+1),sizeof(double));
    for (i=1;i<sizePsi+1;i++) {
        //sm.w[i] = 0.0;
        fscanf(modelfl, "%lf", &sm.w[i]);
    }

    /*while (!feof(modelfl)) {
      fscanf(modelfl, "%d:%lf", &fnum, &fweight);
      if(fnum > sizePsi) {
        sizePsi = fnum;
        sm.w = (double *)realloc(sm.w,(sizePsi+1)*sizeof(double));
      }
      sm.w[fnum] = fweight;
    }*/

    fclose(modelfl);

    //sm.sizePsi = sizePsi;
    //sm.len_w1 = len_w1;
    //sm.len_w2 = len_w2;
    sm.total_time_taken = total_time_taken;
    sm.active_constraints = active_constraints;
    sm.cp_iter = cp_iter;

    return(sm);

}


STRUCTMODEL read_struct_model_apsvm(char *file, STRUCT_LEARN_PARM *sparm) {
    /*
       Reads in the learned model parameters from file into STRUCTMODEL sm.
       The input file format has to agree with the format in write_struct_model().
       */
    STRUCTMODEL sm;

    FILE *modelfl;
    long i;

    //double fweight;
    long sizePsi;

    modelfl = fopen(file,"r");
    if (modelfl==NULL) {
        printf("Cannot open model file %s for input!", file);
        exit(1);
    }

    sizePsi = sparm->sizePsi;
    int lenAPSVMmodel = (sparm->phi1_size + sparm->phi2_size);
    sm.w = (double*)calloc((sizePsi+1),sizeof(double));
    for (i=1;i<=lenAPSVMmodel ; i++) {
        fscanf(modelfl, "%lf", &sm.w[i]);
    }

    /*while (!feof(modelfl)) {
      fscanf(modelfl, "%d:%lf", &fnum, &fweight);
      if(fnum > sizePsi) {
        sizePsi = fnum;
        sm.w = (double *)realloc(sm.w,(sizePsi+1)*sizeof(double));
      }
      sm.w[fnum] = fweight;
    }*/

    fclose(modelfl);


    //sm.total_time_taken = total_time_taken;
    //sm.active_constraints = active_constraints;
    //sm.cp_iter = cp_iter;

    return(sm);

}

void free_struct_model(STRUCTMODEL sm, STRUCT_LEARN_PARM *sparm) {
    /*
       Free any memory malloc'ed in STRUCTMODEL sm after training.
       */

    free(sm.w);

}

void free_pattern(PATTERN x) {
    /*
       Free any memory malloc'ed when creating pattern x.
       */

}

void free_label(LABEL y) {
    /*
       Free any memory malloc'ed when creating label y.
       */
    free(y.labels);

}

void free_struct_sample(SAMPLE sample) {
    /*
       Free the whole training sample.
       */
    int i;
    /*for (i=0;i<s.n;i++) {
        free_pattern(s.examples[i].x);
        free_label(s.examples[i].y);
    }
    free(s.examples);*/

    for(i = 0; i < sample.examples[0].n_imgs; i++){
        free_svector(sample.examples[0].x.x_is[i].phi1);
        free_svector(sample.examples[0].x.x_is[i].phi2);
        free_svector(sample.examples[0].x.x_is[i].phi1_shift);
        free_svector(sample.examples[0].x.x_is[i].phi2_shift);
        free_svector(sample.examples[0].x.x_is[i].phi1phi2_pos);
        free_svector(sample.examples[0].x.x_is[i].phi1phi2_neg);
        free_svector(sample.examples[0].x.x_is[i].phi1phi2_shift);

        free(sample.examples[0].x.neighbors[i]);
    }

    for(i = 0; i < sample.examples[0].x.graph.num_subgraphs; i++){
        free(sample.examples[0].x.graph.subgraph[i].mask);
        free(sample.examples[0].x.graph.subgraph[i].node_index);
    }
    free(sample.examples[0].x.graph.graphID);
    free(sample.examples[0].x.graph.subgraph);

    free(sample.examples[0].x.neighbors);
    free(sample.examples[0].y.labels);
    free(sample.examples[0].x.x_is);
    free(sample.examples[0].y.ranking);
    free(sample.examples);

}

void parse_struct_parameters(STRUCT_LEARN_PARM *sparm) {
    /*
       Parse parameters for structured output learning passed
       via the command line.
       */
    int i;

    /* set default */
    //sparm->phi1_size=24004;
    sparm->phi1_size=2404;
    sparm->phi2_size=512;

    sparm->pairwise_threshold=0;

    for (i=0;(i<sparm->custom_argc)&&((sparm->custom_argv[i])[0]=='-');i++) {
        switch ((sparm->custom_argv[i])[2]) {
        /* your code here */
        default: printf("\nUnrecognized option %s!\n\n", sparm->custom_argv[i]); exit(0);
        }
    }

}

void copy_label(LABEL l1, LABEL *l2)
{
}

void print_label(LABEL l, FILE	*flabel)
{
}

SVECTOR** create_feature_cache(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, LABEL ystar, int *label_mask){

    SVECTOR **fycache;
    fycache = (SVECTOR**)malloc(sizeof(SVECTOR*));
    struct timespec start_t,finish_t;
    double time_taken = 0.0;

    current_utc_time(&start_t);

    fycache[0] = psi(x, ystar, label_mask, sm, sparm);

    current_utc_time(&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    printf("\n\tFeature vector cache created.\tTime taken:%.4f\n",time_taken);
    return fycache;
}


/*SVECTOR* exp_add_ss_abs(SVECTOR *a, SVECTOR *b, double factor_add, double factor_exp )
// compute exp( factor_exp * abs( a + factor_add*b ) ) of two sparse vectors. exponential is done element wise

{
    SVECTOR *vec;
    register WORD *sum,*sumi;
    register WORD *ai,*bj;
    long veclength;

    ai=a->words;
    bj=b->words;
    veclength=0;
    while (ai->wnum && bj->wnum) {
        if(ai->wnum > bj->wnum) {
            veclength++;
            bj++;
        }
        else if (ai->wnum < bj->wnum) {
            veclength++;
            ai++;
        }
        else {
            veclength++;
            ai++;
            bj++;
        }
    }
    while (bj->wnum) {
        veclength++;
        bj++;
    }
    while (ai->wnum) {
        veclength++;
        ai++;
    }
    veclength++;

    sum=(WORD *)my_malloc(sizeof(WORD)*veclength);
    sumi=sum;
    ai=a->words;
    bj=b->words;
    while (ai->wnum && bj->wnum) {
        if(ai->wnum > bj->wnum) {
            (*sumi)=(*bj);
            sumi->weight*=factor_add;
            sumi->weight = fabs(sumi->weight);

            sumi->weight = exp( factor_exp*sumi->weight );

            sumi++;
            bj++;
        }
        else if (ai->wnum < bj->wnum) {
            (*sumi)=(*ai);
            sumi->weight = fabs(sumi->weight);
            sumi->weight = exp( factor_exp*sumi->weight );

            sumi++;
            ai++;
        }
        else {
            (*sumi)=(*ai);
            sumi->weight+=factor_add*bj->weight;
            sumi->weight = fabs(sumi->weight);
            sumi->weight = exp( factor_exp*sumi->weight );
            if(sumi->weight != 0)
                sumi++;
            ai++;
            bj++;
        }
    }
    while (bj->wnum) {
        (*sumi)=(*bj);
        sumi->weight*=factor_add;
        sumi->weight = fabs(sumi->weight);
        sumi->weight = exp( factor_exp*sumi->weight );
        sumi++;
        bj++;
    }
    while (ai->wnum) {
        (*sumi)=(*ai);
        sumi->weight = fabs(sumi->weight);
        sumi->weight = exp( factor_exp*sumi->weight );
        sumi++;
        ai++;
    }
    sumi->wnum=0;

    vec=create_svector(sum,"",1.0);
    free(sum);

    return(vec);
}*/

SVECTOR* exp_add_ss_pow(SVECTOR *a, SVECTOR *b, double factor_add, double factor_pow, double factor_exp )
/* compute exp( factor_exp * (a + factor_add*b)^factor_pow ) of two sparse vectors. */
/* Note: SVECTOR lists are not followed, but only the first
  SVECTOR is used */
{
    SVECTOR *vec;
    register WORD *sum,*sumi;
    register WORD *ai,*bj;
    long veclength;

    ai=a->words;
    bj=b->words;
    veclength=0;
    while (ai->wnum && bj->wnum) {
        if(ai->wnum > bj->wnum) {
            veclength++;
            bj++;
        }
        else if (ai->wnum < bj->wnum) {
            veclength++;
            ai++;
        }
        else {
            veclength++;
            ai++;
            bj++;
        }
    }
    while (bj->wnum) {
        veclength++;
        bj++;
    }
    while (ai->wnum) {
        veclength++;
        ai++;
    }
    veclength++;

    sum=(WORD *)my_malloc(sizeof(WORD)*veclength);
    sumi=sum;
    ai=a->words;
    bj=b->words;
    while (ai->wnum && bj->wnum) {
        if(ai->wnum > bj->wnum) {
            (*sumi)=(*bj);
            sumi->weight*=factor_add;
            //sumi->weight = fabs(sumi->weight);

            ////sumi->weight = pow(sumi->weight, factor_pow);
            ////sumi->weight = exp( factor_exp*sumi->weight );

            sumi->weight = fasterpow(sumi->weight, factor_pow);
            sumi->weight = fasterexp(factor_exp*sumi->weight );

            sumi++;
            bj++;
        }
        else if (ai->wnum < bj->wnum) {
            (*sumi)=(*ai);
            //sumi->weight = fabs(sumi->weight);

            ////sumi->weight = pow(sumi->weight, factor_pow);
            ////sumi->weight = exp( factor_exp*sumi->weight );

            sumi->weight = fasterpow(sumi->weight, factor_pow);
            sumi->weight = fasterexp(factor_exp*sumi->weight );

            sumi++;
            ai++;
        }
        else {
            (*sumi)=(*ai);
            sumi->weight+=factor_add*bj->weight;
            //sumi->weight = fabs(sumi->weight);

            ////sumi->weight = pow(sumi->weight, factor_pow);
            ////sumi->weight = exp( factor_exp*sumi->weight );

            sumi->weight = fasterpow(sumi->weight, factor_pow);
            sumi->weight = fasterexp(factor_exp*sumi->weight );

            if(sumi->weight != 0)
                sumi++;
            ai++;
            bj++;
        }
    }
    while (bj->wnum) {
        (*sumi)=(*bj);
        sumi->weight*=factor_add;
        //sumi->weight = fabs(sumi->weight);

        ////sumi->weight = pow(sumi->weight, factor_pow);
        ////sumi->weight = exp( factor_exp*sumi->weight );

        sumi->weight = fasterpow(sumi->weight, factor_pow);
        sumi->weight = fasterexp(factor_exp*sumi->weight );

        sumi++;
        bj++;
    }
    while (ai->wnum) {
        (*sumi)=(*ai);
        //sumi->weight = fabs(sumi->weight);

        ////sumi->weight = pow(sumi->weight, factor_pow);
        ////sumi->weight = exp( factor_exp*sumi->weight );

        sumi->weight = fasterpow(sumi->weight, factor_pow);
        sumi->weight = fasterexp(factor_exp*sumi->weight );
        sumi++;
        ai++;
    }
    sumi->wnum=0;

    vec=create_svector(sum,"",1.0);
    free(sum);

    return(vec);
}

double sprod_nn(double *a, double *b, long n) {
    double ans=0.0;
    long i;
    for (i=1;i<n+1;i++) {
        ans+=a[i]*b[i];
    }
    return(ans);
}


int** dgc_subgraph_based_mvc(PATTERN x,STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *unary_pos, double *unary_neg, double *up_new, double *un_new, double **binary, double **mm_scores){
    /* this method is based on subgraphs, i.e., dynamic psi for each subgraph independently */


    struct timespec start_t,finish_t;
    int num_ex = x.n_neg + x.n_pos;

    int i,j,index, num_nodes, **mm_labels_mvc;
    double time_taken = 0.0, total_time_taken = 0.0;
    double *mmi_w = (double *)calloc(num_ex, sizeof(double));


    mm_labels_mvc = (int**) malloc (num_ex*sizeof(int*));

    for(i = 0;i<num_ex; i++){
        mm_labels_mvc[i] = (int*)malloc(num_ex*sizeof(int));
        for (j=0;j<num_ex;j++){
            mm_labels_mvc[i][j] = x.x_is[j].label;
        }
    }

    for (i=0;i<x.graph.num_subgraphs;i++){

        num_nodes = x.graph.subgraph[i].num_nodes;
        if(num_nodes == 1){

            index = x.graph.subgraph[i].node_index[0];
            //printf("\t%d",index);
            if(x.x_is[index].label == 1) {
                mmi_w[index] = sprod_ns(sm->w,x.x_is[index].phi1phi2_neg);
                mm_labels_mvc[index][index] = -1;
            }
            else {
                mmi_w[index] = sprod_ns(sm->w,x.x_is[index].phi1phi2_pos);
                mm_labels_mvc[index][index] = 1;
            }

        }else{

            dgc_maxmarginal_labels_subgraph_flow(unary_pos, unary_neg, binary, up_new, un_new, num_nodes, num_ex, x.graph.subgraph[i].node_index, mmi_w, mm_labels_mvc);

        }

        current_utc_time(&finish_t);
        time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
        total_time_taken+=time_taken;
        //printf("\t\tFeature vector cache created (subgraph/total subgraphs) :%05d/%05d.\tTime taken:%.04f/%.04f\r",i+1,x.graph.num_subgraphs,time_taken,total_time_taken);fflush(stdout);
    }

    if(sparm->verbosity){
        //printf("\t\tFeature vector cache created:%05d/%05d.\tTime taken:%.04f/%.04f\n",i+1,num_ex,time_taken,total_time_taken);fflush(stdout);
    }

    //printf("\n");
    *mm_scores = mmi_w;
    return mm_labels_mvc;

}


void check_dynamic_graph_cut_check(PATTERN x, LABEL y, STRUCTMODEL *sm){


    int i,j,num_ex = x.n_pos+x.n_neg;


    double *unary_pos, *unary_neg, **binary;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    struct timespec start_t,finish_t;
    double time_taken;
    //double *mm_scores;

    // no dynamic graph-cut

    for (i = 0; i < num_ex; i++){

        up_new[i] = unary_pos[i];
        un_new[i] = unary_neg[i];
    }


    int ii, **mm_labels_nodgc;
    double *mm_scores_nodgc = (double*)calloc(num_ex,sizeof(double));
    double flow;

    mm_labels_nodgc = (int**) malloc(num_ex*sizeof(int*));

    current_utc_time(&start_t);

    for (ii = 0; ii < num_ex; ii++){


        if(x.x_is[ii].label == 1){
            up_new[ii] = unary_pos[ii]; // y1 = 1
            un_new[ii] = MARGINAL_INF;

            mm_labels_nodgc[ii] = maxflow_labels_withflow(up_new, un_new, binary, x.n_pos, x.n_neg, &flow);

            up_new[ii] = unary_pos[ii];
            un_new[ii] = unary_neg[ii];

            mm_scores_nodgc[ii] = flow;


            if(mm_labels_nodgc[ii][ii] != 1){
                printf("\n\t\t---- NO-DGC Sanity check FAILED (max-marginal labelings incorrect) ---- \n\n");
            }

        }else{

            up_new[ii] = MARGINAL_INF; // y2 = -1
            un_new[ii] = unary_neg[ii];

            mm_labels_nodgc[ii] = maxflow_labels_withflow(up_new, un_new, binary, x.n_pos, x.n_neg, &flow);

            up_new[ii] = unary_pos[ii];
            un_new[ii] = unary_neg[ii];

            mm_scores_nodgc[ii] = flow;

            if(mm_labels_nodgc[ii][ii] != -1){
                printf("\n\t\t---- NO-DGC Sanity check FAILED (max-marginal labelings incorrect) ---- \n\n");
            }

        }
    }

    current_utc_time(&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    printf("\n\tTime take for no dynamic graph cut: %f",time_taken);

    // dynamic graph cut
    for (ii = 0; ii < num_ex; ii++){

        if(y.labels[ii] == 1){
            up_new[ii] = unary_pos[ii];
            un_new[ii] = MARGINAL_INF;
        }else{

            up_new[ii] = MARGINAL_INF;
            un_new[ii] = unary_neg[ii];

        }
    }

    //labels_star = dgc_maxmarginal_labels(unary_pos, unary_neg, binary, x.n_pos, x.n_neg, up_new, un_new);
    //labels_star = dgc_subgraph_based_mvc(x, sm, sparm, unary_pos, unary_neg, up_new, un_new, binary, &mm_scores);

    /*double *mm_scores;
    int **mm_labels_mvc = dgc_maxmarginal_labels_with_flow(unary_pos, unary_neg, binary, x.n_pos, x.n_neg, up_new, un_new, &mm_scores);*/


    current_utc_time(&start_t);

    int **mm_labels_mvc;
    double *mm_scores = (double *)calloc(num_ex, sizeof(double));
    mm_labels_mvc = (int**) malloc (num_ex*sizeof(int*));

    for(i = 0;i<num_ex; i++){
        mm_labels_mvc[i] = (int*)malloc(num_ex*sizeof(int));
        for (j=0;j<num_ex;j++){
            mm_labels_mvc[i][j] = x.x_is[j].label;
        }
    }

    int num_nodes, index;

    for (i=0;i<x.graph.num_subgraphs;i++){

        num_nodes = x.graph.subgraph[i].num_nodes;
        if(num_nodes == 1){

            index = x.graph.subgraph[i].node_index[0];
            //printf("\t%d",index);
            if(x.x_is[index].label == 1) {
                mm_scores[index] = sprod_ns(sm->w,x.x_is[index].phi1phi2_pos);
                mm_labels_mvc[index][index] = 1;
            }
            else {
                mm_scores[index] = sprod_ns(sm->w,x.x_is[index].phi1phi2_neg);
                mm_labels_mvc[index][index] = -1;
            }

        }else{

            dgc_maxmarginal_labels_subgraph_flow(unary_pos, unary_neg, binary, up_new, un_new, num_nodes, num_ex, x.graph.subgraph[i].node_index, mm_scores, mm_labels_mvc);

        }

        //current_utc_time(&finish_t);
        // time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
        //total_time_taken+=time_taken;
        //printf("\t\tFeature vector cache created (subgraph/total subgraphs) :%05d/%05d.\tTime taken:%.04f/%.04f\r",i+1,x.graph.num_subgraphs,time_taken,total_time_taken);fflush(stdout);
    }

    current_utc_time(&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    printf("\n\tTime take for dynamic graph cut - subgraph based: %f",time_taken);



    // check labels and flow
    for (i = 0; i < num_ex; i++){
        //printf("\n%f %f %f\n",mm_scores[i],mm_scores_nodgc[i], mm_scores[i]-mm_scores_nodgc[i]);

    }

    int counter = 0;
    for (i = 0; i < num_ex; i++){
        for (j = 0; j < num_ex; j++){
            if(mm_labels_mvc[i][j] != mm_labels_nodgc[i][j]) counter++;
        }
    }
    printf("\n\tmismatch count: %d\n",counter);



    free(unary_pos);
    free(unary_neg);
    free(up_new);
    free(un_new);
    for (i = 0; i < (x.n_pos+x.n_neg); i++){
        free(binary[i]);
    }
    free(binary);


}

void check_dynamic_graph_cut(PATTERN x, LABEL y, STRUCTMODEL *sm ){

    check_gynamic_gc();

}

double get_score_from_labeling_mvc(PATTERN x, int given_index, double *unary_pos, double *unary_neg, double **binary, int *labels){

    int i,j,num_ex, num_ex_subgraph;
    int graphID = x.graph.graphID[given_index];
    num_ex = x.n_neg + x.n_pos;
    num_ex_subgraph = x.graph.subgraph[graphID].num_nodes;
    double score = 0.0;

    int index_i, index_j;

    for (i = 0 ; i<num_ex_subgraph; i++){

        index_i = x.graph.subgraph[graphID].node_index[i];

        if(labels[index_i] == 1) score += unary_neg[index_i];
        else score += unary_pos[index_i];

        for(j=(i+1);j<num_ex_subgraph; j++){

            index_j = x.graph.subgraph[graphID].node_index[j];
            if(x.neighbors[index_i][index_j] && labels[index_i] != labels[index_j]) score -= binary[index_i][index_j];
        }
    }

    return score;
}

double get_score_from_labeling_lin(PATTERN x, int given_index, double *unary_pos, double *unary_neg, double **binary, int *labels){

    int i,j,num_ex, num_ex_subgraph;
    int graphID = x.graph.graphID[given_index];
    num_ex = x.n_neg + x.n_pos;
    num_ex_subgraph = x.graph.subgraph[graphID].num_nodes;
    double score = 0.0;

    int index_i, index_j;

    for (i = 0 ; i<num_ex_subgraph; i++){

        index_i = x.graph.subgraph[graphID].node_index[i];


        if(labels[index_i] == 1) score += unary_neg[index_i];
        else score += unary_pos[index_i];

        for(j=(i+1);j<num_ex_subgraph; j++){

            index_j = x.graph.subgraph[graphID].node_index[j];
            if(x.neighbors[index_i][index_j] && labels[index_i] != labels[index_j]) score -= binary[index_i][index_j];
        }
    }

    return score;

}

void test_dy_vs_nody_feature_cache(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm){

    printf("\n\n\tTesting dynamic vs non-dynamic feature cache creation for error analysis ...\n");
    int i,j,l;
    int num_ex = x.n_neg + x.n_pos;
    int **labels_star = (int **)malloc(num_ex*sizeof(int*));
    double diff_norm = 0.0, norm = 0.0;
    double max_diff_norm = 0.0;
    double min_norm_dy = 1E40, max_norm_dy = 0.0;
    double min_norm_nody = 1E40, max_norm_nody = 0.0;

    /* random labels */
    for (i = 0; i < num_ex; i++){
        labels_star[i] = (int *)malloc(num_ex*sizeof(int));
        for (j = 0; j < num_ex; j++){
            l = rand() % 3000 + 1;
            if(l>1500) l=1;
            else l = -1;
            labels_star[i][j] = l;
        }
    }

    SVECTOR **fycache_dy = create_feature_cache_dynamic(x, sm, sparm, labels_star);
    SVECTOR **fycache_nody = create_feature_cache_nodynamic(x, sm, sparm, labels_star);
    SVECTOR *temp_diff = NULL;


    for (i=0;i<num_ex;i++) {

        temp_diff = sub_ss(fycache_dy[i],fycache_nody[i]);
        diff_norm = sprod_ss(temp_diff,temp_diff);
        max_diff_norm = MAX(diff_norm, max_diff_norm);

        norm = sprod_ss(fycache_dy[i],fycache_dy[i]);
        min_norm_dy = MIN(min_norm_dy, norm);
        max_norm_dy = MAX(max_norm_dy, norm);

        norm = sprod_ss(fycache_nody[i],fycache_nody[i]);
        min_norm_nody = MIN(min_norm_nody, norm);
        max_norm_nody = MAX(max_norm_nody, norm);

        free_svector(temp_diff);
        //printf("%.4g\n",diff_norm);
    }
    printf("\t\tMAX of the norm of difference vectors\t: %.4g\n",max_diff_norm);
    printf("\t\tMIN/MAX norm of dynamic fycache\t\t: %.4g/%.4g\n",min_norm_dy, max_norm_dy);
    printf("\t\tMIN/MAX norm of non-dynamic fycache\t: %.4g/%.4g\n",min_norm_nody, max_norm_nody);
    printf("\tTesting done !!!\n");

    for (i=0;i<num_ex;i++) {
        free_svector(fycache_nody[i]);
        free_svector(fycache_dy[i]);
        free(labels_star[i]);
    }
    free(fycache_dy);
    free(fycache_nody);
    free(labels_star);

}




