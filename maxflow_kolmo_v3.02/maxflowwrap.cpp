/************************************************************************/
/*   maxflowwrap.cpp                                                    */
/*                                                                      */
/*   Code for dynamic graph cut used by HOB-SVM and HOAP-SVM
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include"maxflow.cpp"
#include <time.h>
#include "../hoap_svm/hoap_svm_api_types.h"

#define BILLION  1E9
#define MAX(x,y) ((x) < (y) ? (y) : (x))
#define MIN(x,y) ((x) > (y) ? (y) : (x))

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

void current_utc_time(struct timespec *ts) {
#ifdef __MACH__ // OS X does not have clock_gettime, use clock_get_time
    clock_serv_t cclock;
    mach_timespec_t mts;
    host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
    clock_get_time(cclock, &mts);
    mach_port_deallocate(mach_task_self(), cclock);
    ts->tv_sec = mts.tv_sec;
    ts->tv_nsec = mts.tv_nsec;
#else
    clock_gettime(CLOCK_REALTIME, ts);
#endif

}

extern "C" int *maxflow_labels(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg)
/* Graph Cut */
{
    int i,j, num_ex;
    int *labels;
    double flow;
    num_ex = n_pos+n_neg;

    typedef Graph<double,double,double> GraphType;    
    GraphType *g = new GraphType( num_ex, num_ex*(num_ex-1)/2);

    for (i = 0; i < num_ex; i++){
        g -> add_node();
    }
    for (i = 0; i < num_ex; i++){
        double up = unary_pos[i];
        double un = unary_neg[i];
        g -> add_tweights( i, up, un); //"using" tree-recycling :
        for (j = (i+1); j < num_ex; j++){
            double bi = binary[i][j];                        
            g -> add_edge(i, j, bi, bi);            
        }
    }
    flow = g -> maxflow(false); // "false" for not using tree recycling
    labels = (int *) malloc(num_ex*sizeof(int));
    for (i = 0; i < num_ex; i++){
        if (g->what_segment(i) == GraphType::SOURCE){
            labels[i] = -1;
        }
        else{
            labels[i] = 1;
        }
    }
    delete g;
    return labels;
}

extern "C" int *maxflow_labels_withflow(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg, double *flow_val){
    int i,j, num_ex;
    int *labels;
    double flow;
    num_ex = n_pos+n_neg;

    typedef Graph<double,double,double> GraphType;
    //typedef Graph<int,int,int> GraphType;

    GraphType *g = new GraphType( num_ex, num_ex*(num_ex-1)/2);


    for (i = 0; i < num_ex; i++){
        g -> add_node();
    }
    for (i = 0; i < num_ex; i++){
        double up = unary_pos[i];
        double un = unary_neg[i];

        //printf("%d:%f:%f\n",i,up,un);

        //g -> edit_tweights_wt( i, up,un); //"not using" tree-recycling :
        g -> add_tweights( i, up, un); //"using" tree-recycling :

        for (j = (i+1); j < num_ex; j++){
            double bi = binary[i][j];
            //int bi = binary[i][j]*BIG_NUMBER;
            //printf("%f: ",bi);

            g -> add_edge(i, j, bi, bi);

            //g -> edit_edge_wt( i, j,  bi, bi ); // Edit capacity of n-edge when "not using" tree-recycling :
            //g -> edit_edge( i, j,  bi, bi ); // Edit capacity of n-edge when "using" tree-recycling

        }
    }

    flow = g -> maxflow(false); // "false" for not using tree recycling
    //printf("\tflow non-dy:%lf\t \n",flow);
    //double flow = g -> maxflow(true); // "true" for using it
    *flow_val = flow;

    labels = (int *) malloc(num_ex*sizeof(int));
    for (i = 0; i < num_ex; i++){
        if (g->what_segment(i) == GraphType::SOURCE){
            labels[i] = -1;
        }
        else{
            labels[i] = 1;
        }
    }


    delete g;
    return labels;
}

extern "C" int **dgc_maxmarginal_labels(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg, double *up_new, double *un_new)
/* max-marginal labeling using dynamic graph cut */
{

    int i,j,ii, index, num_ex;
    int **labels;
    double flow;
    double gamma = 0.0;
    double up, un, bi, upnew, unnew, upold, unold, new_trcap, old_trcap, flow_dy, flow_nody;
    num_ex = n_pos+n_neg;
    labels = (int **) malloc(num_ex*sizeof(int*));

    typedef Graph<double,double,double> GraphType;

    GraphType *g = new GraphType( num_ex, num_ex*(num_ex-1)/2);

    // Create the graph
    for (i = 0; i < num_ex; i++){
        g -> add_node();

    }

    for (i = 0; i < num_ex; i++){
        up = unary_pos[i];
        un = unary_neg[i];
        g -> add_tweights( i, up, un);
        for (j = (i+1); j < num_ex; j++){
            bi = binary[i][j];
            g -> add_edge(i, j, bi, bi);
            //g -> edit_edge_wt( i, j,  bi, bi ); // Edit capacity of n-edge when "not using" tree-recycling :
            //g -> edit_edge( i, j,  bi, bi ); // Edit capacity of n-edge when "using" tree-recycling
        }
    }
    flow = g -> maxflow(false);
    flow = g -> maxflow(true);

    /* max-marginal labeling using dynamic graph cut */
    for (ii = 0; ii < num_ex; ii++){
        upnew = up_new[ii];
        unnew = un_new[ii];

        upold = unary_pos[ii];
        unold = unary_neg[ii];

        old_trcap = g->get_trcap(ii);        
         new_trcap = old_trcap + (upnew - upold) - (unnew - unold);

        //g -> add_tweights( ii, upnew, unnew);
        g -> add_tweights( ii, upnew + gamma, unnew + gamma);
        g -> set_trcap(ii,new_trcap);
        g -> mark_node(ii);
        //printf("\tnew_trcap:%lf..\n",new_trcap);
        index = ii-1;
        if(ii>0){
            upold = up_new[index];
            unold = un_new[index];

            upnew = unary_pos[index];
            unnew = unary_neg[index];

            old_trcap = g->get_trcap(index);            
            new_trcap = old_trcap + (upnew - upold) - (unnew - unold);

            g -> add_tweights( index, upnew + gamma, unnew + gamma );
            g -> set_trcap(index,new_trcap);
            g -> mark_node(index);
        }
        flow = g -> maxflow(true); // "true" for using it
        labels[ii] = (int *) malloc(num_ex*sizeof(int));

        for (i = 0; i < num_ex; i++){
            if (g->what_segment(i) == GraphType::SOURCE){
                labels[ii][i] = -1;
            }
            else{
                labels[ii][i] = 1;
            }
        }

        //sanity check
        /*int label_check;
        if(up_new[ii]>un_new[ii]) label_check = -1;
        else label_check = 1;

        if(labels[ii][ii] != label_check){
            printf("\n\t\t---- Sanity check FAILED (max-marginal labelings incorrect) ---- \n\n");
        }*/

        //printf("\tMax marginal for sample: %04d\t flow:%lf ..\n",ii+1,flow);
    }

    delete g;
    return labels;
}

extern "C" int **dgc_maxmarginal_labels_with_flow(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg, double *up_new, double *un_new, double **mm_score){

    int i,j,ii, index, num_ex;
    int **labels;
    double flow;
    double gamma = 0.0;
    double up, un, bi, upnew, unnew, upold, unold, new_trcap, old_trcap, flow_dy, flow_nody;
    num_ex = n_pos+n_neg;
    labels = (int **) malloc(num_ex*sizeof(int*));
    double *mm = (double*) calloc(num_ex, sizeof(double));   

    typedef Graph<double,double,double> GraphType;
    GraphType *g = new GraphType( num_ex, num_ex*(num_ex-1)/2);

    // Create the graph
    for (i = 0; i < num_ex; i++){
        g -> add_node();

    }
    for (i = 0; i < num_ex; i++){
        up = unary_pos[i];
        un = unary_neg[i];

        g -> add_tweights( i, up, un);

        for (j = (i+1); j < num_ex; j++){

            bi = binary[i][j];
            g -> add_edge(i, j, bi, bi);
        }


    }
    flow = g -> maxflow(false);
    //flow = g -> maxflow(true);   
    //printf("\n\tFlow: %f\n",flow);

    // max-marginal labeling using dynamic graph cut

    for (ii = 0; ii < num_ex; ii++){

        upnew = up_new[ii];
        unnew = un_new[ii];

        upold = unary_pos[ii];
        unold = unary_neg[ii];

        old_trcap = g->get_trcap(ii);       
        new_trcap = old_trcap + (upnew - upold) - (unnew  - unold);

        g -> add_tweights( ii, upnew + gamma, unnew + gamma);
        g -> set_trcap(ii,new_trcap);
        g -> mark_node(ii);

        //printf("\told_up:%f, new_up:%lf..\n\told_un:%f, new_un:%lf\n",upold,upnew, unold,unnew);
        //printf("\told_trcap:%f, new_trcap:%lf..\n",old_trcap,new_trcap);

        index = ii-1;
        if(ii>0){
            upold = up_new[index];
            unold = un_new[index];

            upnew = unary_pos[index];
            unnew = unary_neg[index];

            old_trcap = g->get_trcap(index);           
            new_trcap = old_trcap + (upnew - upold) - (unnew  - unold);

            g -> add_tweights( index, upnew + gamma, unnew + gamma );
            g -> set_trcap(index,new_trcap);
            g -> mark_node(index);
        }

        // max flow

        //flow_dy = g -> maxflow(true,changed_list); // "true" for using it
        flow_dy = g -> maxflow(true); // "true" for using it
        //flow_nody = g -> maxflow(false);

        //printf("\tflow dy:%lf\t flow non-dy:%lf \tdiff:%lf.. gamma:%f\n\n\n",flow_dy,flow_nody,(flow_nody-flow_dy), gamma);
        //printf("\tflow dy:%lf\tgamma:%f\n\n\n",flow_dy,gamma);

        /*if(ii==0) mm[ii] = flow_dy - flow + MAX(old_trcap, new_trcap);
        else mm[ii] = flow_dy - mm[ii-1] + MAX(old_trcap, new_trcap);*/

        mm[ii] = flow_dy;
        labels[ii] = (int *) malloc(num_ex*sizeof(int));

        for (i = 0; i < num_ex; i++){
            if (g->what_segment(i) == GraphType::SOURCE){
                labels[ii][i] = -1;
            }
            else{
                labels[ii][i] = 1;
            }
        }

        //sanity check
        /*int label_check;
        if(up_new[ii]>un_new[ii]) label_check = -1;
        else label_check = 1;

        if(labels[ii][ii] != label_check){
            //printf("\n\t\t---- DGC Sanity check FAILED (max-marginal labelings incorrect) %d ---- \n\n",ii);
        }*/

        //printf("\tMax marginal for sample: %04d\t flow:%lf ..\n",ii+1,flow);
    }

    *mm_score = mm;

    delete g;
    return labels;
}




extern "C" int **dgc_maxmarginal_labels_subgraph_flow(double *unary_pos, double *unary_neg, double **binary, double *up_new, double *un_new, int num_nodes_subgraph, int num_ex, int *node_index, double *mmi_w, int **mm_labels){

    int i,j,ii, index;
    int **labels;
    double flow;
    double gamma = 0.0;
    double up, un, bi, upnew, unnew, upold, unold, new_trcap, old_trcap, flow_dy, flow_nody;

    labels = (int **) malloc(num_ex*sizeof(int*));
    double *mm = (double*) calloc(num_ex, sizeof(double));

    struct timespec start_t,finish_t;
    double time_taken, total_time = 0.0;
    current_utc_time(&start_t);

    typedef Graph<double,double,double> GraphType;

    GraphType *g = new GraphType( num_nodes_subgraph, num_nodes_subgraph*(num_nodes_subgraph-1)/2);

    // Create the graph
    for (i = 0; i < num_nodes_subgraph; i++){
        g -> add_node();

    }

    int counter_i = 0;
    int counter_j = 0;

    int index_i, index_j;

    for (i = 0; i < num_nodes_subgraph; i++){

        index_i = node_index[i];
        up = unary_pos[index_i];
        un = unary_neg[index_i];

        g -> add_tweights( i, up, un);

        for (j = (i+1); j < num_nodes_subgraph; j++){

            index_j = node_index[j];
            bi = binary[index_i][index_j];
            g -> add_edge(i, j, bi, bi);



        }
    }

    flow = g -> maxflow(false);
    //flow = g -> maxflow(true);


    current_utc_time(&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\n\tTime taken to create the graph: %f",time_taken);

    //printf("\n\tFlow: %f",flow);

    // max-marginal labeling using dynamic graph cut
    current_utc_time(&start_t);

    counter_i = 0;

    for (i = 0; i < num_nodes_subgraph; i++){

        index_i = node_index[i];

        upnew = up_new[index_i];
        unnew = un_new[index_i];

        upold = unary_pos[index_i];
        unold = unary_neg[index_i];

        old_trcap = g->get_trcap(i);
         new_trcap = old_trcap + (upnew - upold) - (unnew - unold);

        g -> add_tweights( i, upnew + gamma, unnew + gamma);
        g -> set_trcap(i,new_trcap);
        g -> mark_node(i);

        //printf("\tnew_trcap:%lf..\n",new_trcap);

        index = i-1;
        if(i>0){

            upold = up_new[node_index[index]];
            unold = un_new[node_index[index]];

            upnew = unary_pos[node_index[index]];
            unnew = unary_neg[node_index[index]];

            old_trcap = g->get_trcap(index);
             new_trcap = old_trcap + (upnew - upold) - (unnew - unold);

            g -> add_tweights( index, upnew + gamma, unnew + gamma );
            g -> set_trcap(index,new_trcap);
            g -> mark_node(index);
        }

        // max flow

        //flow_dy = g -> maxflow(true,changed_list); // "true" for using it
        //mm[ii] = g -> maxflow(true); // "true" for using it
        //flow_nody = g -> maxflow(false);
        //printf("\tflow dy:%f\n",mm[ii]);
        //printf("\tflow dy:%lf\t flow non-dy:%lf \tdiff:%lf..\n",flow_dy,flow_nody,(flow_nody-flow_dy));

        flow = g -> maxflow(true);
        //printf("\n\tFlow: %f",flow);
        mmi_w[index_i] = flow;




        for (j = 0; j < num_nodes_subgraph; j++){
            if (g->what_segment(j) == GraphType::SOURCE){
                mm_labels[index_i][node_index[j]] = -1;
            }
            else{
                mm_labels[index_i][node_index[j]] = 1;
            }
        }

        //sanity check
        int label_check;
        if(up_new[index_i]>un_new[index_i]) label_check = -1;
        else label_check = 1;

        /*if(*mm_labels[index_i][index_i] != label_check){
            printf("\n\t\t---- inside 'dgc_maxmarginal_labels_subgraph_flow'', Sanity check FAILED (max-marginal labelings incorrect) ---- \n\n");
        }*/

        //printf("\tMax marginal for sample: %04d\t flow:%lf ..\n",ii+1,flow);

    }

    current_utc_time(&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\n\tTime taken for dynamic graph cut: %f\n",time_taken);


    delete g;
    return labels;
}





extern "C" void check_gynamic_gc(){

           typedef Graph<int,int,int> GraphType;
           GraphType *g = new GraphType(/*estimated # of nodes*/ 2, /*estimated # of edges*/ 1);

           g -> add_node();
           g -> add_node();

           int up0 = 1, un0 = 5;

           g -> add_tweights( 0,   /* capacities */  up0, un0 );
           g -> add_tweights( 1,   /* capacities */  2, 6 );
           g -> add_edge( 0, 1,    /* capacities */  3, 4 );

           int flow = g -> maxflow(false);

           printf("Flow = %d\n", flow);
           printf("Minimum cut:\n");
           if (g->what_segment(0) == GraphType::SOURCE)
                   printf("node0 is in the SOURCE set\n");
           else
                   printf("node0 is in the SINK set\n");
           if (g->what_segment(1) == GraphType::SOURCE)
                   printf("node1 is in the SOURCE set\n");
           else
                   printf("node1 is in the SINK set\n");

           // DG 1
           int new_up0 = 500;
           int new_un0 = 100;

            int old_trcap = g->get_trcap(0);
            int new_trcap = old_trcap + (new_up0 - up0) - (new_un0 - un0);

            g -> add_tweights( 0, new_up0, new_un0);
            g -> set_trcap(0,new_trcap);
            g -> mark_node(0);

            //int flow_dy = g -> maxflow(true) - flow + MIN(old_trcap, new_trcap); // "true" for using it
             int flow_dy = g -> maxflow(true); // to use residual graph
            printf("Dynamic Flow = %d  new_trcap: %d Old_trcap: %d\n", flow_dy, new_trcap, old_trcap);

            if (g->what_segment(0) == GraphType::SOURCE)
                    printf("node0 is in the SOURCE set\n");
            else
                    printf("node0 is in the SINK set\n");
            if (g->what_segment(1) == GraphType::SOURCE)
                    printf("node1 is in the SOURCE set\n");
            else
                    printf("node1 is in the SINK set\n");

            //DG 2

            new_up0 = 1;
            new_un0 = 5;

            old_trcap = g->get_trcap(0);
            new_trcap = old_trcap + (new_up0 - 500) - (new_un0 - 100);

            g -> add_tweights( 0, new_up0, new_un0);
            g -> set_trcap(0,new_trcap);
            g -> mark_node(0);

            flow_dy = g -> maxflow(true); // "true" for using it
            printf("Dynamic Flow = %d  new_trcap: %d Old_trcap: %d\n", flow_dy, new_trcap, old_trcap);

            if (g->what_segment(0) == GraphType::SOURCE)
                    printf("node0 is in the SOURCE set\n");
            else
                    printf("node0 is in the SINK set\n");
            if (g->what_segment(1) == GraphType::SOURCE)
                    printf("node1 is in the SOURCE set\n");
            else
                    printf("node1 is in the SINK set\n");




           delete g;
}



