# **HOAP-SVM - software for the following learning algorithms** #

    (1) Multiclass-SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP-SVM (HOAP-SVM)
    (5) M4 Learning (unpublished work)

**NOTE**: The current version of the code is the implementation of the action classification application presented in the paper. A more general code will be soon uploaded. In the current setting, the unary features consist of the concatenation of poselet and gist features, and the pairwise feature can be of any combination of these two features . The connectivity between the samples can be made based on their distance in the poselet or the gist space (details given in the "Code Usage" section).

    -- Code Download (Run following command in your terminal) --
    git clone https://puneetkdokania@bitbucket.org/puneetkdokania/hoap_svm_code.git

    -- Data Download --
    http://cvn.ecp.fr/projects/ranking-highorder/dependencies/action_poselet_gist/action_data_voc11.tgz
    (Please email me if the link is not working)
    

    Author: Puneet Kumar Dokania 
    http://puneetkdokania.github.io/index.html
    (puneetkdokania at gmail dot com; puneet dot kumar at inria dot fr),
    September 2014.

# Introduction #

HOB-SVM and HOAP-SVM are the frameworks that we proposed in ECCV 2014. Please refer to:

    "Learning to Rank using High-Order Information."
    Puneet K. Dokania, A. Behl, C. V. Jawahar, and M. Pawan Kumar, In ECCV 2014 
    (http://cvn.ecp.fr/projects/ranking-highorder/)

The code uses maxflow code by Boykov and Kolmogorov. We have written a wrapper to convert
this code into the dynamic graph cut code (when only unaries are modified). If you use this software for research purposes, you should cite the aforementioned paper in
any resulting publication.

# Learning Algorithms #
    
    (1) Multiclass-SVM: Standard learning algorithm. Optimizes weighted 0/1 loss. It is a special case of HOB-SVM. In case of no-connectivity, or only first-order information,
    HOB-SVM boils down to Multiclass-SVM
    (2) AP-SVM: Optimizes Average Precision (AP) based loss function. It is a special case of HOAP-SVM. In case of no-connectivity, or only first-order information,
    HOAP-SVM boils down to AP-SVM
    (3) HOB-SVM: Optimizes decomposable loss (in this code we use hamming loss) while incorporating high-order information. Ranking is
    given by the difference of max-marginal scores. Dynamic graph-cut has been used for the efficient computation of max-marginals
    (4) HOAP-SVM: Optimizes AP loss while incorporating high-order information

# High-Order Information #

In this code we only deal with the pairwise interactions between the connected samples. The frameworks proposed can be used for any high-order information also.
In order to find the connectivity between the samples, we use the euclidean distance thresholds in the poselet and gist space. Two samples connected in any of the
two feature space is considered having an edge between them. In order to find the disconnected components of the graphs, or the subgraphs, we used union-find data-
structure. The pairwise potential function between to connected sample is defined as follows:

        \eta exp(sigma(xi - xj)^2)
        all operations are done elementwise. sigma is fixed to -1 and -100 for poselet and gist respectively. \eta was cross validated
        in our experiments.


# Objective Functions #
Please refer to the following pdf to see the exact objective functions.

# Code Usage #

The code uses **Mosek v6** solver for the optimization of the QPs (below I have given instructions on installing Mosek). Please download and install Mosek solver. **Modify the Makefile (MOSEK_H,
MSKLINKFLAGS, and MSKLIBPATH)**. Type "make" to compile your code. Compile creates the executables "***hoap_svm_learn***" for learning and
"***hoap_svm_classify***" for classification/ranking.

**Run following commands to check the installation:**

    ./hoap_svm_learn -c 0.1 -G 0 -A 2 -t 0 trainData model
    ./hoap_svm_classify -G 0 -A 2 -t 0 trainData model result

Standard usage:

    ./hoap_svm_learn -c 0.1 -A 2 trainDataFile model_file model_initialization
    ./hoap_svm_classify -c 0.1 -A 2 testDataFile model_file result_file


Note that the "result_file" contains average 0/1 loss, weighted loss, and AP values.

General Options:
----------------

    -c : trade-off between training error and margin (default 0.1)
    -G : gist threshold to create connectivity (default -1.0, no connectivity)
    -P : poselet threshold to create connectivity (default -1.0, no connectivity)
    -A : training algorithm type

        (1) Multiclass SVM : -A 0 , e.g.
            ./hoap_svm_learn -c 0.1 -A 0 jump_1_train_subset model

        (2) HOB-SVM: Multiclass with connectivity, therefore, -P or -G or both must be non-negative, e.g.
            ./hoap_svm_learn -c 0.1 -A 0 -G 0.1 -P 0.2 jump_1_train_subset model

        (3) AP-SVM : -A 2, e.g.
            ./hoap_svm_learn -c 0.1 -A 2 jump_1_train_subset model

        (4) HOAP-SVM: AP-SVM with connectivity, therefore, -P or -G or both must be non-negative, e.g.
            ./hoap_svm_learn -c 0.1 -A 2 -G 0.1 -P 0.2 jump_1_train_subset model

        (5) M4 Learning: -A 1, e.g.
            ./hoap_svm_learn -c 0.1 -A 1 -G 0.1 -P 0.2 jump_1_train_subset model

    -B : binary weight. The trade-off between the first order and high-order information. Defined as /eta in the paper. (default 1.0)

    -b : binary feature vector type.
        1 - [poselet]
        2 - [gist]
        3 - [poselet;gist]
        for example,

        ./hoap_svm_learn -c 0.1 -A 2 -G 0.1 -P 0.2 -b 1 jump_1_train_subset model

        the above command learns a HOAP-SVM in which samples are connected based on thresholds in gist
        and poselet spaces. Also the pairwise feature vector consists of only the poselet features.

    -l : lambda (the proximal regularizer parameter), e.g.
        ./hoap_svm_learn -c 0.1 -l 100 -G 0.1 -A 2 trainData model

    -t : input data format (default 1)
      0 : single file with all the data. Each pair of lines contains the poselet and gist features (consecutive) in the following format 
      (label feature1:index feature2:index ...)
      1 : metafiles with the locations of the data features. Example can be seen in the metafiles given with the action data in the 
      following link [http://cvn.ecp.fr/projects/ranking-highorder/dependencies/action_poselet_gist/action_data_voc11.tgz].

# Installing Mosek6 #
    1. Download Mosek (http://www.mosek.com/resources/downloads/legacy/version-6)
    2. Get the academic license (http://license.mosek.com/academic/) and put in the "mosek/6/licenses" folder
    3. Put these lines into your bashrc

    **Linux**

    DIR="path of mosek directory"
    MOSEKPLATFORM=linux64x86
    export PATH=$PATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin    
    export MOSEKLM_LICENSE_FILE=$DIR/mosek/6/licenses
    export CLASSPATH=$CLASSPATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin/mosek.jar

    **OSx**
    everything same except the LD_LIBRARY_PATH
    export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin

    4. source ~/.bashrc

    5. Run "mosek -f" in the terminal to check
    6. Modify the Makefile (MOSEK_H, MSKLINKFLAGS, and MSKLIBPATH)