function experimentsVOCtest

%% Author: Puneet Kumar Dokania 
% puneetkdokania at gmail dot com; puneet dot kumar at inria dot fr 
% (http://cvn.ecp.fr/personnel/puneet/)
% project page: (http://cvn.ecp.fr/projects/ranking-highorder/)
%
%% Output:
%This code generates the results for the Action dataset of PASCAL VOCtest
%2011. As an output we the results in in PASCAL VOC format in the "results_pascal_format"
%folder. We also get the max-marginal scores as an output stored at the
%location of the model files.

%% Requirements (Trained Model and Data): 
% Download the poselet and gist features of PASCAL VOC 2011 action dataset
% from (http://cvn.ecp.fr/projects/ranking-highorder/dependencies/action_poselet_gist/action_data_voc11.tgz). 
%Put the dataset ("action_data_voc11") in the code directory (hoap_svm_code). 
%
%Note that the path to the dataset and the trained model files is hard-coded relative 
%to the code folder, so don't put the dataset in any other location, 
%otherwise modify the paths accordingly. 
%
%The dataset contains 
%(1) "metafiles" : location of the poselet features (bounding box), gist features
%(image containing that bounding box), bbox id, and label.
%(2) "poselet_gist" : this folder contains the extracted features. 
%(3) "trainvalModels" : already trained models. The parameters are set
%after five-fold cross validation. 
%(4) "matlab_scripts" : some scripts that might be useful.
%
%Take care of the relative paths of these features w.r.t
%to the binaries.

% POSELET FEATURES:
% ==================
% The poselet features given are 24004 dimensional but we use 2404 dimensional poselet features. The details are as given below.
% 
% Original poselets are 2400 dimension which is multiplied by 10 in our features (5x from spatial pyramid and 2x from segmentation).  
% For segmentation, GrabCut algorithm is used to segment out the contours of the visible person in each instance’s person bounding box. 
% It then outputs a foreground mask, one per instance, for the person contour.
% 
% 1 to 2400: 
% Poselet activations in part1 of spatial pyramid whose overlap with the “segmented” person foreground < segmentation_threshold
% 2401 to 4800:
% Poselet activations in part1 of spatial pyramid whose overlap with the “segmented” person foreground >= segmentation_threshold 
% 4801 to 7200: 
% Poselet activations in part2 of spatial pyramid whose overlap with the “segmented” person foreground < segmentation_threshold
% 7201 to 9600:
% Poselet activations in part2 of spatial pyramid whose overlap with the “segmented” person foreground >= segmentation_threshold
% 9601 to 12000: 
% Poselet activations in part3 of spatial pyramid whose overlap with the “segmented” person foreground < segmentation_threshold
% 12001 to 14400:
% Poselet activations in part3 of spatial pyramid whose overlap with the “segmented” person foreground >= segmentation_threshold
% and so on. 
% 'segmentation_threshold' of 0 is used, therefore first 2400 values are zero. 24001 to 24004 are from object detection (bicycle, motorbike, 
%horse and tvmonitor) scores. 
% We don't use 24004 dimensional poselet features. Infact we use the poselet features of dimension 2404 (2401 to 4800 and 24001 to 24004).
% 
% GIST FEATURES:
% ===============
% The gist feature used are 512 dimensional.
%
%% NOTE:
% Since we don't have the ground truth labels for the VOC test dataset,
% therefore, the value of AP, average loss and weighted loss doesn't make
% any sense. 
%%


clc;
clear all;

action_classes = {
    'jumping'
    'phoning'
    'playinginstrument'
    'reading'
    'ridingbike'
    'running'
    'takingphoto'
    'usingcomputer'
    'walking'
    'ridinghorse'
    };

VOCTestMetafilePath = './action_data_voc11/metafiles/VOCtest_metafiles/'; %final location of the data
test_bin = './hoap_svm_classify';

pascal_format_dir = 'results_pascal_format';
mkdir(pascal_format_dir);
svm_dir = [pascal_format_dir,'/svm/'];
apsvm_dir = [pascal_format_dir,'/apsvm/'];
hobsvm_dir = [pascal_format_dir,'/hobsvm/'];
hoapsvm_dir = [pascal_format_dir,'/hoapsvm/'];

create_pascal_directories(svm_dir);
create_pascal_directories(apsvm_dir);
create_pascal_directories(hobsvm_dir);
create_pascal_directories(hoapsvm_dir);


%% Cross-Validated Parameters (C and B parameters, B is the binary weight parameter)
%Csvm = { '100' '100' '100' '100' '100' '100' '100' '100' '100' '100' };
%Capsvm = { '10', '1', '1', '1', '10', '100', '1', '1', '10', '100' };
%Chob = { '100' '100' '1000' '100' '100' '1000' '100' '10000' '100' '100' };
Bhob = { '10' '100' '1000' '1000' '0.1' '1' '10' '1000' '100' '0.1' };
%Choap = { '1' '1' '1' '0.1' '10' '100' '1' '1' '10' '100'}; %hoap-svm was initialized using hob-svm model
Bhoap = {'0.1' '0.001' '0.01' '10' '0.01' '0.1' '10' '1' '0.01' '0.01'};

tag = 'test';

for c = 1:length(action_classes)
    
    action = action_classes{c};
    fprintf('\t%s\n',action);
    if(strcmp(action,'jumping'))
        metafileTest = [VOCTestMetafilePath 'action_jumping_VOCtest_metafile'];
    else
        metafileTest = [VOCTestMetafilePath 'action_VOCtest_metafile'];
    end
    
    modelSVM = ['./action_data_voc11/trainvalModels/svm/', action,'_trainval_svm'];
    modelAPSVM = ['./action_data_voc11/trainvalModels/apsvm/', action,'_trainval_apsvm'];
    modelHOB = ['./action_data_voc11/trainvalModels/hobsvm/', action, '_trainval_hobsvm'];
    modelHOAP = ['./action_data_voc11/trainvalModels/hoapsvm/hobsvminit/', action, '_trainval_hoapsvm'];
    
    resultFileSVM = [modelSVM,'_',tag];
    resultFileAPSVM = [modelAPSVM,'_',tag];
    resultFileHOB = [modelHOB,'_',tag];
    resultFileHOAP = [modelHOAP,'_',tag];    
    
    testcmdSVM = sprintf('%s -A 2 %s %s %s %s',test_bin, metafileTest,modelSVM,resultFileSVM, tag);
    testcmdAPSVM = sprintf('%s -A 2 %s %s %s %s',test_bin,metafileTest,modelAPSVM,resultFileAPSVM, tag);
    testcmdHOB = sprintf('%s -G 0 -B %s -A 2 %s %s %s %s',test_bin,Bhob{c}, metafileTest,modelHOB,resultFileHOB, tag); %GIST threshold of 0 means connecting the bounding boxes from the same image.
    testcmdHOAP =  sprintf('%s -G 0 -B %s -A 2 %s %s %s %s',test_bin,Bhoap{c}, metafileTest,modelHOAP,resultFileHOAP, tag); %GIST threshold of 0 means connecting the bounding boxes from the same image.
    
    
    if (exist(modelSVM))
        fprintf('\n\t=== Testing SVM for %s action class ===\n',action);
        system(testcmdSVM);
    else
        fprintf('\n\t=== SVM model file does not exist: %s ===\n',modelSVM);
    end
    
    if (exist(modelAPSVM))
        fprintf('\n\t=== Testing AP-SVM for %s action class ===\n',action);
        system(testcmdAPSVM);
    else
        fprintf('\n\t=== APSVM model file does not exist: %s ===\n',modelAPSVM);
    end
    
    if (exist(modelHOB))
        fprintf('\n\t=== Testing HOB-SVM for %s action class ===\n',action);
        system(testcmdHOB);
    else
        fprintf('\n\t=== HOB-SVM model file does not exist: %s ===\n',modelHOB);
    end
    
    if (exist(modelHOAP))
        fprintf('\n\t=== Testing HOAP-SVM for %s action class ===\n',action);
        system(testcmdHOAP);
    else
        fprintf('\n\t=== HOAP-SVM model file does not exist: %s ===\n',modelHOAP);
    end
    
    %% Convert scores into PASCAL VOC format
    pascal_format(metafileTest, svm_dir, action, [resultFileSVM,'_maxmarginals']);
    pascal_format(metafileTest, apsvm_dir, action, [resultFileAPSVM,'_maxmarginals']);
    pascal_format(metafileTest, hobsvm_dir, action, [resultFileHOB,'_maxmarginals']);
    pascal_format(metafileTest, hoapsvm_dir, action, [resultFileHOAP,'_maxmarginals']);
    
    
end
fprintf('\n\t=== Compressing result folder into .tgz  ===');
cmd = sprintf('cd %s; tar -czf results.tgz results', svm_dir);
system(cmd);
cmd = sprintf('cd %s; tar -czf results.tgz results', apsvm_dir);
system(cmd);
cmd = sprintf('cd %s; tar -czf results.tgz results', hobsvm_dir);
system(cmd);
cmd = sprintf('cd %s; tar -czf results.tgz results', hoapsvm_dir);
system(cmd);
fprintf('\n\tDone!!\n');

end

function create_pascal_directories(baseDir)
mkdir(baseDir);
mkdir([baseDir,'results/VOC2011']);
mkdir([baseDir,'results/VOC2011/Action']);
mkdir([baseDir,'results/VOC2011/Layout']);
mkdir([baseDir,'results/VOC2011/Main']);
mkdir([baseDir,'results/VOC2011/Segmentation']);
end

function  pascal_format(metafile, final_dir, action, mm_fname)
[poselet gist id labels] = textread(metafile,'%s %s %d %d');
final_result_fname = [final_dir, 'results/VOC2011/Action/comp9_action_test_', action, '.txt'];
fid = fopen(final_result_fname,'w');
mm_scores = load(mm_fname);
final_scores = mm_scores(:,1) - mm_scores(:,2);
for i = 2:length(labels)
    img_identifier = poselet{i}(end-10:end);
    obj_index = id(i);
    fprintf(fid,'%s %d %f\n',img_identifier, obj_index, final_scores(i-1,1));
end
fclose(fid);
clear poselet;
clear gist;
clear id;
clear labels;
end


