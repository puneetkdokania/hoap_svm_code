/************************************************************************/
/*   hoap_svm_main.c                                                    */
/*                                                                      */
/*   Main Optimization Code for the following learning algorithms:
    (1) Multi-Class SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP SVM (HOAP-SVM)
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

//valgrind -v --tool=memcheck --leak-check=full --show-reachable=yes ./max_marginal_learn -c 100 -G 0.4 -D 1 ../jump_1_train_miniset model
#include <stdio.h>
#include <assert.h>
#include <float.h>
#include<sys/types.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
//#include <omp.h>
#include "hoap_svm_api.h"

#define MAX_MOSEK_ERROR_COUNT 4
#define VERBOSE_LEVEL 0
#define BILLION  1E9
#define ALPHA_THRESHOLD 0.0 // should be zero for high C and high G
#define SLACK_THRESHOLD 1E-12 //I am not using it
#define IDLE_ITER 20
#define CLEANUP_CHECK 50
#define OBJ_CHECK 1
#define PRIMAL_DUAL_GAP 1E-4
#define MV_CHECK 10
#define STOP_PREC 1E-2
#define UPDATE_BOUND 3
#define MAX_CURRICULUM_ITER 10
#define EQUALITY_EPSILON 1E-6 // CAREFUL ABOUT THIS

#define MAX(x,y) ((x) < (y) ? (y) : (x))
#define MIN(x,y) ((x) > (y) ? (y) : (x))

#define DEBUG_LEVEL 0
//#define MAX_OUTER_ITER 100

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

int main(int argc, char* argv[]) {


    double *w, *best_w; /* weight vector */
    int m, i,j;
    double C, epsilon;
    LEARN_PARM learn_parm;
    KERNEL_PARM kernel_parm;
    char trainfile[1024];
    char modelfile[1024];
    int MAX_ITER;
    /* new struct variables */
    SVECTOR **fycache;
    EXAMPLE *ex;
    SAMPLE alldata;
    SAMPLE sample;

    STRUCT_LEARN_PARM sparm;
    STRUCTMODEL sm;
    //LABEL ystar,y;

    double primal_obj;
    double stop_crit;
    char infofile[1024];

    struct timespec start_t_ol,finish_t_ol,start_t_temp,finish_t_temp;
    double time_taken_ol, time_taken_temp, total_ol_time_taken = 0.0;

    int **labels_star;
    double decrement;


    /* read input parameters */
    my_read_input_parameters(argc, argv, trainfile, modelfile, &sparm);


    sprintf(infofile,"%s.info",modelfile);
    //FILE *fid_info;
    //fid_info = fopen(infofile,"w");

    epsilon = sparm.epsilon; // 0.001
    epsilon = 0.01;
    C = sparm.svm_c;
    MAX_ITER = sparm.maxiter;

    /* read in examples */
    if(sparm.t){
        alldata = read_struct_examples(trainfile,&sparm);
    }else{
        alldata = read_struct_examples_direct(trainfile,&sparm);
    }

    sample = alldata;

    ex = sample.examples;
    m = sample.n;
    int num_ex = ex[0].x.n_neg + ex[0].x.n_pos;

    /* initializations */
    init_struct_model(alldata,&sm,&sparm);

    if(sparm.initial_w_file != NULL){
        printf("\n\tWEIGHT VECTOR INITIALIZATION FILE: %s\n",sparm.initial_w_file);
        initialize_with_weight_vector(&sm,sparm); // edit - done
        printf("\tWEIGHT VECTOR INITIALIZATION DONE\n\n");
        w = sm.w;

    }else{
        printf("\n\tNO WEIGHT VECTOR INITIALIZATION FILE GIVEN\n");
        w = create_nvector(sm.sizePsi);
        clear_nvector(w, sm.sizePsi);
        sm.w = w; /* establish link to w, as long as w does not change pointer */
    }

    best_w = (double*) calloc (sm.sizePsi+1, sizeof(double));
    for(i=0;i<=sm.sizePsi;i++){
        best_w[i] = sm.w[i];
    }

    /* print some information */
    print_training_information(&alldata,&sm,&sparm);

    /* CCCP Outer Loop */
    int *label_mask = (int*) malloc(num_ex*sizeof(int));
    int outer_iter = 0;
    double last_primal_obj = 0.0;
    double w_norm = 0.0;
    double obj_after_lin = 0.0, obj_b4_mm = 0.0;
    decrement = 0;
    double eps_new = 0.0;

    int max_outer_iter = 100;
    if(sparm.num_neighbors == 0 || sparm.A==0) max_outer_iter = 1;

    while ((outer_iter<2)||((!stop_crit)&&(outer_iter<max_outer_iter))) {

        //test_dy_vs_nody_feature_cache(ex->x, &sm, &sparm);
        //exit(1);
        //check_dynamic_graph_cut(ex->x, ex->y, &sm);
        //exit(1);

        current_utc_time(&start_t_ol); // starting of outer loop
        if(sparm.A){
            printf("\nOUTER CCCP ITERATION STARTED : %04d/%04d",outer_iter+1,max_outer_iter);

            /* decrease proximal regularizer in each outer iteration */
            if(outer_iter) sparm.lambda_proximal /= (double) (outer_iter+1);
            printf("\n\tPROXIMAL REGULARIZER LAMBDA: %.8g", sparm.lambda_proximal);

            /* linearize concave part */
            current_utc_time(&start_t_temp);
            if(sparm.D){
                labels_star = linearize_concave_part(ex->x, ex->y, &sm, &sparm); // using dynamic graph-cut
            }else{
                labels_star = linearize_concave_part_nodgc(ex->x, ex->y, &sm, &sparm);
            }
            current_utc_time(&finish_t_temp);
            time_taken_temp = (finish_t_temp.tv_sec - start_t_temp.tv_sec) + (finish_t_temp.tv_nsec - start_t_temp.tv_nsec)/BILLION;
            printf("\n\t\tLinearization of the concave part done.\tTime Taken:%.4f secs.\n",time_taken_temp);

            /* Create feature vector cache (highly expensive step in case of high connectivity). A simple dynamic algorithm is developed
            to create feature cache efficiently */

            printf("\tCreating feature vector cache (dynamically) ... ");
            current_utc_time(&start_t_temp);
            fycache = create_feature_cache_dynamic(ex->x, &sm, &sparm, labels_star);
            current_utc_time(&finish_t_temp);
            time_taken_temp = (finish_t_temp.tv_sec - start_t_temp.tv_sec) + (finish_t_temp.tv_nsec - start_t_temp.tv_nsec)/BILLION;
            printf("Time Taken:%.4f secs.\n",time_taken_temp);

            /* find primal objective (first) */
            if(outer_iter == 0 && sparm.initial_w_file != NULL){
                w_norm = sprod_nn(sm.w,sm.w,sm.sizePsi);
                obj_after_lin = current_obj_val(ex, fycache, m, &sm, &sparm, C, labels_star);
                printf("\t\tw_norm2: %.4f\n", w_norm);

            }else {
                obj_after_lin = current_obj_val(ex, fycache, m, &sm, &sparm, C, labels_star);
            }

            if(outer_iter==0){
                last_primal_obj = obj_after_lin;
            }

        }else{

            for(i=0;i<num_ex;i++){
                label_mask[i] = 1;
            }
            printf("\nOUTER ITERATION STARTED : %04d/%04d",outer_iter+1,outer_iter+1);
            fycache = create_feature_cache(ex->x, &sm, &sparm, ex->y, label_mask);

            /* find primal objective (first) */
            last_primal_obj = current_obj_val(ex, fycache, m, &sm, &sparm, C, labels_star);
            printf("\tPrimal objective: %.4f\n", last_primal_obj);

        }

        /* cutting plane algorithm */
        printf("\tInner loop cutting plane algorithm started ...\n");
        primal_obj = cutting_plane_algorithm_dual(w, m, MAX_ITER, C, epsilon, fycache, ex, &sm, &sparm,modelfile,start_t_ol, trainfile, outer_iter+1, &eps_new, labels_star);
        printf("\n\tInner loop optimization finished.\n"); fflush(stdout);
        decrement = last_primal_obj - primal_obj;
        last_primal_obj = primal_obj;
        stop_crit = (decrement<C*eps_new);

        printf("Primal objective: %.4f\n", primal_obj);
        printf("Decrement in the obj: %.gf\n", decrement); fflush(stdout);
        printf("C*epsilon: %.g\n",C*eps_new); fflush(stdout);

        current_utc_time(&finish_t_ol); // outer loop finished
        time_taken_ol = (finish_t_ol.tv_sec - start_t_ol.tv_sec) + (finish_t_ol.tv_nsec - start_t_ol.tv_nsec)/BILLION;
        total_ol_time_taken += time_taken_ol;

        /* store best w based on the decrement */
        if(decrement<0.0){
            printf("Since Obj val decrement < 0.0, therefore, STORING PREVIOUS BEST MODEL\n");
            for(i=0;i<=sm.sizePsi;i++){
                sm.w[i] = best_w[i];
            }
            sm.primal_obj = primal_obj + decrement;
        }else
        {
            for(i=0;i<=sm.sizePsi;i++){
                best_w[i] = sm.w[i];
            }
            sm.primal_obj = primal_obj;

        }

        if(sparm.num_neighbors) printf("OUTER CCCP ITERATION FINISHED: %04d/%04d\tTIME TAKEN:%.4f secs.\n----\n",outer_iter+1,max_outer_iter,time_taken_ol);

        if(sparm.A){
            obj_b4_mm = 0.0;
            for (i=0;i<num_ex;i++) {
                obj_b4_mm += sprod_ns(sm.w,fycache[i]);
            }
            for (i=0;i<num_ex;i++) {
                free_svector(fycache[i]);
                free(labels_star[i]);

            }
            free(fycache);
            free(labels_star);
            //free(mm_lin_score);

        } else{

            free_svector(fycache[0]);
            free(fycache);

        }

        if(sparm.num_neighbors == 0 || sparm.A==0){ // only one outer iteration for HOB-SVM; only one outer iteration for HOAP-SVM and M4 Learning in case of no connectivity
            printf("OUTER ITERATION FINISHED: %04d/%04d\tTIME TAKEN:%.4f secs.\n----\n",outer_iter+1,max_outer_iter,time_taken_ol);
            stop_crit = 1;
            outer_iter = max_outer_iter;
        }

        outer_iter++;

        if(stop_crit && outer_iter > 2){
            printf("\n\tStopping criteria reached ...");

        }
    } // end of CCCP outer loop
    //fclose(fid_info);


    /* storing the best w */
    for(i=0;i<=sm.sizePsi;i++){
        sm.w[i] = best_w[i];
    }

    total_ol_time_taken += sm.inter_time_taken; // adding with the time taken by the previous intermediate results (if any)
    sm.total_time_taken = total_ol_time_taken;
    printf("\n\tTOTAL TRAINING TIME TAKEN: %.4f secs.\n\n",total_ol_time_taken);

    /* write structural model */
    write_struct_model(modelfile, &sm, &sparm);

    /* free memory */
    free_struct_sample(alldata);
    free_struct_model(sm, &sparm);
    free(best_w);
    free(label_mask);

    return(0);
}


void my_read_input_parameters(int argc, char *argv[], char *trainfile, char* modelfile, STRUCT_LEARN_PARM *struct_parm) {

    long i;

    /* set default */
    struct_parm->maxiter=10000;
    struct_parm->svm_c=0.1;
    struct_parm->epsilon= 0.001; // epsilon
    struct_parm->primal_check = 30;
    struct_parm->gram_regularization = 1E-20; // used to make non-psd into psd by adding into the diagonal

    struct_parm->solve_dual = 1.0; // don't change
    struct_parm->slack_normalizer = 1.0;

    struct_parm->store_inter_results = 0;
    struct_parm->load_inter_results = 0;
    struct_parm->timetosave_inter = 0;
    struct_parm->igloo_cluster = 0;
    struct_parm->inter_num = 1;
    struct_parm->fold = 0;

    struct_parm->J = 1; // data balancing is on bydefault
    struct_parm->D = 1; // by-default the algo uses dynamic graph cut
    struct_parm->A = 0; // by default the algortihm type is HOB-SVM
    struct_parm->t = 1; // by-default the input trainfile type is the old one
    struct_parm->verbosity = 1;
    struct_parm->initial_w_file = NULL;
    struct_parm->weight_initial_apsvm = 0; // by-default the initial weight file is NOT apsvm

    struct_parm->pose_sigma = -1.0;
    struct_parm->gist_sigma = -100.0;
    struct_parm->pose_thres = -1.0; // no poselet connectivity
    struct_parm->gist_thres = -1.0; // no gist connectivity
    struct_parm->psi2_type = 1; //poselet only
    //struct_parm->lambda_proximal = 100.0;
    struct_parm->lambda_proximal = 0.0;
    struct_parm->binary_weight = 1.0;
    struct_parm->custom_argc=0;

    for(i=1;(i<argc) && ((argv[i])[0] == '-');i++) {
        switch ((argv[i])[1]) {
        case 'c': i++; struct_parm->svm_c=atof(argv[i]); break;
        case 'e': i++; struct_parm->epsilon=atof(argv[i]); break;
        case 'd': i++; struct_parm->solve_dual = atoi(argv[i]); break; // not required as the primal solver has been removed
        case 'A': i++; struct_parm->A=atof(argv[i]); break; // Algorithm type (0 for cssvm, 1 for M4, 2 for M4AP)
        //case 'p': i++; struct_parm->pose_sigma=atof(argv[i]); break; // binary weight for poselet (not required)
        //case 'g': i++; struct_parm->gist_sigma=atof(argv[i]); break;  // binary weight for gist (not required)
        case 'P': i++; struct_parm->pose_thres=atof(argv[i]); break; // neighborhood threshold for poselet
        case 'G': i++; struct_parm->gist_thres=atof(argv[i]); break; // neighborhood threshold for gist
        case 'B': i++; struct_parm->binary_weight=atof(argv[i]); break; // binary weight
        case 'J': i++; struct_parm->J=atof(argv[i]); break; // data balancing
        case 'D': i++; struct_parm->D=atof(argv[i]); break; // Graphcut type (0 for non-DGC, 1 for DGC)
        case 't': i++; struct_parm->t=atof(argv[i]); break; // Input trainfile type (0 for svml type with whole data in one file, 1 for our old type with location of differnt files)
        case 'v': i++; struct_parm->verbosity=atof(argv[i]); break; // Verbosity level
        case 'w': i++; struct_parm->weight_initial_apsvm=atof(argv[i]); break; // initial weight file
        case 'b': i++; struct_parm->psi2_type=atof(argv[i]); break; // pairwise (binary) feature type; 1:only poselet, 2: only gist, 3: poselet+gist
        case 'l': i++; struct_parm->lambda_proximal=atof(argv[i]); break; // pairwise (binary) feature type; 1:only poselet, 2: only gist, 3: poselet+gist

        case '-': strcpy(struct_parm->custom_argv[struct_parm->custom_argc++],argv[i]);i++; strcpy(struct_parm->custom_argv[struct_parm->custom_argc++],argv[i]);break;
        default: printf("\nUnrecognized option %s!\n\n",argv[i]);
            exit(0);
        }

    }

    if(i>=argc) {
        printf("\nNot enough input parameters!\n\n");
        my_wait_any_key();
        exit(0);
    }
    strcpy (trainfile, argv[i]);

    if((i+1)<argc) {
        strcpy (modelfile, argv[i+1]);
    }
    else {
        strcpy (modelfile, "lssvm.model");
    }

    if((i+2)<argc) {
        struct_parm->initial_w_file = argv[i+2];
    }

    parse_struct_parameters(struct_parm);
}


void my_wait_any_key()
{
    printf("\n(more)\n");
    (void)getc(stdin);
}

void add_vector_nn(double *w, double *dense_x, long n, double factor) {
    long i;
    for (i=1;i<n+1;i++) {
        w[i]+=factor*dense_x[i];
    }
}

double* add_list_nn(SVECTOR *a, long totwords)
/* computes the linear combination of the SVECTOR list weighted
               by the factor of each SVECTOR. assumes that the number of
               features is small compared to the number of elements in the
               list */
{
    SVECTOR *f;
    long i;
    double *sum;

    sum=create_nvector(totwords);

    for(i=0;i<=totwords;i++)
        sum[i]=0;

    for(f=a;f;f=f->next)
        add_vector_ns(sum,f,f->factor);

    return(sum);
}


double current_obj_val(EXAMPLE *ex, SVECTOR **fycache, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double C, int **labels_lin) {

    double margin = 0.0;
    double avg_pred_loss = 0.0;
    double eps_max = 0.0;

    SVECTOR *new_constraint = find_cutting_plane(ex, fycache, &margin, m, sm, sparm, &avg_pred_loss, &eps_max, C, labels_lin);
    double slack = margin - sprod_ns(sm->w, new_constraint);

    if(slack<0.0){ slack = 0.0; }
    double primal_obj = 0.5*sprod_nn(sm->w,sm->w,sm->sizePsi) + C*slack;

    free_svector(new_constraint);

    return primal_obj;


}

int compar(const void *a, const void *b)
{
    sortStruct *c = (sortStruct *) a;
    sortStruct *d = (sortStruct *) b;
    if(c->val < d->val)
        return -1;
    if(c->val > d->val)
        return 1;
    return 0;
}


long *randperm(long m)
{
    long *perm = (long *) malloc(sizeof(long)*m);
    long *map = (long *) malloc(sizeof(long)*m);
    long i, j;
    for(i = 0; i < m; i++)
        map[i] = i;
    srand(time(NULL));
    for(i = 0; i < m; i++)
    {
        int r = (int) (((double) m-i)*((double) rand())/(RAND_MAX+1.0));
        perm[i] = map[r];
        for(j = r; j < m-1; j++)
            map[j] = map[j+1];
    }
    free(map);
    return perm;
}


double* fcp_m4svm(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss){
    /* find cutting plane for Max-margin Max-Marginal learning framework. This work has not been published yet */

    double pred_loss = 0.0,lossval = 0.0;
    double *new_constraint;
    int **mvc_labels;
    int *pred_labels;
    int i, num_ex, num_ex_J,slack_fail = 0, pred_loss_fail = 0;
    SVECTOR *f, *fy, *fybar, *lhs, **fybarcache;
    LABEL      ybar;

    /* find cutting plane */
    lhs = NULL;
    *margin = 0.0;
    *avg_pred_loss = 0.0;

    num_ex = ex[0].x.n_neg + ex[0].x.n_pos;
    num_ex_J = ex[0].x.n_neg + sparm->J*ex[0].x.n_pos;

    pred_labels = (int*)malloc(num_ex*sizeof(int)); // for sanity check (do not remove)

    if(sparm->D) mvc_labels = find_mvc_m4svm_dgc(ex[0].x, ex[0].y, sm, sparm, &fybarcache, pred_labels);
    else mvc_labels = find_mvc_m4svm_nodgc(ex[0].x, ex[0].y, sm, sparm, &fybarcache, pred_labels);


    // Sanity check (labeling with dgc = labeling without dgc)
    /*int *mismatch = (int*)calloc(num_ex,sizeof(int));
    SVECTOR **fybarcache_nodgc;
    int **mvc_labels_nodgc = find_most_violated_constraint_marginrescaling_nodgc(ex[0].x, ex[0].y, sm, sparm, &fybarcache_nodgc, pred_labels);
    int count_old = 0, count_new = 0;
    for (i=0;i<num_ex;i++) {
        count_new = 0;
        for (j=0;j<num_ex;j++) {
            if(mvc_labels[i][j] != mvc_labels_nodgc[i][j]){
                mismatch[i]++;
                count_new++;
            }
        }
        if(count_old<count_new){count_old = count_new; }

        free(mvc_labels_nodgc[i]);
        free_svector(fybarcache_nodgc[i]);
    }
    free(mvc_labels_nodgc);
    free(fybarcache_nodgc);
    printf("\tmax mismatch count: %d\n",count_old);*/

    double final_slack_norm = sparm->slack_normalizer*(double)num_ex;
    for (i=0;i<num_ex;i++) {

        ybar.labels = mvc_labels[i];
        fy = copy_svector(fycache[i]);
        fybar = copy_svector(fybarcache[i]);
        free_svector(fybarcache[i]);

        lossval = loss_zero_one(ex[0].y,ybar,sparm,i); // it's not divided by n or Jp+n

        /*if(i==0){
                fybar = psi(ex[0].x,ybar,sm,sparm);
                fybar_temp = copy_svector(fybar);
            }else{
                ybar_prev.labels = mvc_labels[i-1];
                fybar = dynamic_psi(ex[0].x, fybar_temp, ybar_prev, ybar, sm, sparm);
                free_svector(fybar_temp);
                fybar_temp = copy_svector(fybar);
            }*/

        if( ex[0].y.labels[i] == 1 ){
            lossval = sparm->J*lossval;
        }
        //lossval = lossval*100.0;

        /* scale difference vector */
        for (f=fy;f;f=f->next) {
            if( ex[0].y.labels[i] == 1 ) f->factor *= (double)sparm->J/(double)final_slack_norm;
            else f->factor *= (double)1.0/(double)final_slack_norm;
        }
        for (f=fybar;f;f=f->next) {
            if( ex[0].y.labels[i] == 1 ) f->factor *= (double)-sparm->J/(double)final_slack_norm;
            else f->factor *= (double)-1.0/(double)final_slack_norm;
        }

        // sanity check (slack >= 0)
        /*double slack_temp = 0.0, l_score = 0.0, r_score = 0.0, loss_temp = 0.0;
        l_score = sprod_ns(sm->w, fybar);
        r_score = sprod_ns(sm->w, fy);
        loss_temp = lossval;

        if(ex[0].y.labels[i] == 1) loss_temp /= (double)sparm->J;
        slack_temp =  (l_score + loss_temp - r_score);
        if(slack_temp<-1E-5){
            slack_fail = 1;
            //printf("\n\t\t---- Sanity check FAILED (FCP: slack less than zero) ----");
            //printf("\n\t\t-- index:%03d, slack value:%.10f\tleft_score:%f\tlossval:%f\tright_score:%f --\n",i,slack_temp,l_score, loss_temp, r_score);

        }

        // sanity check (pred_loss <= slack)
        if(pred_labels[i] == ex[0].y.labels[i]) pred_loss = 0.0;
        else pred_loss = 1.0; //pred_loss = 1.0/(double)norm_factor;
        if(pred_loss>slack_temp + 1E-5){
            pred_loss_fail = 1;
            printf("\n\t\t---- Sanity check FAILED (FCP: pred_loss > slack) ----");
            printf("\n\t\t-- index:%03d, slack value:%.10f\tpred_loss:%f \n",i,slack_temp,pred_loss);

        }*/


        if(ex[0].y.labels[i] == 1) pred_loss *= (double)sparm->J;

        /* add ybar to constraint */
        append_svector_list(fy,lhs);
        append_svector_list(fybar,fy);
        lhs = fybar;

        //*margin+= (double) lossval/ (double) num_ex;
        //*avg_pred_loss += pred_loss/(double) num_ex;

        *margin+= (double) lossval/ (double) final_slack_norm;
        *avg_pred_loss += pred_loss/(double) final_slack_norm;
    }


    /* compact the linear representation */
    new_constraint = add_list_nn(lhs, sm->sizePsi);

    for(i=0;i<num_ex;i++){
        free(mvc_labels[i]);
    }
    free(mvc_labels);
    free(fybarcache);
    free(pred_labels);
    free_svector(lhs);
    //free_svector(fybar_temp);

    /*if( slack_fail){
        printf("\n\t\t---- Sanity check FAILED (M4-SVM loss, FCP: slack less than zero) ----");

    }
    if(pred_loss_fail){
        printf("\n\t\t---- Sanity check FAILED (M4-SVM loss, FCP: pred_loss > slack) ----");
    }*/

    return(new_constraint);
}

double* fcp_hob_svm(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss, double *epsilon_max, double C){
    /* find cutting plane for High-Order Binary SVM. This work was published in ECCV 14 */

    double pred_loss = 0.0;

    int *pred_labels;
    int i,j,*label_mask;
    SVECTOR *f, *fy, *fybar, *lhs;
    LABEL      ybar, ypred;
    double lossval = 0.0;
    double *new_constraint;

    int num_ex, num_ex_J;
    int slack_fail = 0, pred_loss_fail = 0;


    /* find cutting plane */
    lhs = NULL;
    *margin = 0.0;
    *avg_pred_loss = 0.0;

    num_ex = ex[0].x.n_neg + ex[0].x.n_pos;
    num_ex_J = ex[0].x.n_neg + sparm->J*ex[0].x.n_pos;
    label_mask = (int*) malloc (num_ex*sizeof(int)); // used for cssvm

    for(i=0;i<num_ex;i++){
        label_mask[i] = 1;
    }

    find_mvc_hob_svm(ex[0].x, ex[0].y, &ybar, sm, sparm, &pred_labels);

    double slack_norm = (double) sparm->slack_normalizer;

    fy = copy_svector(fycache[0]);
    fybar = psi(ex[0].x,ybar,label_mask, sm,sparm);
    lossval = loss_zero_one(ex[0].y,ybar,sparm,i);

    //lossval *= 100.0;

    /*SVECTOR *fy_diff = NULL;
    double diff_norm = 0.0, eps = 0.0;
    fy_diff = sub_ss(fy,fybar);
    diff_norm = sprod_ss(fy_diff,fy_diff)/(sparm->slack_normalizer*sparm->slack_normalizer);
    //eps = 4.0*diff_norm*sparm->C*sparm->slack_normalizer;
    eps = 4.0*diff_norm*C;
    *epsilon_max = MIN(eps,diff_norm);
    printf("\n\t diff norm (Rsquare CSSVM):%.8f\tEpsilon Max (4*R^2*C): %.8f",diff_norm,eps);
    free_svector(fy_diff);*/

    /* scale difference vector */
    for (f=fy;f;f=f->next) {
        f->factor *= (double)1.0/slack_norm;
        //f->factor *= (double)1.0;
    }

    for (f=fybar;f;f=f->next) {
        f->factor *= (double)-1.0/slack_norm;
        //f->factor *= (double)-1.0;
    }

    // sanity check (slack >= 0)
    /*double slack_temp = 0.0, l_score = 0.0, r_score = 0.0, loss_temp = 0.0;
    l_score = sprod_ns(sm->w, fybar);
    r_score = sprod_ns(sm->w, fy);
    loss_temp = lossval;
    slack_temp =  l_score + loss_temp - r_score;
    if(slack_temp<-1E-5){
        slack_fail = 1;
        printf("\n\t\t---- Sanity check FAILED (FCP: slack less than zero) ----");
        printf("\n\t\t-- index:%03d, slack value:%.10f\tleft_score:%f\tlossval:%f\tright_score:%f --\n",i,slack_temp,l_score, loss_temp, r_score);

    }

    // sanity check (pred_loss <= slack)
    ypred.labels = pred_labels;
    pred_loss = loss_zero_one(ex[0].y,ypred,sparm,i);
    if(pred_loss>slack_temp + 1E-4){
        pred_loss_fail = 1;
        printf("\n\t\t---- Sanity check FAILED (FCP: pred_loss > slack) ----");
        printf("\n\t\t-- index:%03d, slack value:%.10f\tpred_loss:%f \n",i,slack_temp,pred_loss);

    }
    */

    /* add ybar to constraint */
    append_svector_list(fy,lhs);
    append_svector_list(fybar,fy);
    lhs = fybar;

    //pred_loss *= (double)num_ex;
    //pred_loss /= (double)num_ex_J;

    *margin = lossval/slack_norm;
    *avg_pred_loss = pred_loss/slack_norm;

    /* compact the linear representation */
    new_constraint = add_list_nn(lhs, sm->sizePsi);

    free_svector(lhs);
    free(ybar.labels);
    free(label_mask);
    free(pred_labels);

    return(new_constraint);
}


double* fcp_hoap_svm(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss, double *epsilon_max, double C, int **labels_lin){
    /* find cutting plane for High-Order AP-SVM. This work was published in ECCV 14 */

    double lossval = 0.0, pred_loss = 0.0;
    double *new_constraint;
    int i, num_ex;

    SVECTOR *f, *lhs, *philin, *phibar;
    LABEL ymv;
    double slack_norm = sparm->slack_normalizer;
    double *pred_score = (double*)calloc(num_ex,sizeof(double));

    lhs = NULL;
    *margin = 0.0;
    *avg_pred_loss = 0.0;

    num_ex = ex[0].x.n_neg + ex[0].x.n_pos;
    int *mv_ranking = find_mvc_hoap_svm_dgc(ex[0].x, ex[0].y, sm, sparm, fycache, pred_score, labels_lin, &phibar, &philin);
    ymv.ranking = mv_ranking;

    double *mv_ranking_double = (double*)malloc(num_ex*sizeof(double));
    for(i=0;i<num_ex;i++){
        mv_ranking_double[i]  = mv_ranking[i];
    }
    lossval = loss_map(ex[0].y.labels, mv_ranking_double, num_ex, sparm);

    /*for(i=0;i<num_ex;i++){
        printf("%d\t",ymv.ranking[i]);
    }*/
    //phi = phi_combined(ex[0].x, fycache, fybarcache, ex[0].y.ranking, sm, sparm);
    //svector *phi_faster = phi_combined_faster(ex[0].x, fycache, fybarcache, ex[0].y.ranking, sm, sparm);

    //phibar = phi_combined(ex[0].x, fycache, fybarcache, mv_ranking, sm, sparm);
    //phi = phi_combined(ex[0].x, fycache, fybarcache, ex[0].y.ranking, sm, sparm);

    //phi = phi_combined_faster(ex[0].x, labels_lin, fybarcache, ex[0].y.ranking, sm, sparm);


    //pred_loss = loss_map(ex[0].y.labels, pred_score, num_ex, sparm);
    pred_loss = 100.0; // a random value, not of any significance

    // scale difference vector
    for (f=philin;f;f=f->next) {
        f->factor*=1.0/slack_norm;
    }
    for (f=phibar;f;f=f->next) {
        f->factor*=-1.0/slack_norm;
    }

    /* sanity check */
    /*double slack_temp = sprod_ns(sm->w, phibar) + lossval - sprod_ns(sm->w, philin);
    if(slack_temp<-1E-4){
        printf("\n\t\t---- Sanity check FAILED (FCP: slack less than zero) ----");
        printf("\n\t\t-- index:%03d, slack value:%.10f\tleft_score:%f\tlossval:%f\tright_score:%f --\n",i,slack_temp,sprod_ns(sm->w, phibar), lossval,sprod_ns(sm->w, philin));
    }*/

    // add ybar to constraint
    append_svector_list(philin,lhs);
    append_svector_list(phibar,philin);
    lhs = phibar;


    *margin = lossval/slack_norm;
    *avg_pred_loss = pred_loss;

    // compact the linear representation
    new_constraint = add_list_nn(lhs, sm->sizePsi);

    free_svector(lhs);
    free(mv_ranking);
    free(mv_ranking_double);
    free(pred_score);

    return(new_constraint);

}



SVECTOR* find_cutting_plane(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss, double *eps_max, double C, int **labels_lin){


    double *new_constraint;
    struct timespec start_t,finish_t;
    double time_taken = 0.0;
    int i,k;
    SVECTOR *fvec;
    WORD *words;

    current_utc_time(&start_t);

    if(sparm->A == 1){ //M4SVM
        new_constraint = fcp_m4svm(ex, fycache, margin, m,sm, sparm, avg_pred_loss);
    }else if (sparm->A == 2){ //HOAP-SVM
        new_constraint = fcp_hoap_svm(ex, fycache, margin, m,sm, sparm, avg_pred_loss, eps_max, C, labels_lin);
    }else{ //HOB-SVM
        new_constraint = fcp_hob_svm(ex, fycache, margin, m,sm, sparm, avg_pred_loss, eps_max, C);
    }

    long l=0;
    for (i=1;i<sm->sizePsi+1;i++) {
        if (fabs(new_constraint[i])>1E-14) l++; // non-zero
        //if (fabs(new_constraint[i])>0.0) l++; // non-zero
    }
    words = (WORD*)my_malloc(sizeof(WORD)*(l+1));
    assert(words!=NULL);
    k=0;
    for (i=1;i<sm->sizePsi+1;i++) {
        if (fabs(new_constraint[i])>1E-14) {
            //if (fabs(new_constraint[i])>0.0) {
            words[k].wnum = i;
            words[k].weight = new_constraint[i];
            k++;
        }
    }
    words[k].wnum = 0;
    words[k].weight = 0.0;
    fvec = create_svector(words,"",1);

    free(words);
    free(new_constraint);

    current_utc_time(&finish_t);
    time_taken = (finish_t.tv_sec - start_t.tv_sec) + (finish_t.tv_nsec - start_t.tv_nsec)/BILLION;
    //printf("\nfcp time taken:%f",time_taken);

    return(fvec);
}



double cutting_plane_algorithm_dual(double *w, long m, int MAX_ITER, double C, double epsilon, SVECTOR **fycache, EXAMPLE *ex,
                                    STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, char *modelfile, struct timespec start_t_ol, char *trainfile, int cccp_num, double *eps_new, int**labels_lin) {


    long i,j;

    DOC **dXc; // constraint matrix
    SVECTOR *new_constraint, *f;
    int iter, size_active, r;
    double slack;
    double threshold = 0.0;
    double margin = 0.0;
    double primal_obj, dual_obj, gap=0.0;
    double *cur_slack = NULL;
    int mv_iter;
    int *idle = NULL;
    double avg_pred_loss = 0.0;

    iter = 0;
    size_active = 0;
    dXc = NULL;

    int inter_num = 1;
    int obj_check = 0;
    int count_mosek_err = 0;

    long len_sol = 0;
    long len_w = sm->sizePsi;
    long len_w1 = sm->len_w1;
    long len_w2 = sm->len_w2;

    double *w_old = (double*) calloc(len_w+1,sizeof(double)); // mem loss
    double *delta; // rhs of constraints

    double *alpha_beta;
    double w_norm = 0.0;


    double **G1, **G2;

    /* malloc before using realloc is always good */
    G2 = (double **) malloc(sizeof(double *));
    G1 = (double **) malloc(sizeof(double *));

    alpha_beta = (double*)malloc(sizeof(double));
    delta = (double*)malloc(sizeof(double));

    /* proximal regularizer variable */
    double prox_lin_factor = (sparm->lambda_proximal)/(1.0 + sparm->lambda_proximal);
    double *w_proximal = (double*) calloc(len_w+1,sizeof(double));
    double *prox_lin_alpha = (double*)malloc(sizeof(double));
    double *prox_lin_beta = (double*)calloc(sizeof(double),len_w2);

    for(i = 0; i < sm->sizePsi+1; i++){
        w_proximal[i] = w[i];
    }
    for (j=len_w1+1; j<=len_w; j++){
        prox_lin_beta[j-len_w1-1] = prox_lin_factor*w_proximal[j];
    }

    double dual_constant = 0.5*sprod_nn(w_proximal,w_proximal,sm->sizePsi)*sparm->lambda_proximal/(sparm->lambda_proximal + 1.0);

    struct timespec start_t_cp,finish_t_cp,start_t_mo,finish_t_mo, time_total;
    double time_taken_mo,time_taken_cp, time_sofar;

    char mosek_error_file[2048];
    sprintf(mosek_error_file,"%s.merror",modelfile);

    double diag_value = 1.0;
    double gram_reg = 0.0;
    double inter_time_taken = 0.0;
    double slack_norm = sparm->slack_normalizer;

    double eps_max = 0.0;
    double eps_min = epsilon/sparm->slack_normalizer;


    printf("\t\tRunning structural SVM solver (dual): \n"); fflush(stdout);

    new_constraint = find_cutting_plane(ex, fycache, &margin, m, sm, sparm, &avg_pred_loss, &eps_max, C, labels_lin);
    slack = margin - sprod_ns(w, new_constraint);
    w_norm = sprod_nn(w,w,sm->sizePsi);
    primal_obj = 0.5*w_norm+C*slack;
    //printf("\nInitial w0    slack:%03.4f\t\tpred_loss:%02.4f margin:%02.4f primal obj:%06.4f w_norm2:%06.4f",slack,avg_pred_loss,margin,primal_obj,w_norm);
    printf("\nInitial w0    slack:%3.4f\tmargin:%2.4f\tprimal obj:%6.4f ||w||^2: %6.3f",slack,margin,primal_obj,w_norm);

    while(iter<2 || (slack>threshold + epsilon)&&(iter<MAX_ITER)) {

        iter+=1;
        size_active+=1;

        if((iter % OBJ_CHECK) == 0){
            obj_check = 1;
        }

        printf("."); fflush(stdout);

        len_sol = size_active + sm->len_w2; // alpha + beta

        /* add  constraint */
        dXc = (DOC**)realloc(dXc, sizeof(DOC*)*size_active);
        assert(dXc!=NULL);
        dXc[size_active-1] = (DOC*)malloc(sizeof(DOC));
        dXc[size_active-1]->fvec = new_constraint;
        dXc[size_active-1]->slackid = 1; // only one common slackid (one-slack)
        dXc[size_active-1]->costfactor = 1.0;


        delta = (double*)realloc(delta, sizeof(double)*size_active);
        assert(delta!=NULL);
        delta[size_active-1] = margin;

        alpha_beta = (double*)realloc(alpha_beta, sizeof(double)*len_sol);

        /* the linear part of the QP because of the proximal regularizer */
        prox_lin_alpha  = (double*)realloc(prox_lin_alpha, sizeof(double)*size_active);
        prox_lin_alpha[size_active-1] = -1.0*prox_lin_factor*sprod_ns(w_proximal,dXc[size_active-1]->fvec);

        //alpha_beta = (double*)malloc(sizeof(double)*len_sol);
        assert(alpha_beta!=NULL);

        //alpha_beta[len_sol-1]=0.0;
        for (j=0; j<len_sol; j++){
            alpha_beta[j] = 0.0;
        }

        idle = (int *) realloc(idle, sizeof(int)*size_active); // Check
        assert(idle!=NULL);
        idle[size_active-1] = 0;

        /* update first part of the Gram matrix */
        G1 = (double **) realloc(G1, sizeof(double *)*size_active);
        assert(G1!=NULL);
        G1[size_active-1] = NULL;
        for(j = 0; j < size_active; j++) {
            G1[j] = (double *) realloc(G1[j], sizeof(double)*size_active);
            assert(G1[j]!=NULL);
        }

        for(j = 0; j < size_active-1; j++) {
            G1[size_active-1][j] = sprod_ss(dXc[size_active-1]->fvec, dXc[j]->fvec)/(sparm->lambda_proximal + 1.0);
            //G1[size_active-1][j] = sprod_ss(dXc[size_active-1]->fvec, dXc[j]->fvec);
            G1[j][size_active-1]  = G1[size_active-1][j];
        }
        G1[size_active-1][size_active-1] = sprod_ss(dXc[size_active-1]->fvec,dXc[size_active-1]->fvec)/(sparm->lambda_proximal + 1.0);;
        //G1[size_active-1][size_active-1] += 1E-6;
        //printf("\n\t\tlast diag(G1) : %f",G1[size_active-1][size_active-1]);

        /* update second part of the Gram matrix */
        G2 = (double **) realloc(G2, sizeof(double *)*size_active);
        G2[size_active-1] = (double *) calloc(len_w2,sizeof(double));

        long k = 0;
        long index = 0;
        double value = 0.0;

        while(dXc[size_active-1]->fvec->words[k].wnum){
            index = dXc[size_active-1]->fvec->words[k].wnum;
            value = dXc[size_active-1]->fvec->words[k].weight;
            if(index > len_w1)
            {
                index -= len_w1;
                G2[size_active-1][index-1] = -(double) 2.0*value/(sparm->lambda_proximal + 1.0);
                //G2[size_active-1][index-1] = -(double) 2.0*value;
            }
            k++;
        }

        //diag_value = 1.0; // diagonal elements of the fourth block of the final G matrix (i.e. G4)
        diag_value = 1.0/(sparm->lambda_proximal + 1.0); // diagonal elements of the fourth block of the final G matrix (i.e. G4)

        /* solve QP to update alpha */
        current_utc_time(&start_t_mo);

        for(i = 0; i < sm->sizePsi+1; i++){
            w_old[i] = w[i];
        }

        r = mosek_qp_optimize_dual(G1, G2, delta, alpha_beta, (long) size_active, (long) len_w2, (long) len_w1, C, &dual_obj,obj_check,diag_value , prox_lin_alpha, prox_lin_beta);

        current_utc_time(&finish_t_mo);
        time_taken_mo = (finish_t_mo.tv_sec - start_t_mo.tv_sec) + (finish_t_mo.tv_nsec - start_t_mo.tv_nsec)/BILLION;


        /* if G is non-psd then make it psd by regularizing it (adding small values to the diagonal elements utill it is psd) */
        if(r >= 1293 && r <= 1296)
        {
            gram_reg = sparm->gram_regularization; // 1E-7;
            printf("\n\t\tr:%d. G might not be PSD. Making it PSD by adding small values in the diagonal",r);

            double eps = 1e-12;
            while(r==1295) {
                //printf("\n\t\tr:%d. G might not be PSD. Gram Reg=%g",r, gram_reg); fflush(stdout);
                for(i=0;i<size_active;i++) {
                    G1[i][i] += gram_reg;
                    //diag_value += gram_reg;
                }
                //diag_value = 1.0 + gram_reg; // violates w2<=0.0
                gram_reg *= 10;
                r = mosek_qp_optimize_dual(G1, G2, delta, alpha_beta, (long) size_active, (long) len_w2, (long) len_w1, C, &dual_obj,obj_check,diag_value , prox_lin_alpha, prox_lin_beta);
            }
        }
        else if(r == 4000) printf("\nMOSEK Response code: %d, The optimizer terminated at the maximum number of iterations. The solution returned may or may not be of acceptable quality. \n",r);
        else if(r == 4006) printf("\nMOSEK Response code: %d, The optimizer terminated because of slow progress. The solution returned may or may not be of acceptable quality. \n",r);
        else if(r == 1001) printf("\nMOSEK Response code: %d, The licence might have expired. \n",r);
        else if(r)         printf("\nMOSEK Response code: %d, Check ${MOSEKHOME}/${VERSION}/tools/platform/${PLATFORM}/h/mosek.h\n",r);

        /* write mosek error file if there is any error */
        if(r){
            FILE *fid_merror = fopen(mosek_error_file,"a+");
            fprintf(fid_merror,"mosek error: %d\n",r);
            fclose(fid_merror);
            if(r==1001) exit(1);
        }

        /* alpha to cur_w */
        clear_nvector(w,sm->sizePsi);
        for (j=0;j<size_active;j++) {
            if (alpha_beta[j]>C*ALPHA_THRESHOLD) { // sometimes objective increases more than C*epsilon in the last iteration of CCCP
                //    if (alpha_beta[j]>0.0) {
                add_vector_ns(w,dXc[j]->fvec,alpha_beta[j]);
                idle[j] = 0;
            }
            else{
                idle[j]++;
            }
        }

        for(j=0; j<len_w1;j++){
            w[j] = (w[j] + w_proximal[j]*sparm->lambda_proximal)/(1.0 + sparm->lambda_proximal);
        }

        for(j=0; j<len_w2;j++){
            w[j+1+len_w1] = (w[j+1+len_w1] - alpha_beta[size_active+j] + w_proximal[j+1+len_w1]*sparm->lambda_proximal)/(1.0 + sparm->lambda_proximal);
        }

        /* very small w2 = 0 */
        /*for(j=(len_w1+1); j<=(len_w1+len_w2);j++){
            if(fabs(w[j])<EQUALITY_EPSILON){
                w[j] = 0;
            }
        }*/

        /* Sanity Check (whether w2<=0 is satisfied or not) */
        /*double w2_gtz_max = 0.0;
        int w2_gtz_flag = 0;
        for(j=(len_w1+1); j<=(len_w1+len_w2);j++){
            if(w[j]>1E-10){
                w2_gtz_max = MAX(w2_gtz_max,w[j]);
                w2_gtz_flag = 1;
            }
        }
        if(w2_gtz_flag){
            printf("\n\t\t--- Sanity check failed max(w2[j]>1E-10) = %0.10f) --- ",w2_gtz_max);
            w2_gtz_flag = 0;
            w2_gtz_max = 0.0;
            fflush(stdout);
        }*/

        /* store old model in case of mosek errors (4000, 4006) */
        if((r==4000)||(r == 4006)){
            //printf("\n\tw_norm2:%f",sprod_nn(w,w,sm->sizePsi));
            for(i = 0; i < sm->sizePsi+1; i++){
                w[i] = w_old[i];
            }
            printf("\n\tMOSEK Error:%d\tStored old model",r);
        }

        /* calculate updated slacks for all the constraints */
        cur_slack = (double *) realloc(cur_slack,sizeof(double)*size_active);

        for(i = 0; i < size_active; i++) {
            cur_slack[i] = 0.0;
            for(f = dXc[i]->fvec; f; f = f->next) {
                j = 0;
                while(f->words[j].wnum) {
                    cur_slack[i] += w[f->words[j].wnum]*f->words[j].weight;
                    j++;
                }
            }
            if(cur_slack[i] >= delta[i])
                cur_slack[i] = 0.0;
            else
                cur_slack[i] = delta[i]-cur_slack[i]; // CAUTION: delta depends on ybar which depends on w, i think delta should also be updated based on new w
        }

        /* finding the most violated iteration i.e. the iteration having the max value of slack (max possible loss) */
        mv_iter = 0;
        if(size_active > 1) {
            for(j = 0; j < size_active; j++) {
                if(cur_slack[j] >= cur_slack[mv_iter])
                    mv_iter = j;
            }
        }

        if((r==4000)||(r == 4006)){
            threshold = 0.0;
            count_mosek_err++;
        }
        //else if(size_active > 1  && iter > MV_CHECK){
        else if(size_active > 1){
            threshold = cur_slack[mv_iter];
            //threshold = cur_slack[size_active-1];
        }
        else{
            threshold = 0.0;
        }

        /* find the new most violated constraint based on updated w and also find the corresponding slack */
        current_utc_time(&start_t_cp);

        new_constraint = find_cutting_plane(ex, fycache, &margin, m, sm, sparm, &avg_pred_loss, &eps_max, C, labels_lin);
        slack = margin - sprod_ns(w, new_constraint);

        /* get primal dual gap (note that the gap might be negative as the slack has been updated) */
        w_norm = sprod_nn(w,w,sm->sizePsi);
        double w_wprox = sprod_nn(w,w_proximal,sm->sizePsi);
        double wprox_norm = sprod_nn(w_proximal,w_proximal,sm->sizePsi);

        //primal_obj = 0.5*w_norm+C*slack; // note that the slack is the updated one
        primal_obj = 0.5*(1.0 + sparm->lambda_proximal)*w_norm + (sparm->lambda_proximal/2.0)*wprox_norm + (-sparm->lambda_proximal)*w_wprox + C*slack; // note that the slack is the updated one
        dual_obj -= dual_constant;
        gap = primal_obj - (-dual_obj); // check
        //if(gap<0) printf("\n\tCAUTION: primal dual gap is negative (gap = %f)", gap);

        current_utc_time(&finish_t_cp);
        time_taken_cp = (finish_t_cp.tv_sec - start_t_cp.tv_sec) + (finish_t_cp.tv_nsec - start_t_cp.tv_nsec)/BILLION;

        //printf("\niter_cp:%04d  slack:%03.4f thres:%02.4f pred_loss:%02.4f margin:%02.4f primal_obj:%04.4f dual_obj:%.4f primal_dual_gap:%.4f w_norm2:%04.6f time_mosek:%.3f  time_fcp:%.3f",iter,slack,threshold,avg_pred_loss,margin,primal_obj,-dual_obj, gap,w_norm,time_taken_mo,time_taken_cp); fflush(stdout);
        printf("\niter_cp:%04d  slack:%03.4f thres:%02.4f margin:%02.3f primal_obj:%04.4f ||w||^2: %04.3f time_mosek:%.3f  time_fcp:%.3f",iter,slack,threshold,margin,primal_obj,w_norm,time_taken_mo,time_taken_cp); fflush(stdout);

        if((iter % CLEANUP_CHECK) == 0)
        {
            int old = size_active;
            printf("+"); fflush(stdout);
            size_active = resize_cleanup_dual(size_active, idle, alpha_beta, delta, dXc, G1, G2);
            printf("\nRemoved idle constraints: old:%d\tnew:%d\n",old,size_active);
        }

        current_utc_time(&time_total);
        time_sofar = (time_total.tv_sec - start_t_ol.tv_sec) + (time_total.tv_nsec - start_t_ol.tv_nsec)/BILLION;

        if(count_mosek_err > MAX_MOSEK_ERROR_COUNT){
            printf("\n\n Too many mosek error: 4000 and 4006, the model is not good. Check boundary conditions for W2 or check something else..\n");
            iter = MAX_ITER;
        }


    } /* end cutting plane while loop */
    sm->cp_iter = iter;
    sm->active_constraints = size_active;
    *eps_new = epsilon;

    primal_obj = 0.5*sprod_nn(w,w,sm->sizePsi)+C*slack;

    /* free memory */
    for (j=0;j<size_active;j++) {
        free(G1[j]);
        free(G2[j]);
        free_example(dXc[j],1);
    }
    free(G1);
    free(G2);

    free(dXc);
    free(alpha_beta);
    free(delta);
    free_svector(new_constraint);
    free(cur_slack);
    free(idle);
    free(w_old);
    free(w_proximal);
    free(prox_lin_alpha);
    free(prox_lin_beta);
    //if (svm_model!=NULL) free_model(svm_model,0);

    return(primal_obj);

}

int resize_cleanup_dual(int size_active, int *idle, double *alpha, double *delta, DOC **dXc, double **G1,double **G2){
    int i,j, new_size_active;
    long k;

    i=0;
    while ((i<size_active)&&(idle[i]<IDLE_ITER)) i++;
    j=i;
    while((j<size_active)&&(idle[j]>=IDLE_ITER)) j++;

    while (j<size_active) {
        // copying
        alpha[i] = alpha[j];
        delta[i] = delta[j];

        free(G1[i]);
        G1[i] = G1[j];
        G1[j] = NULL;

        free(G2[i]);
        G2[i] = G2[j];
        G2[j] = NULL;

        free_example(dXc[i],0);
        dXc[i] = dXc[j];
        dXc[j] = NULL;

        i++;
        j++;
        while((j<size_active)&&(idle[j]>=IDLE_ITER)) j++;
    }
    for (k=i;k<size_active;k++) {
        if (G1[k]!=NULL) free(G1[k]);
        if (G2[k]!=NULL) free(G2[k]);
        if (dXc[k]!=NULL) free_example(dXc[k],1);
    }
    new_size_active = i;
    alpha = (double*)realloc(alpha, sizeof(double)*new_size_active);
    delta = (double*)realloc(delta, sizeof(double)*new_size_active);
    G1 = (double**)realloc(G1, sizeof(double*)*new_size_active);
    G2 = (double**)realloc(G2, sizeof(double*)*new_size_active);
    dXc = (DOC**)realloc(dXc, sizeof(DOC*)*new_size_active);

    // resize G1, G2 and idle
    i=0;
    while ((i<size_active)&&(idle[i]<IDLE_ITER)) i++;
    j=i;
    while((j<size_active)&&(idle[j]>=IDLE_ITER)) j++;

    while (j<size_active) {
        idle[i] = idle[j];
        for (k=0;k<new_size_active;k++) {
            G1[k][i] = G1[k][j];
        }
        i++;
        j++;
        while((j<size_active)&&(idle[j]>=IDLE_ITER)) j++;
    }
    idle = (int*)realloc(idle, sizeof(int)*new_size_active);
    for (k=0;k<new_size_active;k++) {
        G1[k] = (double*)realloc(G1[k], sizeof(double)*new_size_active);
        // don't reallocate rows of G2 since it's fixed (len_w2)
    }
    return(new_size_active);
}


SVECTOR *read_sparse_vector_file(char *sparse_file){

    int scanned;
    WORD *words = NULL;
    FILE *fp = fopen(sparse_file, "r");

    if(fp!=NULL){

        int length = 0;
        while(!feof(fp)){
            length++;
            words = (WORD *) realloc(words, length*sizeof(WORD));
            if(!words) printf("Memory Error\n"); exit(1); //die("Memory error.");
            scanned = fscanf(fp, " %d:%f", &words[length-1].wnum, &words[length-1].weight);
            if(scanned < 2) {
                words[length-1].wnum = 0;
                words[length-1].weight = 0.0;
            }
        }
        fclose(fp);

        if(words[length-1].wnum) {
            length++;
            words = (WORD *) realloc(words,length*sizeof(WORD));
            words[length-1].wnum = 0;
            words[length-1].weight = 0.0;
        }

        SVECTOR *fvec = create_svector(words,"",1);
        free(words);

        return fvec;
    }
    else{
        printf("Constraint file not found: %s\n\n",sparse_file);
        exit(1);
    }
}

void initialize_with_weight_vector(STRUCTMODEL *sm, STRUCT_LEARN_PARM sparm) {    

    STRUCTMODEL model;

    if(sparm.weight_initial_apsvm){
        printf("\tReading initial weight vector (APSVM Trained) \n");
        model = read_struct_model_apsvm(sparm.initial_w_file, &sparm);
    }else{
        printf("\tReading initial weight vector \n");
        model = read_struct_model(sparm.initial_w_file, &sparm);
    }

    sm->w = model.w;
    /*sm->sizePsi = (sparm.phi1_size + sparm.phi2_size)*3;;
    sm->len_w1 = model.len_w1;
    sm->len_w2 = model.len_w2;*/
}

void print_training_information(SAMPLE *sample, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm){

    if(sparm->A == 0){
        if(sparm->D){
            printf("\n\t=== HOB-SVM (weighted 0/1 Loss) Learning Started (With Dynamic Graph Cut) ===\n");
        }else{
            printf("\n\t=== HOB-SVM (weighted 0/1 Loss) Learning Started (Without Dynamic Graph Cut) ===\n");
        }
        if(sample->examples[0].x.n_neighbors==0) printf("\t\t(No Connectivity, therefore, boils down to Multiclass-SVM)\n\n");
    }
    else if(sparm->A == 1){
        if(sparm->D){
            printf("\n\t=== M4 Learning (weighted 0/1 Loss) Started (With Dynamic Graph Cut) ===\n");
        }else{
            printf("\n\t=== M4 Learning (weighted 0/1 Loss) Started (Without Dynamic Graph Cut) ===\n");
        }
        if(sample->examples[0].x.n_neighbors==0) printf("\t(No Connectivity, therefore, boils down to Standard Multiclass-SVM)\n\n");
    }
    else if(sparm->A == 2){
        if(sparm->D){
            printf("\n\t=== HOAP-SVM (AP Loss) Learning Started (With Dynamic Graph Cut) ===\n");
        }else{
            printf("\n\t=== HOAP-SVM (AP Loss) Learning Started (Without Dynamic Graph Cut) ===\n");
        }
        if(sample->examples[0].x.n_neighbors==0) printf("\t\t(No Connectivity, therefore, boils down to AP-SVM)\n\n");
    }else{
        printf("\n\t=== WRONG ALGORITHM TYPE ===\n"); exit(1);
    }


    /* some training information */
    printf("\tC: %.8g\n", sparm->svm_c);
    printf("\tProximal Regularizer lambda: %.8g\n", sparm->lambda_proximal);
    printf("\tJ: %.3f\n", sparm->J);
    if(sparm->psi2_type == 1) printf("\tPairwise feature vector consists of POSELET only\n");
    if(sparm->psi2_type == 2) printf("\tPairwise feature vector consists of GIST only\n");
    if(sparm->psi2_type == 3) printf("\tPairwise feature vector consists of [POSELET ; GIST]\n");
    printf("\tsizePsi [%ld + %ld]: %ld\n",sm->len_w1, sm->len_w2, sm->sizePsi);
    printf("\tepsilon: %.8g\n", sparm->epsilon);
    //printf("\tPSI2 POSELET sigma: %.8g\n", sm->pose_sigma);
    //printf("\tPSI2 GIST sigma: %.8g\n", sm->gist_sigma);
    printf("\tBinary potential weight: %.8g\n", sparm->binary_weight);
    printf("\tnum of samples: %d\n", sample->examples[0].n_imgs);
    printf("\tpos/neg samples: %d/%d\n",sample->examples[0].x.n_pos, sample->examples[0].x.n_neg);
    printf("\tPOSELET thres: %.2f\n", sparm->pose_thres);
    printf("\tGIST thres: %.2f\n", sparm->gist_thres);
    printf("\tNo of independent subgraphs: %d\n",sample->examples[0].x.graph.num_subgraphs);
    printf("\tNo of edges (p-p)\t:%d\n",sparm->num_edges_pp);
    printf("\tNo of edges (n-n)\t:%d\n",sparm->num_edges_nn);
    printf("\tNo of edges (p-n)\t:%d\n",sparm->num_edges_pn);
    printf("\tNo of edges (total)\t:%ld\n",sample->examples[0].x.n_neighbors);
    printf("\tMAX_ITER for cutting plane: %d\n", sparm->maxiter); fflush(stdout);

    //printf("\n\tSLACK NORMALIZATION:\t%.3f \n",sparm->slack_normalizer); fflush(stdout);
    //printf("\tPSI NORMALIZATION:\t%.3f \n",sm->psi_normalizer); fflush(stdout);
    //printf("\tModified C: %.8g\n", C);


}


