/************************************************************************/
/*   mosek_qp_optimize.c                                                */
/*                                                                      */
/*   Wrapper for Mosek solver required for the following learning algorithms:
    (1) Multi-Class SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP SVM (HOAP-SVM)
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

#include <stdio.h>
#include <assert.h>
#include "mosek.h"

#define EQUALITY_EPSILON 1E-5

static void MSKAPI printstr(void *handle, char str[]) {
    //printf("%s", str);
} /* printstr */


int mosek_qp_optimize_dual(double **G1, double **G2, double *delta, double* alpha, long size_active, long len_w2, long len_w1, double C, double *dual_obj, int obj_check, double diag_value, double *prox_lin_alpha, double *prox_lin_beta) {

    //printf("C:%f\n",C);

    long k = size_active;
    long k1 = size_active + len_w2;


    long i,j;
    double *c;
    MSKlidxt *aptrb;
    MSKlidxt *aptre;
    MSKidxt *asub;
    double *aval;

    MSKboundkeye bkc[1]; // only one constraint each time
    double blc[1];
    double buc[1];

    MSKboundkeye *bkx;
    double *blx;
    double *bux;
    MSKidxt *qsubi,*qsubj;
    double *qval;

    MSKenv_t env;
    MSKtask_t task;
    MSKrescodee r;

    /*double dual_obj;*/

    c = (double*) calloc(k1,sizeof(double));
    assert(c!=NULL);
    aptrb = (MSKlidxt*) malloc(sizeof(MSKlidxt)*k1);
    assert(aptrb!=NULL);
    aptre = (MSKlidxt*) malloc(sizeof(MSKlidxt)*k1);
    assert(aptre!=NULL);
    asub = (MSKidxt*) malloc(sizeof(MSKidxt)*k1);
    assert(asub!=NULL);
    aval = (double*) malloc(sizeof(double)*k1);
    assert(aval!=NULL);

    bkx = (MSKboundkeye*) malloc(sizeof(MSKboundkeye)*k1);
    assert(bkx!=NULL);
    blx = (double*) malloc(sizeof(double)*k1);
    assert(blx!=NULL);
    bux = (double*) malloc(sizeof(double)*k1);
    assert(bux!=NULL);

    for (i=0;i<k1;i++) {

        /* linear part of the QP ( C'x ) */
        if(i<k){
            c[i] = -1.0*(delta[i] + prox_lin_alpha[i]); // put negative sign to convert into convex (original formulation of the dual is concave)
            //c[i] = -1.0*delta[i]; // put negative sign to convert into convex (original formulation of the dual is concave)
        }
        else{
            //c[i] = -0.0;
            c[i] = -1.0*prox_lin_beta[i-k];
        }

        /* linear constraints of the QP ( A of AX >= b) */
        aptrb[i] = i;
        aptre[i] = i+1;
        asub[i] = 0;

        if(i<k){
            aval[i] = 1.0;
        }
        else{
            aval[i] = 0.0;
        }
    }

    /* bounds of X in the constraints (blx[j] <= x_j <= bux[j]) */
    for (i=0;i<k;i++) {
        bkx[i] = MSK_BK_LO; // bound key specified in mosek
        blx[i] = 0.0;
        bux[i] = MSK_INFINITY;
    }

    for (i=k;i<k1;i++) {
        bkx[i] = MSK_BK_LO; // bound key specified in mosek
        blx[i] = 1E-10; // more positive means stronger the constraint w2<=0
        //blx[i] = 0.0;
        bux[i] = MSK_INFINITY;
    }

    /* bounds for the constraints */
    // same as selfpaced code
    bkc[0] = MSK_BK_UP; // infinite lower bound and fix upper bound
    blc[0] = -MSK_INFINITY;
    buc[0] = C;

    // this should be the best (my change)
    /*bkc[0] = MSK_BK_RA; // range
    blc[0] = 0.0;
    buc[0] = C;*/

    // same as in latentSSVM code
    /*bkc[0] = MSK_BK_FX; //fix
      blc[0] = C;
      buc[0] = C;*/

    /* create mosek environment */
    r = MSK_makeenv(&env, NULL, NULL, NULL, NULL);
    //r = MSK_makeenv(&env, NULL);

    /* check return code */
    if (r==MSK_RES_OK) {
        /* directs output to printstr function */
        MSK_linkfunctoenvstream(env, MSK_STREAM_LOG, NULL, printstr);
    }

    /* initialize the environment */
    r = MSK_initenv(env);

    if (r==MSK_RES_OK) {
        /* create the optimization task */
        r = MSK_maketask(env,1,k1,&task); // edit

        if (r==MSK_RES_OK) {
            r = MSK_linkfunctotaskstream(task, MSK_STREAM_LOG,NULL,printstr);

            if (r==MSK_RES_OK) {
                r = MSK_inputdata(task,
                                  1,k1,
                                  1,k1,
                                  c,0.0,
                                  aptrb,aptre,
                                  asub,aval,
                                  bkc,blc,buc,
                                  bkx,blx,bux);

            }

            if (r==MSK_RES_OK) {

                /* coefficients for the Gram matrix */
                qsubi = (MSKidxt*) malloc(sizeof(MSKidxt));
                assert(qsubi!=NULL);
                qsubj = (MSKidxt*) malloc(sizeof(MSKidxt));
                assert(qsubj!=NULL);
                qval = (double*) malloc(sizeof(double));
                assert(qval!=NULL);

                long ii,jj;
                double value=0.0;

                long nz = 0; // number of non-zero elements
                int flag = 0;
                for (i=0;i<k1;i++) {
                    for (j=0;j<=i;j++) {

                        if(i<k){
                            ii = i;
                            jj = j;
                            value =  G1[i][j];
                            flag = 1;
                        }
                        else if(i>=k){

                            if(j<k){
                                long itemp = i-k;
                                value = G2[j][itemp]/(double) 2.0; // To make G2 symmetric (mosek only takes symmetric matrices as input) : A = (A+A')/2;
                                //printf("%f\t",value);
                                ii = i;
                                jj = j;
                                flag = 1;
                            }
                            else if (i==j){
                                ii = i;
                                jj = j;
                                value =  diag_value; // generally 1.0 if there is no gram matrix regularization to make it psd
                                flag = 1;
                            }
                        }

                        //if(fabs(value)>EQUALITY_EPSILON && flag){ // this generates non-psd errors
                        if(flag){

                            nz++;

                            qsubi = (MSKidxt*) realloc(qsubi, sizeof(MSKidxt)*nz);
                            assert(qsubi!=NULL);
                            qsubj = (MSKidxt*) realloc(qsubj, sizeof(MSKidxt)*nz);
                            assert(qsubj!=NULL);
                            qval = (double*) realloc(qval, sizeof(double)*nz);
                            assert(qval!=NULL);


                            qsubi[nz-1] = ii;
                            qsubj[nz-1] = jj;
                            qval[nz-1] = value;

                            //printf("\nii: %ld,jj:%ld,value:%f",ii,jj,value);
                            flag = 0;

                        }

                    }
                }

                r = MSK_putqobj(task, nz, qsubi,qsubj,qval);
            }


            /*int append = 1;
            MSK_solutionsummary (task, MSK_STREAM_ERR);
            MSK_linkfiletotaskstream (task, MSK_STREAM_LOG, "/home/puneet/Projects/PhD/Codes/M4Learning/M4/src_bb/moseklog_C1E5_G0_f1_noAlphaApprox_init_CSSVMBEST.txt", append);
            */

            /* DEBUG */
            /*
                   printf("t: %ld\n", t);
                   for (i=0;i<t;i++) {
                   printf("qsubi: %d, qsubj: %d, qval: %.4f\n", qsubi[i], qsubj[i], qval[i]);
                   }
                   fflush(stdout);
                 */
            /* DEBUG */

            /* set relative tolerance gap for Interior point method (DEFAULT = 1E-8, MIN = 1E-14)*/
            MSK_putdouparam(task, MSK_DPAR_INTPNT_TOL_REL_GAP, 1E-10);
            MSK_putdouparam(task, MSK_DPAR_CHECK_CONVEXITY_REL_TOL, 1E-7); // DEFAULT = 1E-10

            if (r==MSK_RES_OK) {
                r = MSK_optimize(task);
            }

            if (r==MSK_RES_OK) {
                MSK_getsolutionslice(task,
                                     MSK_SOL_ITR,
                                     MSK_SOL_ITEM_XX,
                                     0,
                                     k1,
                                     alpha);
                /* print out alphas */

                /*for (i=0;i<k;i++) {
                      printf("alpha[%ld]: %.8f\n", i, alpha[i]); fflush(stdout);
                      }*/

                /* output the objective value */
                if(obj_check){
                    MSK_getprimalobj(task, MSK_SOL_ITR, dual_obj);
                }


                //printf("ITER DUAL_OBJ %.8g\n", -(*dual_obj)); fflush(stdout);
            }
            MSK_deletetask(&task);
        }
        MSK_deleteenv(&env);
    }


    /* free the memory */
    free(c);
    free(aptrb);
    free(aptre);
    free(asub);
    free(aval);
    free(bkx);
    free(blx);
    free(bux);
    free(qsubi);
    free(qsubj);
    free(qval);

    //free(bkc);
    //free(blc);
    //free(buc);

    if(r == MSK_RES_OK)
        return(0);
    else
        return(r);
}

/* didn't modify for long time, most likely that it's not correct */
int mosek_qp_optimize_primal(double** psiDiffs, double* delta, double* w, double* cur_slack, long k, double C, double *primal_obj, long w_size, long w1_size) {
    long i,j,t,l;
    double *c;
    MSKlidxt *aptrb;
    MSKlidxt *aptre;
    MSKidxt *asub=NULL;
    double *aval=NULL;

    MSKboundkeye *bkx;
    double *blx;
    double *bux;
    MSKidxt *qsubi,*qsubj;
    double *qval;

    MSKenv_t env;
    MSKtask_t task;
    MSKrescodee r;

    MSKboundkeye bkc[k];
    double blc[k];
    double buc[k];
    c = (double*) malloc(sizeof(double)*(w_size+1));
    assert(c!=NULL);
    bkx = (MSKboundkeye*) malloc(sizeof(MSKboundkeye)*(w_size+1));
    assert(bkx!=NULL);
    blx = (double*) malloc(sizeof(double)*(w_size+1));
    assert(blx!=NULL);
    bux = (double*) malloc(sizeof(double)*(w_size+1));
    assert(bux!=NULL);

    aptrb = (MSKlidxt*) malloc(sizeof(MSKlidxt)*(w_size+1));
    assert(aptrb!=NULL);
    aptre = (MSKlidxt*) malloc(sizeof(MSKlidxt)*(w_size+1));
    assert(aptre!=NULL);
    qsubi = (MSKidxt*) malloc(sizeof(MSKidxt)*w_size);
    assert(qsubi!=NULL);
    qsubj = (MSKidxt*) malloc(sizeof(MSKidxt)*w_size);
    assert(qsubj!=NULL);
    qval = (double*) malloc(sizeof(double)*w_size);
    assert(qval!=NULL);

    double *solutionVec = (double *) malloc(sizeof(double)*(w_size+1));

    for(i=0;i<(w_size+1);i++){
        c[i] = 0.0;
        if(i==0){c[i]=C;}
    }


    l=0;
    for(i=0; i<1; i++){
        //c[i] = C;
        aptrb[i] = l;
        for(j=0;j<k;j++){
            asub = (MSKidxt*) realloc(asub, sizeof(MSKidxt)*(l+1));
            assert(asub!=NULL);
            aval = (double*) realloc(aval, sizeof(double)*(l+1));
            assert(aval!=NULL);
            asub[l] = j;
            aval[l] = 1;
            l++;
        }
        aptre[i] = l;
    }

    for (i=1;i<(w_size+1);i++) {
        //c[i] = 0.0;
        aptrb[i] = l;
        for(j=0;j<k;j++){
            if(psiDiffs[j][i-1] != 0){
                asub = (MSKidxt*) realloc(asub, sizeof(MSKidxt)*(l+1));
                assert(asub!=NULL);
                aval = (double*) realloc(aval, sizeof(double)*(l+1));
                assert(aval!=NULL);
                asub[l] = j;
                aval[l] = psiDiffs[j][i-1];
                l++;
            }
        }
        aptre[i] = l;
    }

    for(i=0; i<1; i++){
        bkx[i] = MSK_BK_LO;
        blx[i] = 0.0;
        bux[i] = MSK_INFINITY;
    }

    for (i=1;i<(w1_size+1);i++) {
        bkx[i] = MSK_BK_FR;
        blx[i] = -MSK_INFINITY;
        bux[i] = MSK_INFINITY;
    }
    for (i=(w1_size+1);i<(w_size+1);i++) {
        bkx[i] = MSK_BK_UP;
        blx[i] = -MSK_INFINITY;
        //bux[i] = 1E-7; // may be sometimes very small positive, that makes binary>=0.
        bux[i] = -EQUALITY_EPSILON;
    }
    for (i=0; i<k; i++) {
        bkc[i] = MSK_BK_LO;
        blc[i] = delta[i];
        buc[i] = MSK_INFINITY;
    }

    /* create mosek environment */
    r = MSK_makeenv(&env, NULL, NULL, NULL, NULL);
    //r = MSK_makeenv(&env,NULL);

    /* check return code */
    if (r==MSK_RES_OK) {
        /* directs output to printstr function */
        MSK_linkfunctoenvstream(env, MSK_STREAM_LOG, NULL, printstr);
    }

    /* initialize the environment */
    r = MSK_initenv(env);

    if (r==MSK_RES_OK) {
        /* create the optimization task */
        r = MSK_maketask(env,k,(w_size+1),&task);

        if (r==MSK_RES_OK) {
            r = MSK_linkfunctotaskstream(task, MSK_STREAM_LOG,NULL,printstr);

            if (r==MSK_RES_OK) {
                r = MSK_inputdata(task,
                                  k,(w_size+1),
                                  k,(w_size+1),
                                  c,0.0,
                                  aptrb,aptre,
                                  asub,aval,
                                  bkc,blc,buc,
                                  bkx,blx,bux);
            }

            if (r==MSK_RES_OK) {

                /* coefficients for the Gram matrix */
                t = 0;
                for (i=1;i<(w_size+1);i++) {
                    qsubi[t] = i;
                    qsubj[t] = i;
                    qval[t] = 1;
                    t++;
                }

                r = MSK_putqobj(task, w_size, qsubi,qsubj,qval);
            }


            /* DEBUG */
            /*
               printf("t: %ld\n", t);
               for (i=0;i<t;i++) {
               printf("qsubi: %d, qsubj: %d, qval: %.4f\n", qsubi[i], qsubj[i], qval[i]);
               }
               fflush(stdout);
             */
            /* DEBUG */

            /* set relative tolerance gap (DEFAULT = 1E-8)*/
            //MSK_putdouparam(task, MSK_DPAR_INTPNT_TOL_REL_GAP, 1E-10);
            MSK_putdouparam(task, MSK_DPAR_INTPNT_TOL_REL_GAP, 1E-14);

            if (r==MSK_RES_OK) {
                r = MSK_optimize(task);
            }

            if (r==MSK_RES_OK) {
                MSK_getsolutionslice(task,
                                     MSK_SOL_ITR,
                                     MSK_SOL_ITEM_XX,
                                     0,
                                     (w_size+1),
                                     solutionVec);
                /* print out alphas */
                /*
                   for (i=0;i<k;i++) {
                   printf("alpha[%ld]: %.8f\n", i, alpha[i]); fflush(stdout);
                   }
                 */
                /* output the objective value */
                MSK_getprimalobj(task, MSK_SOL_ITR, primal_obj);
                //printf("ITER DUAL_OBJ %.8g\n", *primal_obj); fflush(stdout);
            }
            MSK_deletetask(&task);
        }
        MSK_deleteenv(&env);
    }

    /*for(i=0; i<1; i++){
      cur_slack[i] = solutionVec[i];
      }*/

    cur_slack[k-1] = solutionVec[0];

    for (i = 1; i < (w_size+1); i++)
    {
        w[i] = solutionVec[i]; // check
    }


    /* free the memory */
    free(c);
    free(aptrb);
    free(aptre);
    free(asub);
    free(aval);
    free(bkx);
    free(blx);
    free(bux);
    free(qsubi);
    free(qsubj);
    free(qval);

    free(solutionVec);

    if(r == MSK_RES_OK)
        return(0);
    else
        return(r);
}

