/************************************************************************/
/*   hoap_svm_api.h                                                    */
/*                                                                      */
/*   API function interface for the following learning algorithms:
    (1) Multi-Class SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP SVM (HOAP-SVM)
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

#include "../svm_light/svm_common.h"
#include "hoap_svm_api_types.h"
#include <time.h>

void init_struct_model(SAMPLE sample, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
SVECTOR* multadd_ss(SVECTOR *a, SVECTOR *b, double factor); // not declared in common.h, very important to declare

void write_struct_model(char *file, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
STRUCTMODEL read_struct_model(char *file, STRUCT_LEARN_PARM *sparm);
STRUCTMODEL read_struct_model_apsvm(char *file, STRUCT_LEARN_PARM *sparm);
void free_struct_model(STRUCTMODEL sm, STRUCT_LEARN_PARM *sparm);
void free_pattern(PATTERN x);
void free_label(LABEL y);
void free_struct_sample(SAMPLE s);
void parse_struct_parameters(STRUCT_LEARN_PARM *sparm);

void print_label(LABEL l, FILE *flabel);

void current_utc_time(struct timespec *ts);

void my_wait_any_key();

int resize_cleanup(int size_active, int **ptr_idle, double **ptr_cur_slack, double **ptr_delta, DOC ***ptr_dXc,
                   double ***ptr_psiDiffs, int *mv_iter);

void approximate_to_psd(double **G1,double **G2, int size_active, int len_w2, double eps);
void Jacobi_Cyclic_Method(double eigenvalues[], double *eigenvectors, double *A, int n);
double sprod_nn(double *a, double *b, long n);
void add_vector_nn(double *w, double *dense_x, long n, double factor) ;
double* add_list_nn(SVECTOR *a, long totwords);

int compar(const void *a, const void *b);
long *randperm(long m);
SAMPLE  generate_train_set(SAMPLE alldata, long *perm, int ntrain);
SAMPLE  generate_validation_set(SAMPLE alldata, long *perm, int ntrain);
double compute_current_loss(SAMPLE val, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);



int resize_cleanup_dual(int size_active, int *idle, double *alpha, double *delta, DOC **dXc, double **G1,double **G2);
int resize_cleanup_primal(int size_active, int **ptr_idle, double **ptr_cur_slack, double **ptr_delta, DOC ***ptr_dXc, double ***ptr_psiDiffs, int *mv_iter);


int store_intermediate_results_dual(char *modelfile, double **G1, double **G2, double *delta, long len_w1, long len_w2, int size_active, DOC** dXc, int *idle, int iter, double *w, int inter_num, double inter_time_taken);
int load_intermediate_results_dual(char *modelfile, double ***G1, double ***G2, double **delta, DOC*** dXc, int *size_active, long *len_w1, long *len_w2, int **idle, int *iter, int inter_num, double *inter_time_taken);


void initialize_with_weight_vector( STRUCTMODEL *sm, STRUCT_LEARN_PARM sparm);
void my_read_input_parameters(int argc, char *argv[], char *trainfile, char* modelfile,STRUCT_LEARN_PARM *struct_parm);
SVECTOR *read_sparse_vector_file(char *sparse_file);
SVECTOR *read_sparse_vector(char *file_name, int object_id, STRUCT_LEARN_PARM *sparm);
SVECTOR *read_sparse_vector_phi1(char *file_name, int object_id, STRUCT_LEARN_PARM *sparm);
SVECTOR *read_sparse_vector_phi2(char *file_name, STRUCT_LEARN_PARM *sparm);
SAMPLE read_struct_examples(char *file, STRUCT_LEARN_PARM *sparm);
SAMPLE read_struct_examples_direct(char *traindatafile, STRUCT_LEARN_PARM *sparm);
double current_obj_val(EXAMPLE *ex, SVECTOR **fycache, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double C, int **labels_lin);


int **linearize_concave_part_nodgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
int **linearize_concave_part(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
int **linearize_concave_part_constraint(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);


void get_unary(PATTERN x, STRUCTMODEL *sm, double **up, double **un);
double **get_binary(PATTERN x, STRUCTMODEL *sm);
double** find_max_marginals(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
double** find_max_marginals_dy(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
double** find_max_marginals_nody(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
int** dgc_maxmarginal_labels_pos(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg);
int** dgc_maxmarginal_labels_neg(double *unary_pos, double *unary_neg, double **binary, int n_pos, int n_neg);

SVECTOR *psi(PATTERN x, LABEL y, int *label_mask, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
SVECTOR *dynamic_psi(PATTERN x, SVECTOR *f0, LABEL y, LABEL ybar, int* label_mask, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) ;
//SVECTOR *dynamic_psi(PATTERN x, SVECTOR *f0, LABEL y, LABEL ybar, int* label_mask, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, int graphID_ref);
SVECTOR** create_feature_cache_dynamic(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, int **labels);
SVECTOR** create_feature_cache(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, LABEL ystar, int *label_mask);
SVECTOR *phi_combined(PATTERN x, SVECTOR **fycache_lin, SVECTOR **fycache_mvc, int *ranking,  STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
void test_dy_vs_nody_feature_cache(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);


double loss_zero_one(LABEL y, LABEL ybar, STRUCT_LEARN_PARM *sparm, int ex_num);
double loss_map(int *true_labels, double *scores, int num_ex, STRUCT_LEARN_PARM *sparm);
void loss_classify(LABEL y, LABEL ybar, double *scores, STRUCT_LEARN_PARM *sparm, double *loss_avg, double *loss_weighted, double *loss_ap);
double* classify_hoap(PATTERN x, LABEL *y, double **max_marginals, STRUCT_LEARN_PARM *sparm);
double* classify_hob(PATTERN x, LABEL *y, double **max_marginals, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);


double cutting_plane_algorithm_dual(double *w, long m, int MAX_ITER, double C, double epsilon, SVECTOR **fycache, EXAMPLE *ex,
                               STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, char *modelfile,struct timespec start_t_ol, char *trainfile, int cccp_num, double *eps_new, int **labels_lin);
double cutting_plane_algorithm_primal(double *w, long m, int MAX_ITER, double C, double epsilon, SVECTOR **fycache, EXAMPLE *ex,
                               STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, char *modelfile);


SVECTOR* find_cutting_plane(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss, double *eps_max, double C, int **labels_lin);
double* fcp_CSSVM_zero_one_loss(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss, double *eps_max, double C);
double* fcp_hoap_svm(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss, double *eps_max, double C, int **labels_lin);
double* fcp_m4svm(EXAMPLE *ex, SVECTOR **fycache, double *margin, long m, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *avg_pred_loss);
void find_mvc_hob_svm(PATTERN x, LABEL y, LABEL *ybar, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, int **pred_labels);
int **find_mvc_m4svm_nodgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR ***fybarcache,int *pred_labels);
int **find_mvc_m4svm_dgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR ***fybarcache, int *pred_labels);
//int* find_mvc_hoap_svm_dgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **fycache_lin, SVECTOR ***fybarcache, double *pred_score, int **labels_lin, SVECTOR **phi_faster, double *mm_lin_scores);
int* find_mvc_hoap_svm_dgc(PATTERN x, LABEL y, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **fycache_lin, double *pred_score, int **labels_lin, SVECTOR **phibar, SVECTOR **phi);


SVECTOR* exp_add_ss_abs(SVECTOR *a, SVECTOR *b, double factor_add, double factor_exp );
SVECTOR* exp_add_ss_pow(SVECTOR *a, SVECTOR *b, double factor_add, double factor_pow, double factor_exp );
double sprod_nn(double *a, double *b, long n);

SVECTOR *psi_with_weights(PATTERN x, int *up_factor, int *un_factor, int **bin_factor, int *up_factor_gt, int *un_factor_gt, int **bin_factor_gt, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **philin);
SVECTOR *phi_combined_faster(PATTERN x, int **labels_lin, int **labels_mvc, int *ranking, int *ranking_gt, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, SVECTOR **philin);

int** dgc_subgraph_based_mvc(PATTERN x,STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm, double *unary_pos, double *unary_neg, double *up_new, double *un_new, double **binary, double **mm_scores);
void check_dynamic_graph_cut(PATTERN x, LABEL y, STRUCTMODEL *sm);

double get_score_from_labeling_mvc(PATTERN x, int given_index, double *unary_pos, double *unary_neg, double **binary, int *labels);
double get_score_from_labeling_lin(PATTERN x, int given_index, double *unary_pos, double *unary_neg, double **binary, int *labels);

void print_training_information(SAMPLE *sample, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm);
void print_classification_information(SAMPLE *sample, STRUCTMODEL *model, STRUCT_LEARN_PARM *sparm);
