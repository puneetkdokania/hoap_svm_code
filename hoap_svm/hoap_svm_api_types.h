/************************************************************************/
/*   hoap_svm_api_types.h                                               */
/*                                                                      */
/*   API type definitions for the following learning algorithms:
    (1) Multi-Class SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP SVM (HOAP-SVM)
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

# include "../svm_light/svm_common.h"

typedef struct sampleScore{
    int idx;
    double score;
} SAMPLE_SCORE;


typedef struct sub_graph{
    int num_nodes;
    int num_edges;
    int *node_index;
    int *mask;
}SUB_GRAPH;

typedef struct full_graph {
    int num_subgraphs;
    int *graphID;
    SUB_GRAPH *subgraph;
}FULL_GRAPH;

typedef struct sub_pattern {
  /*
    Type definition for input pattern x
  */
  char phi1_file_name[1000];
  char phi2_file_name[1000];
  int id;
  int label;
  SVECTOR *phi1;
  SVECTOR *phi2;
  SVECTOR *phi1phi2_pos;
  SVECTOR *phi1phi2_neg;
  SVECTOR *phi1phi2_shift;
  SVECTOR *phi1_shift;
  SVECTOR *phi2_shift;

  
} SUB_PATTERN;

typedef struct pattern {
  /*
    Type definition for input pattern x
  */
	double example_cost; /* cost of individual example */
    SUB_PATTERN *x_is;
    int n_pos;
    int n_neg;

    FULL_GRAPH graph;
    int *labels_mask;
    int **neighbors;
    //int *graphID;
    //int *num_edges; // in each disconnected graph component
    //int *num_nodes; // in each disconnected graph component
    //int num_graphs;
    long n_neighbors;


} PATTERN;

typedef struct label {
  /*
    Type definition for output label y
  */
    int *labels;
    int *ranking;
    int **ranking_matrix;
    int n_pos;
    int n_neg;

} LABEL;

typedef struct _sortStruct {
	  double val;
	  int index;
}  sortStruct;

typedef struct example {
  PATTERN x;
  LABEL y;
  int n_imgs; 
} EXAMPLE;

typedef struct sample {
  int n;
  EXAMPLE *examples;
} SAMPLE;


typedef struct structmodel {
  double *w;          /* pointer to the learned weights */
  MODEL  *svm_model;  /* the learned SVM model */

  long   sizePsi;     /* maximum number of weights in w */
  long len_w1;
  long len_w2;
  /* other information that is needed for the stuctural model can be
     added here, e.g. the grammar rules for NLP parsing */
  long n;             /* number of examples */


  double inter_time_taken; // time taken for the previous intermediate results
  double total_time_taken;
  int cp_iter; // total cutting plane iterations taken to converge
  int active_constraints; // active constraints by the end of convergence

  double primal_obj;
  double pose_sigma;
  double gist_sigma;
  int psi2_type;// pairwise (binary) feature type; 1:only poselet, 2: only gist, 3: poselet+gist

  double psi_normalizer;
  double binary_weight;



} STRUCTMODEL;


typedef struct struct_learn_parm {
  double epsilon;              /* precision for which to solve
				  quadratic program */
  long newconstretrain;        /* number of new constraints to
				  accumulate before recomputing the QP
				  solution */
  double svm_c;                    /* trade-off between margin and loss */
  long maxiter;

  char   custom_argv[20][1000]; /* string set with the -u command line option */
  int    custom_argc;          /* number of -u command line options */
  int    slack_norm_type;           /* norm to use in objective function
                                  for slack variables; 1 -> L1-norm, 
				  2 -> L2-norm */
  int    loss_type;            /* selected loss function from -r
				  command line option. Select between
				  slack rescaling (1) and margin
				  rescaling (2) */
  int    loss_function;        /* select between different loss
				  functions via -l command line
				  option */
  /* add your own variables */
          int phi1_size;
          int phi2_size;
          int sizePsi;

          double slack_normalizer;
          double gram_regularization;
          double pairwise_threshold;

          double pose_thres; // for poselet
          double gist_thres; // for gist

          double pose_sigma;
          double gist_sigma;
          double binary_weight;

          int solve_dual;

          int primal_check;

          int load_inter_results;
          int inter_num;
          int store_inter_results;
          double timetosave_inter;

          int igloo_cluster;
          long num_neighbors;
          int num_ex;

          int fold;

          double J; // data balancing factor (#neg/#pos)

          char *initial_w_file;

          int A; // learning algorithm type
          int D; // DGC or non-DGC
          int t; // Input trainfile type (0 for svml type with whole data in one file, 1 for our old type with location of differnt files)

          int verbosity;


          int num_edges_pp;
          int num_edges_pn;
          int num_edges_nn;

		
          int weight_initial_apsvm; // 1 if initialization is done using apsvm model file

          int psi2_type;// pairwise (binary) feature type; 1:only poselet, 2: only gist, 3: poselet+gist

          double lambda_proximal;



} STRUCT_LEARN_PARM;

