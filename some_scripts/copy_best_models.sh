#!/bin/bash
#$-cwd


BEST_MODEL_DIR=./best_models_apsvm/

if [ ! -d ${BEST_MODEL_DIR} ]; then
	mkdir ${BEST_MODEL_DIR}
fi

G=-1
#Cbest=( 10 1 1 1 10 100 1 1 10 100 )
Cbest=( 1 1 10 1 10 100 1 1 10 100 )
folds=5

declare -a action=("jumping" "phoning" "playinginstrument" "reading" "ridingbike" "running" "takingphoto" "usingcomputer" "walking" "ridinghorse")
#declare -a action=("usingcomputer")

cno=0
for a in ${action[@]};
do

        ACTION=$a
        #MODELDIR=./models_m4ap_imagebased_allbb_binweight_cssvminit_${ACTION}/
        #MODELDIR=./models_apsvm_imagebased_allbb_${ACTION}/
        MODELDIR=./models_apsvm_imagebased_mtobb_${ACTION}/
	
	C=${Cbest[$cno]}
	#B=${Bbest[$cno]}
	cno=`expr $cno + 1`

    for k in `seq 1 ${folds}`;
    do  
			
	#model=${MODELDIR}model_cssvm_imagebased_${ACTION}_C${C}_Pt-1_G${G}_B${B}_fold${k}
	#model=${MODELDIR}model_cssvm_imagebased_${ACTION}_C${C}_Pt-1_G${G}_B${B}_fold${k}
	model=${MODELDIR}model_apsvm_imagebased_${ACTION}_C${C}_Pt-1_G${G}_fold${k}
	cp ${model} ${BEST_MODEL_DIR}

    done

done

