#include <stdio.h>
#include "graph.h"

int main()
{

		typedef Graph<int,int,int> GraphType;
		GraphType *g = new GraphType(/*estimated # of nodes*/ 2, /*estimated # of edges*/ 1);

		g -> add_node();
		g -> add_node();

		int up0 = 1, un0 = 5;

		g -> add_tweights( 0,   /* capacities */  up0, un0 );
		g -> add_tweights( 1,   /* capacities */  2, 6 );
		g -> add_edge( 0, 1,    /* capacities */  3, 4 );

		int flow = g -> maxflow(false);

		printf("Flow = %d\n", flow);
		printf("Minimum cut:\n");
		if (g->what_segment(0) == GraphType::SOURCE)
				printf("node0 is in the SOURCE set\n");
		else
				printf("node0 is in the SINK set\n");
		if (g->what_segment(1) == GraphType::SOURCE)
				printf("node1 is in the SOURCE set\n");
		else
				printf("node1 is in the SINK set\n");

		// DG 1
		int new_up0 = 500;
		int new_un0 = 100;

		int old_trcap = g->get_trcap(0);
		int new_trcap = old_trcap + (new_up0 - up0) - (new_un0 - un0);

		g -> add_tweights( 0, new_up0, new_un0);
		g -> set_trcap(0,new_trcap);
		g -> mark_node(0);

		//int flow_dy = g -> maxflow(true) - flow + MIN(old_trcap, new_trcap); // "true" for using it
		int flow_dy = g -> maxflow(true); // to use residual graph
		printf("Dynamic Flow = %d  new_trcap: %d Old_trcap: %d\n", flow_dy, new_trcap, old_trcap);

		if (g->what_segment(0) == GraphType::SOURCE)
				printf("node0 is in the SOURCE set\n");
		else
				printf("node0 is in the SINK set\n");
		if (g->what_segment(1) == GraphType::SOURCE)
				printf("node1 is in the SOURCE set\n");
		else
				printf("node1 is in the SINK set\n");

		//DG 2

		new_up0 = 1;
		new_un0 = 5;

		old_trcap = g->get_trcap(0);
		new_trcap = old_trcap + (new_up0 - 500) - (new_un0 - 100);

		g -> add_tweights( 0, new_up0, new_un0);
		g -> set_trcap(0,new_trcap);
		g -> mark_node(0);

		flow_dy = g -> maxflow(true); // "true" for using it
		printf("Dynamic Flow = %d  new_trcap: %d Old_trcap: %d\n", flow_dy, new_trcap, old_trcap);

		if (g->what_segment(0) == GraphType::SOURCE)
				printf("node0 is in the SOURCE set\n");
		else
				printf("node0 is in the SINK set\n");
		if (g->what_segment(1) == GraphType::SOURCE)
				printf("node1 is in the SOURCE set\n");
		else
				printf("node1 is in the SINK set\n");




		delete g;
}


