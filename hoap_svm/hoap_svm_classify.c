/************************************************************************/
/*   hoap_svm_classify.c                                               */
/*                                                                      */
/*   Prediction code for the following learning algorithms:
    (1) Multi-Class SVM
    (2) AP-SVM
    (3) High Order Binary SVM (HOB-SVM)
    (4) High Order AP SVM (HOAP-SVM)
/*                                                                      */
/*   Author: Puneet Kumar Dokania  (puneetkdokania at gmail dot com)    */
/*   http://cvn.ecp.fr/personnel/puneet/                                */
/*   Please refer to the project website for more details:
     (http://cvn.ecp.fr/projects/ranking-highorder/) /*
/*                                                                      */
/*   This software is available for non-commercial use only. It must    */
/*   not be modified and distributed without prior permission of the    */
/*   author. The author is not responsible for implications from the    */
/*   use of this software.                                              */
/*                                                                      */
/************************************************************************/

#include <stdio.h>
#include <assert.h>
#include "hoap_svm_api.h"
#include "../maxflow_kolmo_v3.02/maxflowwrap.hpp"
//#include "dgc_kohli/dgcwrapper.h"

#define MARGINAL_INF 100000.0


void read_input_parameters(int argc, char **argv, char *testfile, char *modelfile, char *outfile, char *tag, STRUCT_LEARN_PARM *sparm);


int main(int argc, char* argv[]) {
    double loss_avg = 0.0, loss_weighted = 0.0, ap = 0.0;
    long i, j,num_samples;

    double **max_marginals, *scores;

    char testfile[1024];
    char modelfile[1024];
    char outfile[1024];    
    char tag[1024];
    char maxmarginal_file[1024];
    FILE	*foutfile;

    STRUCTMODEL model;
    STRUCT_LEARN_PARM sparm;

    SAMPLE testsample;
    LABEL y;


    /* read input parameters */
    read_input_parameters(argc,argv,testfile,modelfile,outfile,tag, &sparm);
    foutfile = fopen(outfile,"w");

    /* read model file */
    printf("\n\tReading model: %s ...",modelfile); fflush(stdout);
    model = read_struct_model(modelfile, &sparm);
    printf("\n\tDone.\n");

    /* read test examples */
    if(sparm.t){
        testsample = read_struct_examples(testfile,&sparm);
    }else{
        testsample = read_struct_examples_direct(testfile,&sparm);
    }

    init_struct_model(testsample,&model,&sparm);

    double wnorm2 = sprod_nn(model.w,model.w,model.sizePsi);

    /* Some Info */    
    print_classification_information(&testsample,&model,&sparm);

    num_samples = testsample.examples[0].x.n_neg + testsample.examples[0].x.n_pos;
    sprintf(maxmarginal_file,"%s_%s_maxmarginals",modelfile,tag);

    printf("\n\tCalculating max-marginal scores ..");
    max_marginals = find_max_marginals_dy(testsample.examples[0].x,&model,&sparm);
    //max_marginals = find_max_marginals_nody(testsample.examples[0].x,&model,&sparm);
    printf("\n\tdone!!");

    //printf("\n\tClassifying/Ranking based on max-marginal scores ..");
    if(sparm.A==1 || sparm.A==2) scores = classify_hoap(testsample.examples[0].x, &y, max_marginals, &sparm);
    else scores = classify_hob(testsample.examples[0].x,&y, max_marginals, &model,&sparm);

    loss_classify(testsample.examples[0].y, y, scores, &sparm, &loss_avg, &loss_weighted, &ap);
    free_label(y);

    //fprintf(foutfile, "ZeroOneLoss:%0.4f\nWeightedZeroOneLoss:%0.4f\nAveragePrecision:%0.4f\n", loss_avg, loss_weighted,ap);
    fprintf(foutfile, "%0.4f\t%0.4f\t%0.4f\t", loss_avg, loss_weighted,ap);
    fflush(foutfile);
    fclose(foutfile);

    printf("\n\t\tAverage loss\t\t\t: %.4f", loss_avg);
    printf("\n\t\tWeighted loss (J = %.3f)\t\t: %.4f", sparm.J, loss_weighted);
    printf("\n\t\tAverage precision\t\t: %.4f\n\n",ap);

    //if(sparm.A){
        FILE *fid_mm;
        fid_mm = fopen(maxmarginal_file,"w");
        for (i=0; i<num_samples;i++){
            fprintf(fid_mm,"%lf %lf\n",max_marginals[i][0],max_marginals[i][1]);
        }
        fclose(fid_mm);

        for (i=0; i<num_samples;i++){
            free(max_marginals[i]);
        }
        free(max_marginals);
    //}

    free_struct_sample(testsample);
    free_struct_model(model,&sparm);
    free(scores);

    return(0);
}


void read_input_parameters(int argc, char **argv, char *testfile, char *modelfile, char *outfile, char *tag, STRUCT_LEARN_PARM *sparm) {

    long i;

    /* set default */
    strcpy(modelfile, "hoap_model");
    strcpy(outfile, "hoap_outfile");
    strcpy(tag, "test");

    sparm->custom_argc = 0;

    sparm->pose_thres = -1.0;
    sparm->gist_thres = -1.0; // no gist connectivity
    sparm->pose_sigma = -1.0;
    sparm->gist_sigma = -100.0;
    sparm->binary_weight = 1.0;
    sparm->psi2_type = 1; //poselet only

    sparm->D = 1; // by default uses dynamic graph cut. non-dynamic graph cut needs some modification (check)

    sparm->t = 1; // by-default the input trainfile type is the old one
    sparm->A = 0; // by default the algortihm type is HOB-SVM

    for(i=1;(i<argc) && ((argv[i])[0] == '-');i++) {
        switch ((argv[i])[1]) {

        case 'p': i++; sparm->pose_sigma=atof(argv[i]); break;
        case 'g': i++; sparm->gist_sigma=atof(argv[i]); break;
        case 'P': i++; sparm->pose_thres=atof(argv[i]); break;
        case 'G': i++; sparm->gist_thres=atof(argv[i]); break;
        case 'B': i++; sparm->binary_weight=atof(argv[i]); break;
        case 't': i++; sparm->t=atof(argv[i]); break; // Input trainfile type (0 for svml type with whole data in one file, 1 for our old type with location of differnt files)
        case 'A': i++; sparm->A=atof(argv[i]); break; // Algorithm type (0 for cssvm, 1 for M4, 2 for M4AP)
        case 'b': i++; sparm->psi2_type=atof(argv[i]); break; // pairwise (binary) feature type; 1:only poselet, 2: only gist, 3: poselet+gist
        //case '-': strcpy(struct_parm->custom_argv[struct_parm->custom_argc++],argv[i]);i++; strcpy(struct_parm->custom_argv[struct_parm->custom_argc++],argv[i]);break;
        default: printf("\nUnrecognized option %s!\n\n",argv[i]);
            exit(0);
        }

    }

    /* for (i=1;(i<argc)&&((argv[i])[0]=='-');i++) {
    switch ((argv[i])[1]) {
      case '-': strcpy(sparm->custom_argv[sparm->custom_argc++],argv[i]);i++; strcpy(sparm->custom_argv[sparm->custom_argc++],argv[i]);break;
      default: printf("\nUnrecognized option %s!\n\n",argv[i]); exit(0);
    }
  }*/

    if (i>=argc) {
        printf("\nNot enough input parameters!\n\n");
        exit(0);
    }

    strcpy(testfile, argv[i]);
    if(i+1<argc)
        strcpy(modelfile, argv[i+1]);
    if(i+2<argc)
        strcpy(outfile,argv[i+2]);
    if(i+3<argc)
        strcpy(tag,argv[i+3]);

    parse_struct_parameters(sparm);

}

double** find_max_marginals_nody(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {


    int i, ii;
    int num_ex = (x.n_pos+x.n_neg);
    LABEL ybar;
    SVECTOR *psi_mm;

    double *unary_pos, *unary_neg, **binary;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));
    double **max_marginal = (double **) malloc(num_ex*sizeof(double*));

    for (i = 0; i < num_ex; i++){
        max_marginal[i] = (double *) malloc(sizeof(double)*2);
    }


    // find max marginals for each sample

    //FILE *fid1,*fid2;
    //fid1 = fopen("./pos_mf.labels","w");
    //fid2 = fopen("./neg_mf.labels","w");

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    for (i = 0; i < num_ex; i++){
        up_new[i] = unary_pos[i];
        un_new[i] = unary_neg[i];
    }

    for (ii = 0; ii < num_ex; ii++){

        // max marginal condition for the sample being postive
        up_new[ii] = -MARGINAL_INF;
        un_new[ii] = MARGINAL_INF;

        // graph cut
        ybar.labels = maxflow_labels(up_new, un_new, binary, x.n_pos, x.n_neg);

        /*for(jj=0;jj<(x.n_pos+x.n_neg);jj++){
            fprintf(fid1,"%d ",ybar.labels[jj]);
        }
        fprintf(fid1,"\n");*/


        // find the score
        //psi_mm = psi(x,ybar,sm,sparm); CARE

        max_marginal[ii][0] = sprod_ns(sm->w,psi_mm);
        free_svector(psi_mm);
        free(ybar.labels);

        // max marginal condition for the sample being negative
        up_new[ii] = MARGINAL_INF;
        un_new[ii] = -MARGINAL_INF;

        // graph cut
        ybar.labels = maxflow_labels(up_new, un_new, binary, x.n_pos, x.n_neg);

        up_new[ii] = unary_pos[ii];
        un_new[ii] = unary_neg[ii];

        /*for(jj=0;jj<(x.n_pos+x.n_neg);jj++){
            fprintf(fid2,"%d ",ybar.labels[jj]);
        }
        fprintf(fid2,"\n");*/

        // find the score
        //psi_mm = psi(x,ybar,sm,sparm); CARE

        max_marginal[ii][1] = sprod_ns(sm->w,psi_mm);
        free_svector(psi_mm);
        free(ybar.labels);

        //printf("Max marginal: sample:%d\t pos:%lf\tneg:%lf\n",ii,max_marginal[ii][0],max_marginal[ii][1]);
    }

    //fclose(fid1);
    //fclose(fid2);

    free(unary_pos);
    free(unary_neg);
    free(up_new);
    free(un_new);
    for (i = 0; i < num_ex; i++){
        free(binary[i]);
    }
    free(binary);

    return max_marginal;

}


double** find_max_marginals_dy(PATTERN x, STRUCTMODEL *sm, STRUCT_LEARN_PARM *sparm) {

    int i;
    int num_ex = (x.n_pos+x.n_neg);
    LABEL ybar;
    SVECTOR *psi_mm;
    SVECTOR **fymmcache_pos, **fymmcache_neg;


    double *unary_pos, *unary_neg, **binary;
    double *up_new = (double*)malloc(num_ex*sizeof(double));
    double *un_new =  (double*)malloc(num_ex*sizeof(double));

    double **max_marginal = (double **) malloc(num_ex*sizeof(double*));

    int **mm_labels_pos;
    int **mm_labels_neg;


    for (i = 0; i < num_ex; i++){
        max_marginal[i] = (double *) malloc(sizeof(double)*2);
    }

    get_unary(x, sm, &unary_pos, &unary_neg);
    binary = get_binary(x, sm);

    //printf("\nFinding max-marginal labeling (pos) ...");
    for (i = 0; i < num_ex; i++){
        up_new[i] = -MARGINAL_INF;
        un_new[i] = MARGINAL_INF;
    }

    mm_labels_pos = dgc_maxmarginal_labels( unary_pos, unary_neg, binary, x.n_pos, x.n_neg , up_new, un_new);

    //printf("\nFinding max-marginal labeling (neg) ...");
    for (i = 0; i < num_ex; i++){
        up_new[i] = MARGINAL_INF;
        un_new[i] = -MARGINAL_INF;
    }

    mm_labels_neg = dgc_maxmarginal_labels( unary_pos, unary_neg, binary, x.n_pos, x.n_neg , up_new, un_new);


    fymmcache_pos = create_feature_cache_dynamic(x, sm, sparm, mm_labels_pos);
    fymmcache_neg = create_feature_cache_dynamic(x, sm, sparm, mm_labels_neg);

    for (i=0;i<num_ex;i++){
        max_marginal[i][0] = sprod_ns(sm->w,fymmcache_pos[i]);
        max_marginal[i][1] = sprod_ns(sm->w,fymmcache_neg[i]);

    }

    /*

    //FILE *fid1,*fid2;
    //fid1 = fopen("./pos_dgc1.labels","w");
    //fid2 = fopen("./neg_dgc1.labels","w");

    //printf("\nCalculating max-marginal scores (pos and neg) ...\n");
    for(i = 0; i < num_ex; i++){

        ybar.labels = mm_labels_pos[i];
        psi_mm = psi(x,ybar,sm,sparm);
        max_marginal[i][0] = sprod_ns(sm->w,psi_mm);
        free_svector(psi_mm);

        ybar.labels = mm_labels_neg[i];
        psi_mm = psi(x,ybar,sm,sparm);
        max_marginal[i][1] = sprod_ns(sm->w,psi_mm);
        free_svector(psi_mm);

        //printf("\tsample:%04d\tpos:%lf\tneg:%lf\n",i,max_marginal[i][0],max_marginal[i][1]);


        //for(j = 0; j < (x.n_pos+x.n_neg); j++){
        //    fprintf(fid1,"%d ",mm_labels_pos[i][j]);
        //    fprintf(fid2,"%d ",mm_labels_neg[i][j]);
       // }
       // fprintf(fid1,"\n");
       // fprintf(fid2,"\n");

    }
    //fclose(fid1);
    //fclose(fid2);
    printf("\n");*/

    free(unary_pos);
    free(unary_neg);
    free(up_new);
    free(un_new);

    for (i = 0; i<num_ex; i++){
        free(mm_labels_pos[i]);
        free(mm_labels_neg[i]);
        free(binary[i]);
        free_svector(fymmcache_pos[i]);
        free_svector(fymmcache_neg[i]);
    }
    free(mm_labels_pos);
    free(mm_labels_neg);
    free(binary);
    free(fymmcache_pos);
    free(fymmcache_neg);

    return max_marginal;

}

void print_classification_information(SAMPLE *sample, STRUCTMODEL *model, STRUCT_LEARN_PARM *sparm){

    printf("\n");
    if(sparm->D){
        printf("\n\t=== Prediction Started (With Dynamic Graph Cut) ===\n");
    }else{
        printf("\n\t=== Prediction Started (Without Dynamic Graph Cut) ===\n");
    }

    double wnorm2 = sprod_nn(model->w,model->w,model->sizePsi);


    /* some training information */
    if(sparm->psi2_type == 1) printf("\tPairwise feature vector consists of POSELET only\n");
    if(sparm->psi2_type == 2) printf("\tPairwise feature vector consists of GIST only\n");
    if(sparm->psi2_type == 3) printf("\tPairwise feature vector consists of [POSELET ; GIST]\n");
    printf("\tsizePsi [%ld + %ld]: %ld\n",model->len_w1, model->len_w2, model->sizePsi);
    //printf("\tPSI2 POSELET sigma: %.8g\n", sm->pose_sigma);
    //printf("\tPSI2 GIST sigma: %.8g\n", sm->gist_sigma);
    printf("\t||w||^2 : %.3f\n", wnorm2);
    printf("\tJ\t: %.3f\n", sparm->J);
    printf("\tBinary potential weight : %.8g\n", sparm->binary_weight);
    printf("\tnum of samples\t\t: %d\n", sample->examples[0].n_imgs);
    printf("\tpos/neg samples\t\t: %d/%d\n",sample->examples[0].x.n_pos, sample->examples[0].x.n_neg);
    printf("\tPOSELET thres\t\t: %.2f\n", sparm->pose_thres);
    printf("\tGIST thres\t\t: %.2f\n", sparm->gist_thres);
    printf("\tNo of independent subgraphs: %d\n",sample->examples[0].x.graph.num_subgraphs);
    printf("\tNo of edges (p-p)\t:%d\n",sparm->num_edges_pp);
    printf("\tNo of edges (n-n)\t:%d\n",sparm->num_edges_nn);
    printf("\tNo of edges (p-n)\t:%d\n",sparm->num_edges_pn);
    printf("\tNo of edges (total)\t:%ld\n",sample->examples[0].x.n_neighbors);

}


