#!/bin/bash
#$-cwd

#module load matlab
#echo ${f_export}
#cd /workdir/puneet/codes/M4Learning/svm_light
#PROCSTRING_PBS="cd /workdir/puneet/codes/M4Learning/svm_light; addpath(genpath(pwd)); batch_train(${C_export},${J_export},${f_export});"
#PROCSTRING_PBS="cd /workdir/puneet/codes/M4Learning/svm_light; addpath(genpath(pwd)); batch_train(${C_export},${J_export});"

#PROCSTRING=${PROCSTRING_PBS}
#echo $PROCSTRING

#matlab -nodesktop -nosplash -nojvm -r "${PROCSTRING};exit(1);"i
#/workdir/puneet/codes/M4Learning/context_ssvm
#dual=1
#tlimit=84000
#igloo=1

#cd /workdir/puneet/codes/M4Learning/context_ssvm/src_bb



#Mosek
#DIR=/home/users/pukumar/software
#MOSEKPLATFORM=linux64x86
#
#export PATH=$PATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin
#export MOSEKLM_LICENSE_FILE=$DIR/mosek/6/licenses
#export CLASSPATH=$CLASSPATH:$DIR/mosek/6/tools/platform/$MOSEKPLATFORM/bin/mosek.jar


#Learn
./max_marginal_learn -c $1 -G $2 -B $3 -t 0 -b 1 -A 2 $4 $5 $6

#Train Loss
TAG=train
RESULTFILE=$5_$TAG # modelfile_train
./max_marginal_classify -G $2 -B $3 -t 0 -b 1 -A 2 $4 $5 $RESULTFILE $TAG

#Test Loss
TAG=test
RESULTFILE=$5_$TAG # modelfile_test
./max_marginal_classify -G $2 -B $3 -t 0 -b 1 -A 2 $7 $5 $RESULTFILE $TAG


echo "Job finished"

